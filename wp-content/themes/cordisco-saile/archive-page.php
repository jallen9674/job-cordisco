<?php 

get_header('inner'); ?>

      
      
      <section class="container-fluid page-container section">
        <div class="container">
          		<main class="blog-page">
          			<h1><?= single_cat_title(); ?> FAQs</h1>
              
								<?php //query_posts(  array('cat' => get_query_var('cat'), 'paged' => get_query_var('paged'), 'order' => 'DESC', 'posts_per_page' => 12 ) ); ?>
								<?php if(have_posts()) : while ( have_posts() ) : the_post(); ?>
				                   		<div class="blog-post">
				                       
				                            
				                            <a href="<?php the_permalink(); ?>" class="blog-link" rel="bookmark"><h2><?php the_title(); ?></h2></a>
				                            <div class="blog-list-meta">Posted: <?php the_time('F jS, Y') ?> | Categories: <?php the_category(', '); ?></div>
				                      
				                <?php if(function_exists('get_jamie_social_code')) echo get_jamie_social_code();?>
								   		
				                        
								   			<?php the_excerpt("Continue reading " . get_the_title('', '', false).'&raquo;'); ?> 
				                           
				         				<!-- .entry-content -->
				                        
				    	               <?php edit_post_link( __( 'Edit'), '<div class="edit-link">', '</div>' ); ?>   
				                   
				                   </div>

					                <?php endwhile; else: ?>
				              <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
				              <?php endif; ?>
				              <?php wp_pagenavi(); wp_reset_query();?>
				            </main>
				            <aside>
				            	<?php get_sidebar(); ?>
				            </aside>
         </div>
      </section>






<?php get_footer('inner'); ?>
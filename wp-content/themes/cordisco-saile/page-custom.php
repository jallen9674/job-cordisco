<?php
/*
 Template Name: Custom Page Example
 *
 * A custom page template -- for duplication.
 *
*/
?>

<?php get_header(); ?>

	<div id="inner-content" class="wrapper">

			<div id="main" class="content-container">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> >

				<header class="article-header">
					<h1 class="page-title"><?php the_title(); ?></h1>
				</header>

				<section class="entry-content cf" itemprop="articleBody">
					<?php
						// the content (pretty self explanatory huh)
						the_content();
					?>
				</section>

				<?php comments_template(); ?>

			</article>

			<?php endwhile; else : ?>

					<article id="post-not-found" class="hentry cf">
							<header class="article-header">
								<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
						</header>
							<section class="entry-content">
								<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
						</section>
						<footer class="article-footer">
								<p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
						</footer>
					</article>

			<?php endif; ?>

		</div>

		<?php get_sidebar(); ?>

	</div>

<?php get_footer(); ?>

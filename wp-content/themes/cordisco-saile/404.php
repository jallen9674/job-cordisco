<?php get_header(); ?>

				<div id="inner-content" class="wrapper">

					<div id="main-full" class="content-container">

						<article id="post-not-found" class="hentry cf">

							<header class="article-header">

								<h1 class="h2">Error 404: The page you requested could not be found.</h1>

							</header>

							<section class="entry-content">

								<p><a href="<?php echo home_url(); ?>">Please click here to return to the homepage</a></p>

							</section>

							<section class="search">

									<p><?php //get_search_form(); ?></p>

							</section>


						</article>

					</div>

				</div>

<?php get_footer(); ?>

<?php get_header(); ?>


	<div id="inner-content" class="wrapper">

		<div id="main" class="content-container" role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
 				<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?>>

 					<header class="article-header">
						<?php
						 	//Get Attorney Name
						    if ( get_post_meta( $post->ID, '_cordisco_attorney_name', true ) ) {
						    	echo '<h1 class="main-attorney-name">';
						    	echo get_post_meta( $post->ID, '_cordisco_attorney_name', true );
						    	echo '</h1>';
						    }

							//Get Attorney Title
						    if ( get_post_meta( $post->ID, '_cordisco_attorney_title', true ) ) {
						    	echo '<p class="attorney-title"><strong>';
						    	echo get_post_meta( $post->ID, '_cordisco_attorney_title', true );
						    	echo '</strong></p>';
						    }

						 ?>
 					</header>

	                <section class="entry-content cf">
						<div class="attorney-bio">
							<?php
								//Display Photo if Possible
								if ( has_post_thumbnail() ) {
								    the_post_thumbnail('attorney-thumb', array('class'=>'attorney-thumb','alt' => get_the_title()));
								}
							?>
							 <?php
							 	//Get Attorney Bio
							    if ( get_post_meta( $post->ID, '_cordisco_attorney_bio', true ) ) {
							    	echo apply_filters('the_content', '<br>' . get_post_meta( $post->ID, '_cordisco_attorney_bio', true ));
							    }
							 ?>
						</div>

	                </section> <?php // end article section ?>
             	</article> <?php // end article ?>
			<?php endwhile; ?>

			<?php else : ?>
				<p>Something went wrong.</p>
			<?php endif; ?>

		</div>

		<?php get_sidebar(); ?>

	</div>



<?php get_footer(); ?>

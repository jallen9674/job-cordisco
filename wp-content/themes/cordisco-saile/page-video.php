<?php //Template Name: Video Embed ?>
<?php get_header(); ?>


	<div id="inner-content" class="wrapper">

			<div id="main" class="content-container">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<header class="article-header">
					<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
				</header>

				<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?>>
					<section class="entry-content cf" itemprop="articleBody">

						<?php
						//Begin Video Embed
							if ( (get_post_meta( $post->ID, '_cordisco_video_id', true )) && !get_post_meta( $post->ID, '_cordisco_video_id_schema', true ) ){
						   		$videoID =  get_post_meta( $post->ID, '_cordisco_video_id', true );
						   		$videoDate = get_the_date('c');
						  		?>

							<div itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
								  <meta itemprop="name" content="<?php the_title(); ?>">
								  <meta itemprop="thumbnailUrl" content="http://i3.ytimg.com/vi/<?php echo $videoID; ?>/hqdefault.jpg" />
								  <meta itemprop="contentURL" content="https://www.youtube.com/embed/<?php echo $videoID; ?>" />
								  <meta itemprop="uploadDate" content="<?php echo $videoDate; ?>" />
								    <div class="iframe-wrapper iframe-wrapper-16x9">
								        <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $videoID; ?>?rel=0" frameborder="0" allowfullscreen></iframe>
								      </div>
								      <br>
								  <meta itemprop="description" content="A Cordisco and Saile Video">

								</div>
								<hr class="video-hr">
						<?php
						}
						//End Video Embed
						?>

					</section>

				</article>

				<?php
				/*-------------------------------------------
				Display Related FAQs
				---------------------------------------------*/
				?>
				<h2 class="related-faq-title">Frequently Asked Questions</h2>
				<ul class="related-faq-list">
					<?php

					$faqTerms = get_terms( 'cordisco_faq_cat' );
	        // convert array of term objects to array of term IDs
	        $faqTermIDs = wp_list_pluck( $faqTerms, 'term_id' );

					$args = array(
						'posts_per_page' => '5',
						'post_type' => 'cordisco_faq',
						'tax_query' => array(
							array(
								'taxonomy' => 'cordisco_faq_cat',
								'field' => 'term_id',
								'terms' => $faqTermIDs
							),
						),
						'order' => 'DSC',
						'orderby' => 'rand',
					);

					$the_query = new WP_Query( $args );
					if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
					?>
					<?php //Getting Category for Filtering
					$postTerms =  wp_get_object_terms($post->ID, 'cordisco_faq_cat');
					$categoryFilterSlug = '';
					$categoryPrettyName = '';
					if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
						foreach ( $postTerms as $term ) {
							$categoryFilterSlug .= ' ' . $term->slug;
							$categoryPrettyName .= ' ' . $term->name . '<span class="divider">, </span>';
						}
					}
					?>

					<li><a href="<?php the_permalink(); ?>" class="faq-filter-title-link"><?php the_title(); ?></a></li>

				<?php endwhile; else : ?>
					<p>Sorry no related FAQs found.</p>
				<?php endif; ?>
				<?php wp_reset_query(); ?>
			</ul>

			<?php //End Related FAQ Loop ?>

				<?php endwhile; else : ?>

						<article id="post-not-found" class="hentry cf">
							<header class="article-header">
								<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
							</header>
							<section class="entry-content">
								<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
							</section>
							<footer class="article-footer">
									<p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
							</footer>
						</article>



				<?php endif; ?>

			</div>

			<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>

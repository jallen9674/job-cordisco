<?php
//Template Name: FAQs Archive
?>
<?php get_header(); ?>

	<div id="inner-content" class="wrapper">

			<div id="main" class="content-container">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?>>

					<header class="article-header">
						<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
					</header>

					<section class="entry-content cf result-listing" itemprop="articleBody">

						<ul>

						<?php /*----------- BEGIN DYNAMIC VERDICT LISTINGS -------------- */ ?>

						<?php
				            $args = array(
											'post_type'              => 'cordisco_faq',
											'posts_per_page'         => -1,
											'order'                  => 'ASC',
											'orderby'                => 'date'
				            );

				            $the_query = new WP_Query( $args );
				            if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

				            	<li><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
				          <?php endwhile; else : ?>
				            <!-- IF NOTHING FOUND CONTENT HERE -->
				          <?php endif; ?>
				          <?php wp_reset_query(); ?>

						</ul>
					</section>

					<?php comments_template(); ?>

				</article>

				<?php endwhile; else : ?>

						<article id="post-not-found" class="hentry cf">
							<header class="article-header">
								<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
							</header>
							<section class="entry-content">
								<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
							</section>
							<footer class="article-footer">
									<p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
							</footer>
						</article>

				<?php endif; ?>

			</div>

			<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>

<?php //Template Name: Scholarship Page ?>
<?php get_header(); ?>

	<div id="inner-content" class="wrapper">

			<div id="" class="content-container scholarship-page-left">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?>>

					<header class="article-header">
						<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
					</header>

					<section class="entry-content cf" itemprop="articleBody">

						<?php
							the_content();
						?>
					</section>

					<?php comments_template(); ?>

				</article>

				<?php endwhile; else : ?>

						<article id="post-not-found" class="hentry cf">
							<header class="article-header">
								<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
							</header>
							<section class="entry-content">
								<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
							</section>
							<footer class="article-footer">
									<p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
							</footer>
						</article>

				<?php endif; ?>

			</div>

			<div class="content-container scholarship-page-right">
          <?php echo do_shortcode('[contact-form-7 id="3913" title="Leukemia Application Form html_class="scholarship-form"]'); ?>
      </div>

	</div>

<?php get_footer(); ?>

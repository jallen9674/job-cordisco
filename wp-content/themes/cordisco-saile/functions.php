<?php
/******************************************************************
Square 4 Starter Theme
Built on Bones By Eddie Machado
******************************************************************/


/*--------------------------------------------
Brining in all of Bones, Start it Up!
---------------------------------------------*/

//Bones Core
require_once( 'assets/bones.php' );

//Admin Modifications
require_once( 'assets/admin.php' );

function bones_ahoy() {

  // launching operation cleanup
  add_action( 'init', 'bones_head_cleanup' );

  // A better title
  add_filter( 'wp_title', 'rw_title', 10, 3 );

  // remove WP version from RSS
  add_filter( 'the_generator', 'bones_rss_version' );

  // remove pesky injected css for recent comments widget
  add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );

  // clean up comment styles in the head
  add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );

  // clean up gallery output in wp
  add_filter( 'gallery_style', 'bones_gallery_style' );

  // enqueue base scripts and styles
  add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );

  // launching this stuff after theme setup
  bones_theme_support();

  // adding sidebars to Wordpress (these are created in functions.php)
  add_action( 'widgets_init', 'bones_register_sidebars' );

  // cleaning up random code around images
  add_filter( 'the_content', 'bones_filter_ptags_on_images' );

  // cleaning up excerpt
  add_filter( 'excerpt_more', 'bones_excerpt_more' );

} /* end bones ahoy */

// Get it Started! Miley style.
add_action( 'after_setup_theme', 'bones_ahoy' );


/*--------------------------------------------
Bring a Wrecking Ball to that Emoji
--------------------------------------------*/

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );


/*--------------------------------------------
oEmbed Size Options
---------------------------------------------*/

if ( ! isset( $content_width ) ) {
	$content_width = 640;
}


/*--------------------------------------------
Thumbnail Sizes
---------------------------------------------*/


add_image_size( 'bones-thumb-600', 600, 150, true );
add_image_size( 'bones-thumb-300', 300, 100, true );
add_image_size( 'bones-thumb-page', 300, 150 );
add_image_size( 'faq-thumb', 300, 200, true );
add_image_size( 'attorney-thumb', 250, 300 );

add_filter( 'image_size_names_choose', 'square4_nice_image_sizes' );

function square4_nice_image_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'bones-thumb-600' => __('600px by 150px'),
        'bones-thumb-300' => __('300px by 100px'),
        'bones-thumb-page' => __('300px by 150px'),

    ) );
}

/*--------------------------------------------
Adding Images to FAQ Pages
---------------------------------------------*/

function hennessey_add_faq_images($content){
    if ( has_post_thumbnail($post->ID) && is_singular('cordisco_faq') && in_the_loop() ) {
          $postTitle = get_the_title();
          $thumbnail = get_the_post_thumbnail($post->ID, 'faq-thumb', array('class' => 'faq-thumb', 'alt' => $postTitle) );

          $content = $thumbnail . $content;
          return $content;

      } else {
        return $content;
      }

}
add_filter('the_content','hennessey_add_faq_images');


/*--------------------------------------------
Adding Lucky Orange to Site
---------------------------------------------*/
function hennessey_add_lucky_orange(){
?>
  <script type='text/javascript'>
  window.__lo_site_id = 78200;

   (function() {
    var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
    wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
     })();
   </script>
<?php
}

//add_action('wp_footer', 'hennessey_add_lucky_orange');


/*--------------------------------------------
Declaring Sidebars/Widget Areas
---------------------------------------------*/

function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __( 'Default Sidebar', 'bonestheme' ),
		'description' => __( 'This is the default sidebar', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

  register_sidebar(array(
    'id' => 'footer',
    'name' => __( 'Footer Widgets', 'bonestheme' ),
    'description' => __( 'These are the widgets that appear in the footer (3 max)', 'bonestheme' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s footer-widget">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));

  register_sidebar(array(
    'id' => 'blog',
    'name' => __( 'Blog Sidebar', 'bonestheme' ),
    'description' => __( 'These are the widgets displayed in the blog sidebar', 'bonestheme' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));

  register_sidebar(array(
    'id' => 'faq',
    'name' => __( 'FAQ Sidebar', 'bonestheme' ),
    'description' => __( 'These are the widgets displayed in the faq sidebar', 'bonestheme' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s faq-widget">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));


} //End bones_register_sidebars()


/*--------------------------------------------
Improved Comment Layout
---------------------------------------------*/

function bones_comments( $comment, $args, $depth ) {
   $GLOBALS['comment'] = $comment; ?>
  <div id="comment-<?php comment_ID(); ?>" <?php comment_class('cf'); ?>>
    <article  class="cf">
      <header class="comment-author vcard">
        <?php
        /*
          this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
          echo get_avatar($comment,$size='32',$default='<path_to_url>' );
        */
        ?>
        <?php // custom gravatar call ?>
        <?php
          // create variable
          $bgauthemail = get_comment_author_email();
        ?>
        <img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5( $bgauthemail ); ?>?s=40" class="load-gravatar avatar avatar-48 photo" height="40" width="40" src="<?php echo get_template_directory_uri(); ?>/assets/images/nothing.gif" />
        <?php // end custom gravatar call ?>
        <?php printf(__( '<cite class="fn">%1$s</cite> %2$s', 'bonestheme' ), get_comment_author_link(), edit_comment_link(__( '(Edit)', 'bonestheme' ),'  ','') ) ?>
        <time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__( 'F jS, Y', 'bonestheme' )); ?> </a></time>

      </header>
      <?php if ($comment->comment_approved == '0') : ?>
        <div class="alert alert-info">
          <p><?php _e( 'Your comment is awaiting moderation.', 'bonestheme' ) ?></p>
        </div>
      <?php endif; ?>
      <section class="comment_content cf">
        <?php comment_text() ?>
      </section>
      <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </article>
  <?php // </li> is added by WordPress automatically ?>
<?php
} //End bones_comments()

/*--------------------------------------------
Integer Money Conversion
---------------------------------------------*/

function toMoney($val,$symbol='$',$r=0) {
    $n = $val;
    $c = is_float($n) ? 1 : number_format($n,$r);
    $d = '.';
    $t = ',';
    $sign = ($n < 0) ? '-' : '';
    $i = $n=number_format(abs($n),$r);
    $j = (($j = $i.length) > 3) ? $j % 3 : 0;


    if ($val == 'Confidential' ){
      return  'Confidential';
    } else {
      return  $symbol.$sign .($j ? substr($i,0, $j) + $t : '').preg_replace('/(\d{3})(?=\d)/',"$1" + $t,substr($i,$j)) ;
    }

}


/*--------------------------------------------
Insert Attachment to Post
---------------------------------------------*/

function insert_attachment($file_handler,$post_id,$setthumb='false') {

  // check to make sure its a successful upload
  if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK) __return_false();

  require_once(ABSPATH . "wp-admin" . '/includes/image.php');
  require_once(ABSPATH . "wp-admin" . '/includes/file.php');
  require_once(ABSPATH . "wp-admin" . '/includes/media.php');

  $attach_id = media_handle_upload( $file_handler, $post_id );

  if ($setthumb) update_post_meta($post_id,'_thumbnail_id',$attach_id);
  return $attach_id;
}



/*--------------------------------------------
Declaring Icons for CPTs
---------------------------------------------*/

  function add_menu_icons_styles(){ ?>
  <style>
      #adminmenu .menu-icon-custom_type div.wp-menu-image:before { content: "\f491"; }
      #adminmenu .menu-icon-cordisco_faq div.wp-menu-image:before { content: "\f339"; }
      #adminmenu .menu-icon-cordisco_results div.wp-menu-image:before { content: "\f483"; }
      #adminmenu .menu-icon-cordisco_testimonial div.wp-menu-image:before { content: "\f487"; }
      #adminmenu .menu-icon-cordisco_attorneys div.wp-menu-image:before { content: "\f338"; }
      #adminmenu .menu-icon-cordisco_videos div.wp-menu-image:before { content: "\f03d"; }
  </style>
  <?php }
  add_action( 'admin_head', 'add_menu_icons_styles' );


/*--------------------------------------------
Adding Parent Class to Menu Items
 - Used for mobile menu arrows
---------------------------------------------*/

add_filter( 'wp_nav_menu_objects', 'add_menu_parent_class' );
function add_menu_parent_class( $items ) {
  $parents = array();
  foreach ( $items as $item ) {
    if ( $item->menu_item_parent && $item->menu_item_parent > 0 ) {
      $parents[] = $item->menu_item_parent;
    }
  }
  foreach ( $items as $item ) {
    if ( in_array( $item->ID, $parents ) ) {
      $item->classes[] = 'menu-parent-item';
    }
  }
  return $items;
}


/*--------------------------------------------
Bringing in CMB2
---------------------------------------------*/

require_once dirname( __FILE__ ) . '/assets/cmb2/init.php';

function update_cmb_meta_box_url( $url ) {
    $url = '/wp-content/themes/cordisco-saile/assets/cmb2';
    return $url;
}
add_filter( 'cmb2_meta_box_url', 'update_cmb_meta_box_url' );



/*--------------------------------------------
Preventing Null Search Queries
---------------------------------------------*/

add_filter( 'request', 'no_empty_search' );
function no_empty_search( $query_vars ) {
    if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
        $query_vars['s'] = " ";
    }
    return $query_vars;
}


/*---------------------------------
Add Tag to Meta Description & Category
----------------------------------*/
function hennessey_description_tag($s){
 if( is_tag() ){
    $s .= single_tag_title(" Posts Tagged as: ", false);
 }
 if( is_category() ){
    $s .= single_cat_title(" Posts Categorized as: ", false);
 }

 if ( is_tax('cordisco_faq_cat') ){
    $s = single_term_title(" FAQs Categorized as: ", false);
 }

if ( is_tax('cordisco_page_cat') ){
    $s = single_term_title(" Pages Categorized as: ", false);
 }

 return $s;
}
add_filter( 'wpseo_metadesc', 'hennessey_description_tag', 100, 1 );


/*--------------------------------------------
Adding Page Number to Description
---------------------------------------------*/

if ( ! function_exists( 't5_add_page_number' ) )
{
    function t5_add_page_number( $s )
    {
        global $page;
        $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
        ! empty ( $page ) && 1 < $page && $paged = $page;

        $paged > 1 && $s .= ' | ' . sprintf( __( 'Page: %s' ), $paged );

        return $s;
    }

    //add_filter( 'wp_title', 't5_add_page_number', 100, 1 );
    add_filter( 'wpseo_metadesc', 't5_add_page_number', 100, 1 );
}

/*--------------------------------------------
Custom Excerpt Length
---------------------------------------------*/

function my_excerpt($excerpt_length = 55, $id = false, $echo = true) {
    $text = '';

                global $post;
                $text = get_the_content('');


                $text = strip_shortcodes( $text );
                $text = apply_filters('the_content', $text);
                $text = str_replace(']]>', ']]&gt;', $text);
                $text = strip_tags($text);
                $excerpt_more = ' ' . '... <a href="'.get_the_permalink().'">Read More &raquo;</a>';
                $words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
                if ( count($words) > $excerpt_length ) {
                        array_pop($words);
                        $text = implode(' ', $words);
                        $text = $text . $excerpt_more;
                } else {
                        $text = implode(' ', $words);
                }
        if($echo)
        echo  apply_filters('the_content', $text);
        else
        return '<p>'.$text.'</p>';
}

function get_my_excerpt($excerpt_length = 55, $id = false, $echo = false) {
  return my_excerpt($excerpt_length, $id, $echo);
}


/*--------------------------------------------
Improved Page Navigation
---------------------------------------------*/

function numeric_posts_nav() {

  if( is_singular() )
    return;

  global $wp_query;

  /** Stop execution if there's only 1 page */
  if( $wp_query->max_num_pages <= 1 )
    return;

  $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
  $max   = intval( $wp_query->max_num_pages );

  /** Add current page to the array */
  if ( $paged >= 1 )
    $links[] = $paged;

  /** Add the pages around the current page to the array */
  if ( $paged >= 3 ) {
    $links[] = $paged - 1;
    $links[] = $paged - 2;
  }

  if ( ( $paged + 2 ) <= $max ) {
    $links[] = $paged + 2;
    $links[] = $paged + 1;
  }

  echo '<div class="pagination"><ul>' . "\n";

  /** Previous Post Link */
  if ( get_previous_posts_link() )
    printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

  /** Link to first page, plus ellipses if necessary */
  if ( ! in_array( 1, $links ) ) {
    $class = 1 == $paged ? ' class="active"' : '';

    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

    if ( ! in_array( 2, $links ) )
      echo '<li><span>...</span></li>';
  }

  /** Link to current page, plus 2 pages in either direction if necessary */
  sort( $links );
  foreach ( (array) $links as $link ) {
    $class = $paged == $link ? ' class="active"' : '';
    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
  }

  /** Link to last page, plus ellipses if necessary */
  if ( ! in_array( $max, $links ) ) {
    if ( ! in_array( $max - 1, $links ) )
      echo '<li><span>...</span></li>' . "\n";

    $class = $paged == $max ? ' class="active"' : '';
    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
  }

  /** Next Post Link */
  if ( get_next_posts_link() )
    printf( '<li>%s</li>' . "\n", get_next_posts_link() );

  echo '</ul></div>' . "\n";

}


/*--------------------------------------------
Grabbing Post Meta
---------------------------------------------*/
function getPostMeta($metaKey){
  if ( get_post_meta( $post->ID, '$metaKey', true ) ){
     return get_post_meta( $post->ID, '$metaKey', true );
  } else {
    return false;
  }
}

/*--------------------------------------------
Adding Favicon
---------------------------------------------*/
function hennessey_add_favicons(){
?>
  <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/manifest.json">
  <meta name="theme-color" content="#ffffff">
<?php
}

add_action('wp_head', 'hennessey_add_favicons');


/*--------------------------------------------
Adding Additional Post Types
---------------------------------------------*/

//FAQs
require_once('assets/post-types/faqs.php');

//General Page Types
require_once('assets/post-types/page.php');

//Case Results
require_once('assets/post-types/case-results.php');

//Testimonials
require_once('assets/post-types/testimonials.php');

//Attorneys
require_once('assets/post-types/attorneys.php');

//Videos
require_once('assets/post-types/videos.php');



/*--------------------------------------------
Adding Shortcodes
---------------------------------------------*/

require_once('assets/shortcodes/widget-ebook.php');
require_once('assets/shortcodes/widget-faq.php');
require_once('assets/shortcodes/widget-blog.php');
require_once('assets/shortcodes/temp-practice-list.php');
require_once('assets/shortcodes/footer-contact-us.php');
require_once('assets/shortcodes/display-local-number.php');
require_once('assets/shortcodes/ebook-infusionsoft-forms.php');
require_once('assets/shortcodes/avvo-buttons.php');
require_once('assets/shortcodes/infusionsoft-contact-forms.php');
require_once('assets/shortcodes/random-faq-image.php');

require_once('assets/shortcodes/locations.php');

/*--------------------------------------------
Adding Widgets
---------------------------------------------*/

require_once('assets/widgets/near-me-widget.php');
require_once('assets/widgets/blog-sidebar.php');
require_once('assets/widgets/faq-sidebar.php');

/*--------------------------------------------
Adding Video JS and Lightbox JS to Homepage
-- Note removed homepage classifier to prevent errors w/ autoptimize on other pages
---------------------------------------------*/

function hennessey_enqueue_homepage_video_js(){
    wp_enqueue_script('hennessey-video-js', get_template_directory_uri() . '/assets/js/vendor/video.js', array('jquery'), '1.0', true);
    wp_enqueue_script('hennessey-lightbox-js', get_template_directory_uri() . '/assets/js/vendor/lightbox.js', array('jquery'), '1.0', true);
}
//add_action( 'wp_enqueue_scripts', 'hennessey_enqueue_homepage_video_js', 40);


/*--------------------------------------------
Adding JS & Other Map Dependencies to Homepage

- Google Maps API Key - AIzaSyDcXR2oai8IV_1VilhTOKBbhYJ7JMYpYgo
- Infobox JS/CSS in vendor folders
---------------------------------------------*/

function hennessey_enqueue_map_js(){
  if  ( is_front_page() ) {
    wp_enqueue_script('hennessey-map-js', get_template_directory_uri() . '/assets/js/scripts/homepage-map.js', array('jquery'), '1.0', true);
    wp_enqueue_script('hennessey-infobox-js', get_template_directory_uri() . '/assets/js/vendor/map-infobox.js', array('jquery'), '1.0', true);
  }
}
//add_action( 'wp_enqueue_scripts', 'hennessey_enqueue_map_js', 40);


function hennessey_add_map_js(){
  if( is_front_page() ) :
?>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDcXR2oai8IV_1VilhTOKBbhYJ7JMYpYgo&extension=.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/scripts/homepage-map.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/vendor/map-infobox.js"></script>
<?php
  endif;
}
//add_action('wp_footer', 'hennessey_add_map_js');

/*---------------------------------
Bringing in Modules
----------------------------------*/
require_once('assets/modules/widget-location-list/location-module.php');

/*---------------------------------
Adding nGage Chat
----------------------------------*/
function hennessey_add_ngage_chat(){
  //Only Load For Non-Logged In Users
  //if ( !is_user_logged_in() && !is_page( array(2576, 269) ) ) :
/*  if ( !is_page( '2576' ) ) :
  ?>

    <script>(function(ng,a,g,e){var l=document.createElement(g);l.async=1;l.src =(ng+e);var c=a.getElementsByTagName(g)[0];c.parentNode.insertBefore(l,c);})("https://messenger.ngageics.com/ilnksrvr.aspx?websiteid=",document,"script","56-49-236-10-48-122-52-187");</script>
  <?php
  endif;
}*/
}

add_action('wp_footer', 'hennessey_add_ngage_chat');

/*---------------------------------
Adding Google Analytics
----------------------------------*/

function hennessey_add_google_analytics(){
?>

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-94083004-1', 'auto');
    ga('send', 'pageview');

  </script>

<?php
}

add_action('wp_head','hennessey_add_google_analytics');


/*---------------------------------
Adding Facebook Pixel Tracking
----------------------------------*/
function hennessey_add_facebook_pixel(){
?>

<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '124193298237070');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=124193298237070&ev=PageView&noscript=1"
/></noscript>


<?php
}

add_action('wp_head','hennessey_add_facebook_pixel');


/*---------------------------------
Adding AdWords Remarketing
- Removed 1/15/19 - Client no longer using
----------------------------------*/

function hennessey_add_adwords_remarketing() {
?>
<!--noptimize-->
<!-- Google Code for Remarketing Tag -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 945967154;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/945967154/?guid=ON&amp;script=0"/>
</div>
</noscript>
<!--/noptimize-->
<?php
}
//add_action('wp_footer', 'hennessey_add_adwords_remarketing');

/*---------------------------------
Adding LinkedIn Insights Tracking
- Removed 1/14/19
---------------------------------*/

function hennessey_add_linkedin_insights(){
  ?>

  <script type="text/javascript">
  _linkedin_data_partner_id = "57521";
  </script><script type="text/javascript">
  (function(){var s = document.getElementsByTagName("script")[0];
  var b = document.createElement("script");
  b.type = "text/javascript";b.async = true;
  b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
  s.parentNode.insertBefore(b, s);})();
  </script>
  <noscript>
  <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=57521&fmt=gif" />
  </noscript>

  <?php
}

//add_action('wp_footer', 'hennessey_add_linkedin_insights');


/*--------------------------------------------
Adding Geo Information
---------------------------------------------*/

function hennessey_add_geo(){
?>

  <?php if( is_page( '3646' ) ) { ?>

      <meta name="zipcode" content="19047" />
      <meta name="city" content="Langhorne" />
      <meta name="state" content="PA" />
      <meta name="geo.region" content="US-PA" />
      <meta name="geo.placename" content="Langhorne" />
      <meta name="geo.position" content="40.221595;-74.908635" />
      <meta name="ICBM" content="40.221595, -74.908635" />

  <?php } else if ( is_page('3645') ) { ?>

        <meta name="zipcode" content="19053" />
        <meta name="city" content="Trevose" />
        <meta name="state" content="PA" />
        <meta name="country" content="usa, united states of america" />
        <meta name="geo.region" content="US-PA" />
        <meta name="geo.placename" content="Feasterville-Trevose" />
        <meta name="geo.position" content="40.142918;-74.963982" />
        <meta name="ICBM" content="40.142918, -74.963982" />

  <?php } else { ?>

      <meta name="zipcode" content="19126, 19125, 19128, 19127, 19130, 19129, 19132, 19131, 19134, 19133, 19136, 19135, 19138, 19137, 19140, 19139, 19142, 19141, 19144, 19143, 19146, 19145, 19148, 19147, 19150, 19149, 19152, 19151, 19154, 19153, 19155, 19170, 19173, 19019, 19176, 19187, 19192, 19101, 19099, 19103, 19102, 19105, 19104, 19107, 19106, 19109, 19111, 19112, 19115, 19114, 19118, 19116, 19120, 19119, 19122, 19121, 19124, 19123, 19047, 19053, 19007, 18940" />
      <meta name="city" content="Langhorne, Trevose, Bristol, Newtown, Philadelphia" />
      <meta name="state" content="PA" />
      <meta name="country" content="usa, united states of america" />
      <meta name="geo.region" content="US-PA" />
      <meta name="geo.placename" content="Feasterville-Trevose" />
      <meta name="geo.position" content="40.142918;-74.963982" />
      <meta name="ICBM" content="40.142918, -74.963982" />

  <?php } ?>

<?php
}

add_action('wp_head', 'hennessey_add_geo');

/*--------------------------------------------
Adding CallRail
---------------------------------------------*/

function hennessey_add_callrail(){
?>

<script type="text/javascript" src="//cdn.callrail.com/companies/185614293/2f56520a87766037a099/12/swap.js" async></script>

<?php
}

add_action('wp_footer', 'hennessey_add_callrail');

/*--------------------------------------------
Adding Business Schema
---------------------------------------------*/

function hennessey_add_business_schema(){
?>

  <script type='application/ld+json'>
  {
    "@context": "http://www.schema.org",
    "@type": "Attorney",
    "name": "Cordisco & Saile LLC",
    "url": "<?php echo site_url(); ?>",
    "sameAs": [
      "https://www.facebook.com/pages/Cordisco-Law-LLC/335095316644989",
      "https://plus.google.com/116305063492805400807/posts",
      "https://twitter.com/cordiscosaile",
      "https://www.linkedin.com/in/johncordiscoesq"


    ],
    "logo": "<?php echo get_template_directory_uri(); ?>/assets/images/cordisco-saile-logo.png",
    "image": "<?php echo site_url(); ?>/wp-content/uploads/2017/03/cordisco-saile-team.jpg",
    "address": [
        {
          "@type": "PostalAddress",
          "streetAddress": "403 Executive Dr #100",
          "addressLocality": "Langhorne",
          "addressRegion": "PA",
          "postalCode": "19047",
          "addressCountry": "United States"
        },
      {
        "@type": "PostalAddress",
        "streetAddress": "900 Northbrook Dr #120 ",
        "addressLocality": "Trevose",
        "addressRegion": "PA",
        "postalCode": "19053",
        "addressCountry": "United States"
      },
      {
        "@type": "PostalAddress",
        "streetAddress": "220 Radcliffe St #100",
        "addressLocality": "Bristol",
        "addressRegion": "PA",
        "postalCode": "19007",
        "addressCountry": "United States"
      },
      {
        "@type": "PostalAddress",
        "streetAddress": "44 E Court St #100 ",
        "addressLocality": "Doylestown",
        "addressRegion": "PA",
        "postalCode": "19053",
        "addressCountry": "United States"
      }
    ],
    "openingHoursSpecification" : {
      "@type" : "OpeningHoursSpecification",
      "dayOfWeek" : [ "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" ],
      "opens" : "00:00:00",
      "closes" : "23:59:59"
    },

    "telephone": ["
    <?php
      if( is_page('3646') ) {
        echo '215-642-2336';
      } else {
          echo '215-642-2335';
      }
    ?>
    "],
    "priceRange": "$$"
  }

   </script>

<?php
}

add_action('wp_head', 'hennessey_add_business_schema');

/*---------------------------------
Filtering Yoast Breadcrumbs For FAQ URL update
----------------------------------*/
function hennessey_yoast_faq_breadcrumb_mod($crumb){
  $faqURLOriginal = site_url() . '/faq/';
  $faqURLNew = site_url() . '/faqs/';

  $crumb = str_replace($faqURLOriginal, $faqURLNew, $crumb);

  return $crumb;

}
add_filter('wpseo_breadcrumb_output', 'hennessey_yoast_faq_breadcrumb_mod', 10, 1);

/*--------------------------------------------
Small Fix for Possible Relevanssi "Did you mean" Crash
---------------------------------------------*/
add_filter('relevanssi_get_words_having', 'fix_query');
function fix_query($c) {
    return 2;
}

/*--------------------------------------------
Modify Search Form
---------------------------------------------*/

function hennessey_search_form( $form ) {

  $form = '<form role="search" method="get" class="search-form search-box" action="' . esc_url( home_url( '/' ) ) . '">
  <input type="text" placeholder="Search the Site" name="s" value="" aria-label="Search Input">
  <input type="submit" class="search-submit" value="" aria-label="Search Submit">
  </form>';

  return $form;

}

add_filter( 'get_search_form', 'hennessey_search_form', 10 );

/*--------------------------------------------
Search Schema
---------------------------------------------*/

function hennessey_add_search_schema() {
?>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebSite",
    "url": "<?php echo site_url(); ?>",
    "potentialAction": {
      "@type": "SearchAction",
      "target": "<?php echo site_url(); ?>?s={search_term_string}",
      "query-input": "required name=search_term_string"
    }
  }
  </script>
<?php
}
//Now added by Yoast
//add_action('wp_head', 'hennessey_add_search_schema');


/*************************************************
Autoptimize Ignore AMP Pages
**************************************************/

add_filter('autoptimize_filter_noptimize','my_ao_noptimize',10,0);
function my_ao_noptimize() {
  if (  (strpos($_SERVER['REQUEST_URI'],'faq')!==false) ||
        (strpos($_SERVER['REQUEST_URI'],'amp')!==false) ||
        (strpos($_SERVER['REQUEST_URI'],'discusses')!==false) ||
        (strpos($_SERVER['REQUEST_URI'],'test')!==false)
    ) {
    return true;
  } else {
    return false;
  }
}


/*************************************************
Adding AdWords Conversion Code
- Removed 1/15/19 - Client no longer using
**************************************************/

function hennessey_add_adwords_conversion() {

  if ( is_page( array('3365','3360', '3362', '3633', '3634') )  ) {
  ?>

  <!--noptimize-->
  <!-- Google Code for Home Page - Form Submit Conversion Page -->
  <script type="text/javascript">
  /* <![CDATA[ */
  var google_conversion_id = 945967154;
  var google_conversion_language = "en";
  var google_conversion_format = "3";
  var google_conversion_color = "ffffff";
  var google_conversion_label = "jitbCJOevXIQsqCJwwM";
  var google_remarketing_only = false;
  /* ]]> */
  </script>
  <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
  </script>
  <noscript>
  <div style="display:inline;">
  <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/945967154/?label=jitbCJOevXIQsqCJwwM&amp;guid=ON&amp;script=0"/>
  </div>
  </noscript>
  <!--/noptimize-->
  <?php
  }
}
//add_action('wp_footer', 'hennessey_add_adwords_conversion');

/*--------------------------------------------
Adding Custom AMP Template
---------------------------------------------*/

add_filter( 'amp_post_template_file', 'hennessey_set_amp_template', 10, 3 );

function hennessey_set_amp_template( $file, $type, $post ) {

  if ( ('single' === $type) || ('page' === $type) ) {
    $file = dirname( __FILE__ ) . '/assets/amp/template.php';
  }
  return $file;
}

/*--------------------------------------------
Adding AMP Menu
---------------------------------------------*/

  register_nav_menus(
    array(
      'amp-nav' => __( 'The Amp Menu', 'bonestheme' )
    )
  );



// Related Pages Module
// Disabled at request of client, was interfering with their PPC ad approval
//require_once('assets/modules/rs-related-pages-module.php');

// Add page number into titles

add_filter('wpseo_title', 'filter_product_wpseo_title');
function filter_product_wpseo_title($title) {
    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
    if( strpos($title, 'Blog') !== false && $paged > 1) {
        $title = 'Blog - Page '.$paged.' | Cordisco & Saile';
    }
    return $title;
}


add_filter( 'amp_post_template_metadata', 'amp_modify_json_metadata', 10, 2 ); // Adding custom metadata
function amp_modify_json_metadata( $metadata, $post ) {

   if( 'post'=== $post->post_type  ){
      $metadata['@type'] = 'Article';
    }

    $metadata['image'] = get_stylesheet_directory_uri().'/amp/amp-logo.png';

   $metadata['publisher']['name'] = 'Cordisco Saile Team';

    $metadata['publisher']['logo'] = array(
        '@type' => 'ImageObject',
        'url' => get_stylesheet_directory_uri().'/amp/amp-logo.png',
        'height' => 60,
        'width' => 256,
    );

   return $metadata;
}

/*--------------------------------------------
Filter AMP URL in wp_head
- Update output of amp_frontend_add_canonical() so posts exist at /?amp
- Find a better way, but this works :)
---------------------------------------------*/

function start_wp_head_buffer() {
    ob_start();
}
add_action('wp_head','start_wp_head_buffer',0);

function end_wp_head_buffer() {
    $in = ob_get_clean();
    $in = str_replace('/amp/','/?amp',$in);
    echo $in;
}
add_action('wp_head','end_wp_head_buffer', PHP_INT_MAX);


/*--------------------------------------------
Adding Adroll PPC Snippet
---------------------------------------------*/

//add_action('wp_footer', 'hennessey_add_addroll_ppc');
function hennessey_add_addroll_ppc(){
?>
<script type="text/javascript">

    adroll_adv_id = "FN7EJCTVQ5FUTFJ2CVZNGY";
    adroll_pix_id = "PNJJAY6AJBE4FN2IINE3SH";

    (function () {
        var _onload = function(){
            if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
            if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
                document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
        };
        if (window.addEventListener) {window.addEventListener('load', _onload, false);}
        else {window.attachEvent('onload', _onload)}
    }());
</script>
<?php
}


//Disable gutenberg style in Front
function wps_deregister_styles() {
    wp_dequeue_style( 'wp-block-library' );
}
add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );

/*--------------------------------------------
Remove Category UNcategorized
---------------------------------------------*/
/* add_action( 'init', function()
{
    // Get all the posts which is assigned to the uncategorized category
    $args = [
        'posts_per_page' => -1, // Adjust as needed
        'cat'            => 1, // Category ID for uncategorized category
        'fields'         => 'ids', // Only get post ID's for performance
        // Add any additional args here, see WP_Query
    ];
    $q = get_posts( $args );

    // Make sure we have posts
    if ( !$q )
        return;

    // We have posts, lets loop through them and remove the category
    foreach ( $q as $id )
        wp_remove_object_terms(
            $id, // Post ID
            1, // Term ID to remove
            'category' // The taxonomy the term belongs to
        );
}, PHP_INT_MAX ); */


/*--------------------------------------------
 __       _                  _          _
/ _\_ __ (_)_ __  _ __   ___| |_ ___   / \
\ \| '_ \| | '_ \| '_ \ / _ \ __/ __| /  /
_\ \ | | | | |_) | |_) |  __/ |_\__ \/\_/
\__/_| |_|_| .__/| .__/ \___|\__|___/\/
           |_|   |_|
---------------------------------------------*/

/*--------------------------------------------

Simple Loop for Custom Post Type


<?php
    $args = array(
        'posts_per_page' => 1,
        'post_type' => 'presidents_letter',
        'order' => 'DSC',
    );

    $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
?>
<!-- MAIN CONTENT HERE -->
<?php endwhile; else : ?>
<!-- IF NOTHING FOUND CONTENT HERE -->
<?php endif; ?>
<?php wp_reset_query(); ?>

---------------------------------------------*/

/*--------------------------------------------

Alternate Page Limits For Things


function custom_query_additions( $query ) {

  // Check if on frontend and main query is modified
     if ( is_post_type_archive( 'presidents_letter' ) ) {
            // Display 50 posts for a custom post type called 'movie'
            $query->set( 'posts_per_page', 50 );
            return;
      }
  }
 add_action( 'pre_get_posts', 'custom_query_additions' );

---------------------------------------------*/


/* End of File */ ?>

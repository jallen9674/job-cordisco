<div id="sidebar" class="sidebar content-container faq-sidebar">

	<?php if ( is_active_sidebar( 'faq' ) ) : ?>

		<?php dynamic_sidebar( 'faq' ); ?>

	<?php else : ?>

		<?php
			/*
			 * This content shows up if there are no widgets defined in the backend.
			*/
		?>

		<div class="no-widgets">
			<p>These are not the widgets you are looking for...</p>
		</div>

	<?php endif; ?>

	<?php
			echo do_shortcode('[cordisco-random-faq-loop-image amount="4"]');
	?>
	<div class="widget widget_cordisco_city_widget"><div class="near-me-widget-box"><h2 class="widgettitle">Find a Reliable <br> Lawyer Now!</h2>



	<ul class="near-me-widget-list">

        <?php //Output a list of 10 random cities ?>

          <?php
            // get all terms in the taxonomy
            $terms = get_terms( 'hc_location' );
            // convert array of term objects to array of term IDs
            $term_ids = wp_list_pluck( $terms, 'term_id' );
            global $post;

            

            remove_all_filters('posts_orderby');
            $args = array(
                    'post_type' => 'page',
                    'posts_per_page' => 10,
                    'orderby' => 'rand',
                    'order' => 'ASC',
                    'hc_title' => 'Personal Injury',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'hc_location',
                            'field'    => 'term_id',
                            'terms'    => $term_ids,
                        ),
                    )
                );

            $query = new WP_Query($args);

            $max = $query->post_count;
            $rand_main_page = rand(0, $max-1);
            $l = 0;
            if($query->have_posts()):  while($query->have_posts()):$query->the_post();


                //Get the Link Title
                $linkTitle = get_post_meta( $post->ID, '_hc_location_widget_title', true );

                //Get Location Title
                $postTerms =  wp_get_object_terms($post->ID, 'hc_location');

                $categoryFilterSlug = '';
                $categoryPrettyName = '';

                if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
                     foreach ( $postTerms as $term ) {
                       $categoryFilterSlug .= '' . $term->slug;
                       $categoryPrettyName .= ' ' . $term->name;
                     }
                 }
            ?>

            <?php if (1 || $linkTitle !== 'General Information' ) :?>
                <li>
                    <a href="<?php the_permalink(); ?>">
                        <?php echo $categoryPrettyName . '  ' . $linkTitle; ?> Lawyer
                    </a>
                </li>
            <?php endif; 
            if($l == $rand_main_page) { ?>
                <li>
                    <a href="/">
                        Bucks County Personal Injury Lawyer
                    </a>
                </li>
            <?php }
            $l++;
            ?>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
        <?php endif; ?>

    </ul>

    </div> </div>

</div>

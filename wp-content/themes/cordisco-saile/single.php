<?php get_header(); ?>



	<div id="inner-content" class="wrapper">

		<div id="main" class="content-container" role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<?php //Blog Post Meta ?>
				<?php
					if ( has_post_thumbnail() ) {
						$imageURL = get_the_post_thumbnail_url('full');
					} else {
						$imageURL = get_template_directory_uri() . '/assets/images/mobile-homepage-video-background.jpg';
					}

					$blogContent = strip_tags(get_the_content());
					$blogContent = str_replace('"', "", $blogContent);
					$blogContent = str_replace("'", "", $blogContent);

					function shorten_string($string, $wordsreturned) {
						$retval = $string;
						$array = explode(" ", $string);
						if (count($array)<=$wordsreturned) {
							$retval = $string;
						} else {
							array_splice($array, $wordsreturned);
							$retval = implode(" ", $array)." ...";
						}
						return $retval;
					}

					$blogExcerpt = shorten_string($blogContent, 30);
				 ?>

				 <script type="application/ld+json">
				{ "@context": "http://schema.org",
				 "@type": "BlogPosting",
				 "headline": "<?php the_title(); ?>",
				 "image": "<?php echo $imageURL; ?>",
				 "publisher": {
				    "@type": "Organization",
				    "name": "Cordisco & Saile, LLC",
				    "logo": {
				    	"@type": "imageObject",
				    	"url": "<?php echo get_template_directory_uri() . '/assets/images/cordisco-saile-schema.png'?>",
				    	"width": "243",
				    	"height": "60"
				    }
				   },
				 "url": "<?php the_permalink(); ?>",
				 "datePublished": "<?php echo get_the_date('c',$post->ID); ?>",
				 "dateCreated": "<?php echo get_the_date('c',$post->ID); ?>",
				 "dateModified": "<?php echo get_the_date('c',$post->ID); ?>",
				 "description": "<?php echo $blogExcerpt; ?>",
				 "articleBody": "<?php echo $blogContent; ?>",
				   "author": {
				    "@type": "Person",
				    "name": "Mike Cordisco"
				  },
				  "mainEntityOfPage": {
				    "@type": "WebPage",
				    "id": "<?php the_permalink(); ?>"
				  }
				 }
				</script>


				<?php //End Blog Post Meta ?>

 				<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> >

	                <header class="article-header entry-header">
					  <h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
	                  <p class="byline entry-meta vcard">

	                    <?php printf( __( 'Posted', 'bonestheme' ).' %1$s %2$s',
	                       /* the time the post was published */
	                       '<time class="updated entry-time" datetime="' . get_the_time('Y-m-d') . '">' . get_the_time(get_option('date_format')) . '</time>',
	                       /* the author of the post */
	                       '<span class="by">'.__( 'by', 'bonestheme' ).'</span> <span class="entry-author author">' . get_the_author_link( get_the_author_meta( 'ID' ) ) . '</span>'
	                    ); ?>

	                  </p>

	                </header> <?php // end article header ?>

	                <section class="entry-content cf" itemprop="articleBody">
	                  <?php
	                    // the content (pretty self explanatory huh)
	                    the_content();

	                    /*
	                     * Link Pages is used in case you have posts that are set to break into
	                     * multiple pages. You can remove this if you don't plan on doing that.
	                     *
	                     * Also, breaking content up into multiple pages is a horrible experience,
	                     * so don't do it. While there are SOME edge cases where this is useful, it's
	                     * mostly used for people to get more ad views. It's up to you but if you want
	                     * to do it, you're wrong and I hate you. (Ok, I still love you but just not as much)
	                     *
	                     * http://gizmodo.com/5841121/google-wants-to-help-you-avoid-stupid-annoying-multiple-page-articles
	                     *
	                    */
	                    wp_link_pages( array(
	                      'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'bonestheme' ) . '</span>',
	                      'after'       => '</div>',
	                      'link_before' => '<span>',
	                      'link_after'  => '</span>',
	                    ) );
	                  ?>
	                </section> <?php // end article section ?>

	                <footer class="article-footer">

	                  <?php printf( __( 'Categorized', 'bonestheme' ).': %1$s', get_the_category_list(', ') ); ?>



	                </footer> <?php // end article footer ?>

	                <?php //comments_template(); ?>

              </article> <?php // end article ?>


				<?php
				/*-------------------------------------------
				Display Related FAQs
				---------------------------------------------*/
				?>
				<h2 class="related-faq-title">Frequently Asked Questions</h2>
				<ul class="related-faq-list">
					<?php

					$faqTerms = get_terms( 'cordisco_faq_cat' );
	        // convert array of term objects to array of term IDs
	        $faqTermIDs = wp_list_pluck( $faqTerms, 'term_id' );

					$args = array(
						'posts_per_page' => '5',
						'post_type' => 'cordisco_faq',
						'tax_query' => array(
							array(
								'taxonomy' => 'cordisco_faq_cat',
								'field' => 'term_id',
								'terms' => $faqTermIDs
							),
						),
						'order' => 'DSC',
						'orderby' => 'rand',
					);

					$the_query = new WP_Query( $args );
					if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
					?>
					<?php //Getting Category for Filtering
					$postTerms =  wp_get_object_terms($post->ID, 'cordisco_faq_cat');
					$categoryFilterSlug = '';
					$categoryPrettyName = '';
					if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
						foreach ( $postTerms as $term ) {
							$categoryFilterSlug .= ' ' . $term->slug;
							$categoryPrettyName .= ' ' . $term->name . '<span class="divider">, </span>';
						}
					}
					?>

					<li><a href="<?php the_permalink(); ?>" class="faq-filter-title-link"><?php the_title(); ?></a></li>

				<?php endwhile; else : ?>
					<p>Sorry no related FAQs found.</p>
				<?php endif; ?>
				<?php wp_reset_query(); ?>
			</ul>

			<?php //End Related FAQ Loop ?>

			<?php endwhile; ?>

			<?php else : ?>

				<article id="post-not-found" class="hentry cf">
						<header class="article-header">
							<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
						</header>
						<section class="entry-content">
							<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
						</section>
						<footer class="article-footer">
								<p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
						</footer>
				</article>

			<?php endif; ?>

		</div>

		<?php get_sidebar('blog'); ?>

	</div>



<?php get_footer(); ?>

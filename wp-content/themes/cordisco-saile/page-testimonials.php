<?php //Template Name: Testimonials Display ?>
<?php get_header(); ?>

	<div id="inner-content" class="wrapper">

			<div id="main" class="content-container">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?>>

					<header class="article-header">
						<h1 class="page-title" itemprop="headline">

							<span class="page-title__general-text">
								<?php the_title(); ?>
							</span>

							<span class="page-title__review-info-stars">
								★★★★★
							</span>

							<span class="page-title__review-info-text">
								<?php
									//Getting Totals to Display
									$onSiteTestimonials = wp_count_posts('cordisco_testimonial');
									$onSiteTestimonialsPublished = $onSiteTestimonials->publish;
									//
								?>
								5.0 (<?php echo $onSiteTestimonialsPublished; ?>)
							</span>

						</h1>
					</header>

					<section class="entry-content cf" itemprop="articleBody">

						<?php
							the_content();
						?>

						<?php /*----------- BEGIN DYNAMIC VERDICT LISTINGS -------------- */ ?>

										<?php
										/*------------------------------------
										Secondary Loop Pagination
										-------------------------------------*/

										//Usual WP Query Arguments here
										$custom_query_args  = array(
											'posts_per_page' => 15,
											'post_type' => 'cordisco_testimonial',
											'meta_key' => '_cs_testimonial_date',
											'orderby' => 'meta_value_num',
											'order' => 'DSC'
										);

										// Get current page and append to custom query parameters array
										$custom_query_args['paged'] = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

										$custom_query  = new WP_Query( $custom_query_args  );

										// Pagination fix
										$temp_query = $wp_query;
										$wp_query   = NULL;
										$wp_query   = $custom_query;

										if ( $custom_query ->have_posts() ) : while ( $custom_query ->have_posts() ) : $custom_query ->the_post();
										?>

										<?php //Start Output ?>

				            <div class="single-testimonial">
				            	<div class="single-testimonial__stars">
				            		<?php
				                        if ( get_post_meta( $post->ID, '_cs_testimonial_stars', true ) ) {

				                             $starCount = get_post_meta( $post->ID, '_cs_testimonial_stars', true );

				                             for ($i = 1; $i <= $starCount; $i++) {
											    echo '★';
											}

				                        } else {
				                        	echo '★★★★★';
				                        }
				                    ?>
				            	</div>
				                <div class="single-testimonial__description">
				                    <?php
				                        if ( get_post_meta( $post->ID, '_cs_testimonial_description_long', true ) ) {
				                             echo get_post_meta( $post->ID, '_cs_testimonial_description_long', true );
				                        }
				                    ?>
				                </div>
				                <div class="single-testimonial__author">
				                	<?php
				                        if ( get_post_meta( $post->ID, '_cs_testimonial_author', true ) ) {
				                             echo get_post_meta( $post->ID, '_cs_testimonial_author', true );
				                        }
				                    ?>
				                </div>
				                <div class="single-testimonial__link">
				                    <?php
				                        if ( get_post_meta( $post->ID, '_cs_testimonial_url', true ) && get_post_meta( $post->ID, '_cs_testimonial_url_title', true )  ) {

				                             echo '<a href="' .  get_post_meta( $post->ID, '_cs_testimonial_url', true ) . '">' . get_post_meta( $post->ID, '_cs_testimonial_url_title', true ) . '</a>';
				                        }
				                    ?>
				                </div>
				                <div class="single-testimonial__date">
				                    <?php
				                        if ( get_post_meta( $post->ID, '_cs_testimonial_date', true ) ) {
				                             $testimonialDate = get_post_meta( $post->ID, '_cs_testimonial_date', true );
				                             echo date('F jS, Y', $testimonialDate);
				                        }
				                    ?>
				                </div>
				            </div>
				            <hr class="solid-hr">
									<?php endwhile; ?>
									<?php numeric_posts_nav(); ?>
									<?php else : ?>
									<p>Sorry Nothing Found</p>
									<?php endif; ?>

									<?php
									wp_reset_query();
									wp_reset_postdata();

									// Reset main query object
									$wp_query = NULL;
									$wp_query = $temp_query;
									?>

					</section>

					<?php comments_template(); ?>

				</article>

				<?php endwhile; else : ?>

						<article id="post-not-found" class="hentry cf">
							<header class="article-header">
								<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
							</header>
							<section class="entry-content">
								<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
							</section>
							<footer class="article-footer">
									<p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
							</footer>
						</article>

				<?php endif; ?>

			</div>

			<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>

<?php
/*
 Template Name: Homepage Template
*/
?>

<?php get_header(); ?>


	<?php
	/*----------------------------------------
	Begin Statis Header Section
	- Revised 7/20/2018
	-----------------------------------------*/
	?>

	<div class="homepage-section homepage-hero-section">
		<div class="homepage-hero-section__inner">
			<div class="wrapper">

				<div class="homepage-wistia-video desktop">
					<script src="https://fast.wistia.com/embed/medias/us3uis5mzk.jsonp" async></script>
					<script src="https://fast.wistia.com/assets/external/E-v1.js" async></script>
					<span class="wistia_embed wistia_async_us3uis5mzk popover=true popoverContent=link" style="display:inline;position:relative">
						<a href="#"><i class="fa fa-play"></i></a>
					</span>
				</div>

				<div class="homepage-hero-section__info-section">

				<div class="homepage-hero-section__info-section-inner">
					<div class="homepage-hero-info">
						<span class="homepage-hero-info__title">
							WE <strong>NEVER</strong> STOP FIGHTING.
						</span>
						<span class="homepage-hero-info__sub-title">
							Your Bucks County Injury Lawyers
						</span>
						<span class="homepage-hero-info__info">
							Featuring Over 200 Five Star Ratings and Reviews from Past Clients
						</span>
						<div class="homepage-hero-info__testimonials">
							<div class="homepage-hero-info__testimonial-stars">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</div>
							<a href="<?php echo site_url(); ?>/testimonials/" class="homepage-hero-info__testimonial-link">Read Our Testimonials &raquo;</a>
						</div>
						<div class="homepage-hero-info__practice-areas">
							<a href="<?php echo site_url(); ?>/bucks-county-injury/car-accident-lawyer/" class="homepage-hero-info__practice-area">Motor Vehicle Accidents</a>
							<a href="<?php echo site_url(); ?>/bucks-county-injury/workers-compensation-lawyer/" class="homepage-hero-info__practice-area">Workers Compensation</a>
							<a href="<?php echo site_url(); ?>/bucks-county-injury/slip-and-fall-lawyer/" class="homepage-hero-info__practice-area">Slip and Falls</a>
							<a href="<?php echo site_url(); ?>/bucks-county-injury/nursing-home-abuse-lawyer/" class="homepage-hero-info__practice-area">Nursing Home Neglect</a>
						</div>
						<a href="<?php echo site_url(); ?>/practice-areas/" class="homepage-hero-info__practice-area-link">View More Practice Areas &raquo;</a>
					</div>
				</div>
				</div>
			</div>
		</div>

		<div class="homepage-hero-section__mobile-photo">
			<div class="homepage-wistia-video mobile">
					<span class="wistia_embed wistia_async_us3uis5mzk popover=true popoverContent=link" style="display:inline;position:relative">
						<a href="#"><i class="fa fa-play"></i></a>
					</span>
				</div>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage-hero-background-mobile.jpg" alt="Cordisco and Saile LLC" class="lozad">
		</div>
	</div>


	<?php
	/*----------------------------------------
	Begin Header Section w/ photos and form
	-----------------------------------------*/
	?>
	<?php /*
	<div class="homepage-section homepage-intro-section">
		<div class="wrapper">
			<div class="homepage-intro-section__inner">

				<?php // Original Mobile Header (No Slideshow) - Currently Disabled ?>

				<div class="homepage-intro-section__left show-mobile">
					<div class="homepage-intro-section__header-text">
						Locally Trusted.<br>Nationally Recognized.
					</div>
					<div class="homepage-intro-section__john-photo">
						<img data-src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/michael-saile-homepage-top-cropped.png" alt="John Cordisco">
					</div>
					<div class="homepage-intro-section__michael-photo">
						<img data-src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/john-cordisco-homepage-cropped.png" alt="Michael Saile">
					</div>
				</div>




				<div class="homepage-intro-section__left show-mobile homepage-desktop-video"> <?php //In A Hurry, Still Need to Fix Classes Here ?>
					<div class="homepage-intro-section__inner">
						<div class="slide-video">
							<a href="#homepage-video"  class="video-play-button popup-with-zoom-anim">
								<span class="homepage-intro-section__video-headline">
									We Never Stop Fighting.
								</span>
								<i class="fa fa-play"></i>
							</a>

							  <video id="homepage-video" class="video-js homepage-video vjs-big-play-centered mfp-hide" controls preload="auto" width="640" height="264"
							  poster="<?php echo get_template_directory_uri(); ?>/assets/video/homepage-video-placeholder.jpg" data-setup='{"fluid": true}'>
							    <source data-src="<?php echo get_template_directory_uri(); ?>/assets/video/cordisco-homepage-video.mp4" type='video/mp4'>
							    <p class="vjs-no-js">
							      To view this video please enable JavaScript, and consider upgrading to a web browser that
							      <a href="//videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
							    </p>
							   </video>
						</div>
					</div>
				</div>


				<?php //Desktop Header w/ Slideshow ?>
				<div class="homepage-intro-section__left show-desktop">
					<div class="flexslider homepage-desktop-slider homepage-desktop-video">

						<div class="slide-video">
							<a href="#homepage-video"  class="video-play-button popup-with-zoom-anim">
								<span class="homepage-intro-section__video-headline">
									We Never Stop Fighting.
								</span>
								<i class="fa fa-play"></i>
							</a>

							  <video id="homepage-video" class="video-js homepage-video vjs-big-play-centered mfp-hide" controls preload="auto" width="640" height="264"
							  poster="<?php echo get_template_directory_uri(); ?>/assets/video/homepage-video-placeholder.jpg" data-setup='{"fluid": true}'>
							    <source data-src="<?php echo get_template_directory_uri(); ?>/assets/video/cordisco-homepage-video.mp4" type='video/mp4'>
							    <p class="vjs-no-js">
							      To view this video please enable JavaScript, and consider upgrading to a web browser that
							      <a href="//videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
							    </p>
							   </video>
						</div>

					</div>
				</div>

				<div class="homepage-intro-section__right">
					<?php //echo do_shortcode('[contact-form-7 id="3364" title="Homepage Top Contact Form" html_class="homepage-top-contact-form"]'); ?>
					<?php //echo do_shortcode('[hc-contact-form-homepage]'); ?>
					<?php echo do_shortcode('[contact-form-7 id="3361" title="Sidebar Contact Form" html_class="sidebar-contact-form standard-form"]'); ?>
				</div>
			</div>
		</div>
	</div>

	*/ ?>

	<?php
	/*----------------------------------------
	End  Header Section w/ photos and form
	-----------------------------------------*/
	?>


	<?php
	/*----------------------------------------
	Begin Primary H1 Section
	-----------------------------------------*/
	?>

	<div class="homepage-section main-header-section">
		<div class="wrapper">
			<div class="main-header-section__inner">
				<h1>
					Bucks County Personal Injury Lawyers located in Bensalem, Newtown, Langhorne, Bristol, Doylestown, and Philadelphia, PA
				</h1>
			</div>
		</div>
	</div>


	<?php
	/*----------------------------------------
	End  Primary H1 Section
	-----------------------------------------*/
	?>

	<?php
	/*----------------------------------------
	Begin Trust Logo Section
	-----------------------------------------*/
	?>

	<div class="homepage-section trust-logo-section">
		<div class="wrapper">
			<div class="trust-logo-section__inner">
				<div class="trust-logo-section__image-wrapper">
					<img data-src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/trust-logos/litigator-awards-logo.png" alt="Litigator Awards Logo" class="trust-logo-section__logo lozad litigator-awards-logo">
				</div>
				<div class="trust-logo-section__image-wrapper">
					<img data-src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/trust-logos/av-logo.jpg" alt="AV Logo" class="trust-logo-section__logo lozad av-logo">
				</div>
				<div class="trust-logo-section__image-wrapper">
					<img data-src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/trust-logos/super-lawyers-logo.png" alt="Super Lawyers Logo" class="trust-logo-section__logo lozad super-lawyers-logo">
				</div>
				<div class="trust-logo-section__image-wrapper">
					<img data-src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/trust-logos/nadc-logo.png" alt="NADC Logo" class="trust-logo-section__logo lozad nadc-logo">
				</div>
				<div class="trust-logo-section__image-wrapper">
					<img data-src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/trust-logos/ntla-logo.jpg?v=1" alt="NTLA Logo" class="trust-logo-section__logo lozad ntla-logo">
				</div>
				<div class="trust-logo-section__image-wrapper">
					<img data-src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/trust-logos/google-logo.png" alt="Google Logo" class="trust-logo-section__logo lozad google-logo">
				</div>
				<div class="trust-logo-section__image-wrapper">
					<img data-src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/trust-logos/bbb-logo.png" alt="BBB Logo" class="trust-logo-section__logo lozad bbb-logo">
				</div>
			</div>
		</div>
	</div>

	<?php
	/*----------------------------------------
	End  Trust Logo Section
	-----------------------------------------*/
	?>


	<?php
	/*----------------------------------------
	Begin Testimonial and Video Section
	-----------------------------------------*/
	?>
	<div class="homepage-section homepage-testimonial-section">
		<div class="wrapper">
				<div class="row">
					<div class="one-third">
						<a href="<?php echo site_url(); ?>/testimonials/">
							<img data-src="<?php echo get_template_directory_uri(); ?>/assets/images/cordisco-testimonial-1.jpg" alt="Fascinating and Trustworthy Attorneys" class="homepage-testimonial-section__image lozad">
						</a>
					</div>
					<div class="one-third">
						<a href="<?php echo site_url(); ?>/testimonials/">
							<img data-src="<?php echo get_template_directory_uri(); ?>/assets/images/cordisco-testimonial-2.jpg" alt="The process was as painless as possible." class="homepage-testimonial-section__image lozad">
						</a>
					</div>
					<div class="one-third">
						<a href="<?php echo site_url(); ?>/testimonials/">
							<img data-src="<?php echo get_template_directory_uri(); ?>/assets/images/cordisco-testimonial-3.jpg" alt="They never gave up on us." class="homepage-testimonial-section__image lozad">
						</a>
					</div>
				</div>
		</div>
	</div>

	<?php
	/*----------------------------------------
	End Testimonial and Video Section
	-----------------------------------------*/
	?>

	<?php
	/*----------------------------------------
	Begin Attorney Feature Section
	-----------------------------------------*/
	?>

	<div class="homepage-section homepage-attorney-feature">
		<div class="wrapper">

			<h2 class="section-title">Our Team</h2>

			<div class="row">

				<div class="one-half">
					<div class="homepage-attorney-feature__attorney">
						<div class="homepage-attorney-feature__attorney-left">
							<div class="homepage-attorney-feature__attorney-image">
								<a href="<?php echo site_url(); ?>/attorneys/john-f-cordisco/"><img class="lozad" data-src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/about-john-cordisco-homepage.jpg" alt="Attorney Michael Saile"></a>
							</div>
						</div>
						<div class="homepage-attorney-feature__attorney-right">
							<div class="homepage-attorney-feature__attorney-name">
								<a href="<?php echo site_url(); ?>/attorneys/john-f-cordisco/">
									John F. Cordisco
								</a>
							</div>
							<div class="homepage-attorney-feature__attorney-title">
								Founder
							</div>
							<div class="homepage-attorney-feature__attorney-description">
								John F. Cordisco, attorney at law, has led a diverse life of service, dedication, and philanthropy. His passions bring him to serve in and out of the... <a href="<?php echo site_url(); ?>/attorneys/john-f-cordisco/">Read More &raquo;</a>
							</div>
						</div>
					</div>
				</div>

				<div class="one-half">
					<div class="homepage-attorney-feature__attorney">
						<div class="homepage-attorney-feature__attorney-left">
							<div class="homepage-attorney-feature__attorney-image">
								<a href="<?php echo site_url(); ?>/attorneys/michael-l-saile-jr/"><img class="lozad" data-src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/about-michael-saile-homepage.jpg" alt="Attorney Michael Saile"></a>
							</div>
						</div>
						<div class="homepage-attorney-feature__attorney-right">
							<div class="homepage-attorney-feature__attorney-name">
								<a href="<?php echo site_url(); ?>/attorneys/michael-l-saile-jr/">
									Michael L. Saile, Jr.
								</a>
							</div>
							<div class="homepage-attorney-feature__attorney-title">
								Managing Attorney
							</div>
							<div class="homepage-attorney-feature__attorney-description">
								Attorney Michael L. Saile, Jr. proudly serves and helps personal injury victims throughout Bucks County, PA. Recognized with a perfect score on Avvo... <a href="<?php echo site_url(); ?>/attorneys/michael-l-saile-jr/">Read More &raquo;</a>
							</div>
						</div>
					</div>
				</div>

			</div>
			<div class="homepage-attorney-feature__members-button-wrapper">
				<a class="homepage-attorney-feature__members-button" href="<?php echo site_url(); ?>/about-us/">View All Members &raquo;</a>
			</div>

		</div>
	</div>

	<?php
	/*----------------------------------------
	End Attorney Feature Section
	-----------------------------------------*/
	?>

	<?php
	/*----------------------------------------
	Begin Practice Area Circles Section
	-----------------------------------------*/
	?>

	<div class="homepage-section homepage-practice-areas-section">
		<div class="wrapper">

			<h2 class="section-title">Our Practice Areas</h2>

			<div class="practice-area-circles">

				<a class="homepage-practice-area-circle" href="<?php echo site_url(); ?>/bucks-county-injury/wrongful-death-lawyer/">
					<div class="homepage-practice-area-circle__inner lozad" data-background-image="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/PA_wrongful_death_pa.jpg);">
						<div class="homepage-practice-area-circle__title">Wrongful Death</div>
						<div class="homepage-practice-area-circle__read-more">Read More &raquo;</div>
					</div>
				</a>

				<a class="homepage-practice-area-circle" href="<?php echo site_url(); ?>/bucks-county-injury/car-accident-lawyer/">
					<div class="homepage-practice-area-circle__inner lozad" data-background-image="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/PA_car_accident_pa.jpg);">
						<div class="homepage-practice-area-circle__title">Car Accidents</div>
						<div class="homepage-practice-area-circle__read-more">Read More &raquo;</div>
					</div>
				</a>

				<a class="homepage-practice-area-circle" href="<?php echo site_url(); ?>/bucks-county-injury/truck-accident-lawyers/">
					<div class="homepage-practice-area-circle__inner lozad" data-background-image="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/PA_truck_accident_pa_image.jpg);">
						<div class="homepage-practice-area-circle__title">Truck Accidents</div>
						<div class="homepage-practice-area-circle__read-more">Read More &raquo;</div>
					</div>
				</a>

				<a class="homepage-practice-area-circle" href="<?php echo site_url(); ?>/bucks-county-injury/motorcycle-accident-lawyer/">
					<div class="homepage-practice-area-circle__inner lozad" data-background-image="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/PA_motorcycle_accident_pa.jpg);">
						<div class="homepage-practice-area-circle__title">Motorcycle Accidents</div>
						<div class="homepage-practice-area-circle__read-more">Read More &raquo;</div>
					</div>
				</a>

				<a class="homepage-practice-area-circle" href="<?php echo site_url(); ?>/bucks-county-injury/pedestrian-bike-accident-lawyer/">
					<div class="homepage-practice-area-circle__inner lozad" data-background-image="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/PA_bicycle_accident_pa.jpg);">
						<div class="homepage-practice-area-circle__title">Bicycle Accidents</div>
						<div class="homepage-practice-area-circle__read-more">Read More &raquo;</div>
					</div>
				</a>

				<a class="homepage-practice-area-circle" href="<?php echo site_url(); ?>/bucks-county-injury/pedestrian-accident-lawyer/">
					<div class="homepage-practice-area-circle__inner lozad" data-background-image="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/PA_pedestrian_accidents_pa.jpg);">
						<div class="homepage-practice-area-circle__title">Pedestrian Accidents</div>
						<div class="homepage-practice-area-circle__read-more">Read More &raquo;</div>
					</div>
				</a>

				<a class="homepage-practice-area-circle" href="<?php echo site_url(); ?>/bristol-injury/train-accident-lawyer/">
					<div class="homepage-practice-area-circle__inner lozad" data-background-image="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/PA_Cordisco_Law_Train_Accident_Attorney.jpg);">
						<div class="homepage-practice-area-circle__title">Train Accidents</div>
						<div class="homepage-practice-area-circle__read-more">Read More &raquo;</div>
					</div>
				</a>

				<a class="homepage-practice-area-circle" href="<?php echo site_url(); ?>/bucks-county-injury/bus-accident-lawyer/">
					<div class="homepage-practice-area-circle__inner lozad" data-background-image="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/PA_bus_accident.jpg);">
						<div class="homepage-practice-area-circle__title">Bus Accidents</div>
						<div class="homepage-practice-area-circle__read-more">Read More &raquo;</div>
					</div>
				</a>

				<a class="homepage-practice-area-circle" href="<?php echo site_url(); ?>/bucks-county-injury/workers-compensation-lawyer/">
					<div class="homepage-practice-area-circle__inner lozad" data-background-image="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/PA_workplace_injuries_pa.jpg);">
						<div class="homepage-practice-area-circle__title">Workplace Injuries</div>
						<div class="homepage-practice-area-circle__read-more">Read More &raquo;</div>
					</div>
				</a>

				<a class="homepage-practice-area-circle" href="<?php echo site_url(); ?>/bucks-county-injury/premises-liability-lawyer/">
					<div class="homepage-practice-area-circle__inner lozad" data-background-image="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/PA_premises_liability_pa.jpg);">
						<div class="homepage-practice-area-circle__title">Premises Liability</div>
						<div class="homepage-practice-area-circle__read-more">Read More &raquo;</div>
					</div>
				</a>

			</div> <?php // .practice-area-circles ?>

			<div class="homepage-centered-button-wrapper">
				<a href="<?php echo site_url(); ?>/practice-areas/" class="homepage-gray-button">View All Practice Areas &raquo;</a>
			</div>


			<div class="row homepage-search-row">
				<hr>
				<div class="wrapper">
					<div class="search-box-wrapper search-box-wrapper--homepage">
						<span class="homepage-search-title">Or Enter Your Search Term Here</span>
						<form class="desktop-search search-box" action="<?php echo home_url('/'); ?>">
							<input type="text" placeholder="Search the Site" name="s" value="" aria-label="Search Input">
						    <input type="submit" class="search-submit" value="" aria-label="Search Submit">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php
	/*----------------------------------------
	End Practice Area Circles Section
	-----------------------------------------*/
	?>

	<?php
	/*----------------------------------------
	Begin Results
	-----------------------------------------*/
	?>

	<div class="homepage-section homepage-results">
		<div class="wrapper">
			<h2 class="section-title">
				Results
			</h2>

			<div class="homepage-results-list">
			<?php
			    $args = array(
			        'posts_per_page' => 3,
			        'post_type' => 'cordisco_results',
			        'order' => 'DSC',
			        'meta_key' => '_cs_case_result_amount',
	            	'orderby'   => 'meta_value_num'
			    );

			    $the_query = new WP_Query( $args );
			    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
			?>

				<a class="homepage-result" href="<?php echo site_url(); ?>/case-results/">
					<div class="homepage-result__title">
						<?php //Get Result Title
							 if ( get_post_meta( $post->ID, '_cs_case_result_homepage_title', true ) ){
							     echo  get_post_meta( $post->ID, '_cs_case_result_homepage_title', true );
							  } else {
							  	echo 'Case Result for Client';
							  }
						?>
					</div>
					<div class="homepage-result__amount">
						<?php //Get Result Title
							 if ( get_post_meta( $post->ID, '_cs_case_result_amount', true ) ){
								$resultAmount =  get_post_meta( $post->ID, '_cs_case_result_amount', true );
								//Add commas and currency
								echo toMoney($resultAmount);
							  }
						?>
					</div>
					<div class="homepage-result__learn-more">
						Learn More &raquo;
					</div>
				</a>

			<?php endwhile; else : ?>
				<p>No results found.</p>
			<?php endif; ?>
			<?php wp_reset_query(); ?>
			</div> <?php //.homepage-results-list ?>

			<div class="homepage-results__button-wrapper">
				<a href="<?php echo site_url(); ?>/case-results/" class="homepage-gray-button">View All Results &raquo;</a>
			</div>

		</div>
	</div>


	<?php
	/*----------------------------------------
	End  Results
	-----------------------------------------*/
	?>


	<?php
	/*----------------------------------------
	Begin Testimonials
	-----------------------------------------*/
	?>
	<div class="homepage-section testimonials-section">
		<div class="wrapper">
			<h2 class="section-title">Testimonials</h2>

			<div class="testimonial-list testimonials">
				<div class="flexslider">
					<ul class="slides">
						<?php   //Display 4 most recent testimonials
								$args = array(
									'posts_per_page' => 4,
									'post_type' => 'cordisco_testimonial',
									 'order' => 'DSC',
							        'meta_key' => '_cs_testimonial_date',
					            	'orderby'   => 'meta_value_num'
								);

								$the_query = new WP_Query( $args );
								if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
							?>

							<li class="single-testimonial">
								<span class="left-quote">&ldquo;</span>
								<span class="right-quote">&rdquo;</span>
								<div class="single-testimonial-inner">
									<?php //Get Quote Content
										if ( get_post_meta( $post->ID, '_cs_testimonial_description_short', true ) ) {
											echo '<div class="quote-content">';
											echo get_post_meta( $post->ID, '_cs_testimonial_description_short', true );
											echo '</div>';
										}
									?>

									<?php //Get Quote Author
										if ( get_post_meta( $post->ID, '_cs_testimonial_author', true ) ) {
											echo '<div class="quote-author">';
											echo get_post_meta( $post->ID, '_cs_testimonial_author', true );
											echo '</div>';
										}
									?>
								</div>
							</li>
							<?php endwhile; else : ?>
								<p>No testimonials found.</p>
							<?php endif; ?>

					<?php wp_reset_query(); ?>
					</ul>
				</div>
			</div>

		</div>
	</div>
	<?php
	/*----------------------------------------
	End Testimonials
	-----------------------------------------*/
	?>

	<?php
	/*----------------------------------------
	Begin Ebook Section
	-----------------------------------------*/
	?>

	<div class="homepage-section homepage-ebook-promo">
		<div class="wrapper">
			<h2 class="section-title">Free Ebooks</h2>
			<div class="row">
				<div class="homepage-ebook-promo__left">
					<div class="homepage-ebook">

						<div class="homepage-ebook-promo__image">
							<img class="lozad" data-src="<?php echo get_template_directory_uri(); ?>/assets/images/ebooks/dont-crash-again-ebook-cover-no-shadow.png" alt="Don't Crash Again Ebook">
						</div>
						<div class="homepage-ebook-promo__content">
							<div class="homepage-ebook-promo__title">
								Download Free Car Accident Information to Maximizing Your Recovery
							</div>
							<div class="homepage-ebook-promo__description">
								<p>Don't let the insurance company add insult to injury with a low settlement. Order this free car accident information to learn how to maximize your claim.</p>
							</div>
							<a href="<?php echo site_url(); ?>/free-offers/free-car-accident-information-maximizing-recovery-ebook/" class="homepage-ebook-promo__button">
								View Details
							</a>
						</div>

					</div>
				</div>
				<div class="homepage-ebook-promo__right">
					<div class="homepage-ebook">

						<div class="homepage-ebook-promo__image">
							<img class="lozad" data-src="<?php echo get_template_directory_uri(); ?>/assets/images/ebooks/not-another-bad-lawyer-ebook-cover-no-shadow.png" alt="Best Personal Injury Lawyer Ebook">
						</div>
						<div class="homepage-ebook-promo__content">
							<div class="homepage-ebook-promo__title">
								Searching for the Best Personal Injury Lawyer in Bucks County? Then, You NEED This Book!
							</div>
							<div class="homepage-ebook-promo__description">
								Save yourself the frustration of switching lawyers mid-stream. This FREE guide tells you how to hire the best personal injury lawyer in Bucks County for your claim.
							</div>
							<a href="<?php echo site_url(); ?>/free-offers/searching-best-personal-injury-lawyer-ebook/" class="homepage-ebook-promo__button">
								View Details
							</a>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<?php
	/*----------------------------------------
	End Ebook Section
	-----------------------------------------*/
	?>

	<?php
	/*----------------------------------------
	Begin Homepage Intro Content Section
	-----------------------------------------*/
	?>
	<div class="homepage-section homepage-intro-content">
		<div class="wrapper">
			<h2 style="text-align: left;">Your Personal Injury Attorneys in Bucks County</h2>

			<div class="homepage-intro-content__content entry-content">
				<p>Our job as personal injury lawyers is to tell your story. At Cordisco &amp; Saile LLC we want to know your pain, your fears, and your family so we can illustrate all the financial, emotional, and physical effects of your injuries.</p>
				<p>That’s how we build a convincing case that gets you the money you need to pay your bills, get treatment, and compensate for the many ways your accident has turned your life upside down. We want to help you achieve the financial and emotional security you deserve after a serious accident. That’s why in our 30 years of practicing personal injury law mediocrity has never been an option. And that’s why we never stop fighting for our clients.</p>
				<p>Call 215-642-2335 to go over your case with a personal injury lawyer in Bucks County.</p>
				<h2 style="text-align: left;">Why Choose Us as Your Personal Injury Lawyers?</h2>
				<p>Our law practice stands on four pillars of service:</p>
				<ul>
				    <li>We listen to your story to develop a personal case strategy.</li>
				    <li>We keep you updated and return your calls.</li>
				    <li>We do what we say we will do – every time.</li>
				    <li>We treat you like family.</li>
				</ul>
				<h2 style="text-align: left;">We Listen to Your Story to Develop a Personal Case Strategy</h2>
				<p>You need and deserve a personalized case strategy for your injury claim to be successful. We take the time to get to know you and your family so we can develop a plan to get you the money you need.</p>
				<p>Your personal injury claim should account for your current bills and lost wages as well as the long-term financial, physical, and emotional effects of your injuries. That’s why we investigate the details of your accident and injuries and we listen as you describe how your life has changed. Your personal injury claim should seek damages for:</p>
				<ul style="columns: 120px 3;">
					<li>Medical treatment</li>
					<li>Physical therapy</li>
					<li>In-home care</li>
					<li>Lost wages</li>
					<li>Reduced earning capacity</li>
					<li>Loss of benefits</li>
					<li>Loss of promotion</li>
					<li>Emotional distress</li>
					<li>Pain and suffering</li>
				</ul>
				<p>We exhaust every resource to develop a fair value of your case. Our personal injury attorneys will deal with the insurance company for you, create a strategy to prove the defendant’s fault and liability, negotiate case value, and remove any obstacles the insurance company puts up to avoid paying your claim.</p>
				<p>We work to get you the<strong> settlement you need to put your finances and life back in order</strong>.</p>
				<h2 style="text-align: left;">We Keep You Updated and Return Your Calls</h2>
				<p>You will never be unsure of the status of your case. We <strong>communicate with you directly and regularly</strong> to keep you updated on developments with your claim. If you ever have questions or concerns, we are a phone call or email away. If we cannot take your call right away, we will return your call promptly.</p>
				<h2 style="text-align: left;">We Do What We Say We Will Do – Every Time</h2>
				<p>When we tell you we will…</p>
				<ul>
				    <li>Talk to insurance companies</li>
				    <li>File your claims</li>
				    <li>Investigate your accident</li>
				    <li>Get your medical records</li>
				    <li>Contact eyewitnesses</li>
				    <li>Secure expert testimony</li>
				    <li>And do the many other things that go into a successful personal injury claim</li>
				</ul>
				<p>…rest assured we will keep our promise.</p>
				<p>There isn’t much worse than hiring a lawyer to help you with an injury claim only to feel frustrated and unsure what to do when your lawyer lets you down. If we tell you we will do something, we will do it.</p>
				<h2 style="text-align: left;">We Treat You Like Family</h2>
				<p>You become our family when you hire us to take your personal injury case. By this we mean that we will listen to your story, communicate with you, and keep our promises, just as we would with our own families.</p>
				<p>Cordisco &amp; Saile LLC is based in Bucks County, PA. Call us at <a href="tel:+1-215-642-2335">215-642-2335</a> to set up a free consultation where we will review your case, answer your questions, and explain the next steps.</p>

			</div>

		</div>
	</div>
	<?php
	/*----------------------------------------
	End Homepage Intro Content Section
	-----------------------------------------*/
	?>

	<?php
	/*----------------------------------------
	Begin Map Section
	- Disabled
	-----------------------------------------*/
	?>
	<?php /*
	<div class="homepage-section homepage-map-section">

		<div id="homepage-map" class="homepage-map"></div>
	</div> */ ?>

	<?php
	/*----------------------------------------
	End Map Section
	-----------------------------------------*/
	?>


<?php get_footer(); ?>

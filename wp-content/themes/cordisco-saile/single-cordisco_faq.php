<?php get_header(); ?>



	<div id="inner-content" class="wrapper">

		<div id="main" class="content-container" role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php
					$questionContent = strip_tags(get_the_content());
					$questionContent = str_replace('"', "", $questionContent);
					$questionContent = str_replace("'", "", $questionContent);
				?>
				<?php /* Question Schema */?>
				<script type="application/ld+json">
					{
					    "@context": "http://schema.org",
					    "@type": "Question",
					    "text": "<?php echo get_the_title(); ?>",
					    "dateCreated": "<?php echo get_the_date('c',$post->ID); ?>",
					    "author": {
					        "@type": "Person",
					        "name": "Cordisco & Saile, LLC"
					    },
					    "answerCount": "1",
					    "acceptedAnswer": {
					        "@type": "Answer",
					        "text": "<?php echo $questionContent; ?>",
					        "dateCreated": "<?php echo get_the_date('c',$post->ID); ?>",
					        "author": {
					            "@type": "Person",
					            "name": "Cordisco & Saile, LLC"
					        }
					    }
					}
				</script>
				<?php //End Question Schema ?>


 				<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?>>


	                <header class="article-header entry-header">

	                  <h1 class="entry-title single-title" rel="bookmark"><?php the_title(); ?></h1>

	                  <p class="byline entry-meta">

	                    <?php printf( __( 'Posted', 'bonestheme' ).' %1$s %2$s',
	                       /* the time the post was published */
	                       '<time class="updated entry-time" datetime="' . get_the_time('Y-m-d') . '">' . get_the_time(get_option('date_format')) . '</time>',
	                       /* the author of the post */
	                       '<span class="by">'.__( 'by', 'bonestheme' ).'</span> <span class="entry-author author">' . get_the_author_link( get_the_author_meta( 'ID' ) ) . '</span>'
	                    ); ?>

	                  </p>

	                  <p class="faq-categories">
	                  	<?php
	                  		$faqCategories =  get_the_term_list( $post->ID, 'cordisco_faq_cat', 'Categorized: ', ', ' );
	                  		//Remove incorrect anchor tags from output
	                  		echo  strip_tags( $faqCategories );
	                  	?>
	                  </p>

	                </header> <?php // end article header ?>

	                <section class="entry-content cf" itemprop="articleBody">
	                  <?php
	                    // the content (pretty self explanatory huh)
	                    the_content();
	                  ?>
	                </section> <?php // end article section ?>


	                <?php //comments_template(); ?>

              </article> <?php // end article ?>

              <p>
              	<a href="<?php echo site_url(); ?>/faqs/">&laquo; Return to Main FAQ Listing</a>
              </p>

              <hr>

          	<?php
		    /*--------------------------------------------
		    Display Related FAQs
		    ---------------------------------------------*/
		    ?>

		    <h2 class="related-faq-title">Frequently Asked Questions</h2>
			<ul class="related-faq-list">
				<?php

				$faqTerms = get_terms( 'cordisco_faq_cat' );
				// convert array of term objects to array of term IDs
				$faqTermIDs = wp_list_pluck( $faqTerms, 'term_id' );

				$args = array(
				  'posts_per_page' => '5',
				  'post_type' => 'cordisco_faq',
				  'tax_query' => array(
				        array(
				            'taxonomy' => 'cordisco_faq_cat',
				            'field' => 'term_id',
				            'terms' => $faqTermIDs
				        ),
				    ),
				  'order' => 'DSC',
				  'orderby' => 'rand',
				);

				$the_query = new WP_Query( $args );
				if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
				?>
				<?php //Getting Category for Filtering
				    $postTerms =  wp_get_object_terms($post->ID, 'cordisco_faq_cat');
				    $categoryFilterSlug = '';
				    $categoryPrettyName = '';
				    if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
				         foreach ( $postTerms as $term ) {
				           $categoryFilterSlug .= ' ' . $term->slug;
				           $categoryPrettyName .= ' ' . $term->name . '<span class="divider">, </span>';
				         }
				     }
				 ?>

				 <li><a href="<?php the_permalink(); ?>" class="faq-filter-title-link"><?php the_title(); ?></a></li>

				  <?php endwhile; else : ?>
				 	<p>Sorry no related FAQs found.</p>
				  <?php endif; ?>
				  <?php wp_reset_query(); ?>
			</ul>

			<?php //End Related FAQ Loop ?>





			<?php endwhile; ?>

			<?php else : ?>

				<article id="post-not-found" class="hentry cf">
						<header class="article-header">
							<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
						</header>
						<section class="entry-content">
							<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
						</section>
						<footer class="article-footer">
								<p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
						</footer>
				</article>

			<?php endif; ?>

		</div>

		<?php get_sidebar('faq'); ?>

	</div>



<?php get_footer(); ?>

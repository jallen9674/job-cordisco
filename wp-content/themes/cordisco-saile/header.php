<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?php wp_title(''); ?></title>


	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>


	<?php // wordpress head functions ?>
	<?php wp_head(); ?>
	<?php // end of wordpress head ?>

</head>

<body <?php body_class(); ?>>

	<?php // <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous"> ?>
	<div class="rs-mobile-header">
		<a href="/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/cordisco-saile-logo.png" alt="Cordisco & Saile LLC, Logo"></a>
		<div class="rs-mobile-header-buttons">
			<a href="" class="rs-icon-btn rs-icon-menu"><i class="fa fa-bars"></i></a>
			<a href="tel:+1-<?php echo do_shortcode('[hc-localized-number]'); ?>" class="rs-click-to-call-btn"><i class="fa fa-phone"></i> Click to call</a>
			<a href="" class="rs-icon-btn rs-icon-form"><i class="fa fa-envelope"></i></a>
		</div>
	</div>

<div class="header sb-slide mobile-header">
	<div id="inner-header-mobile" class="wrap cf">

		<?php // To print blog title: php bloginfo('name');  ?>
		<div id="mobile-logo" class="h1">
			<a href="<?php echo home_url(); ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/cordisco-saile-logo.png" alt="Cordisco & Saile LLC, Logo">
			</a>
		</div>

		<div class="mobile-header-info">
			<div class="header-call-to-action">
					<span>Call Now!</span>
					<a href="tel:+1-<?php echo do_shortcode('[hc-localized-number]'); ?>"><?php echo do_shortcode('[hc-localized-number]'); ?></a>
				</div>
		</div>

		<a href="<?php echo site_url(); ?>/john-cordisco-mike-missanelli"  class="mobile-header-radio">
			<span class="mobile-header-radio__title">
				As Heard On
			</span>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/97-5-the-fanatic.png" alt="As Featured on 97.5 The Fanatic" class="mobile-header-radio__image">
		</a>

	</div>

	<div class="mobile-nav-menu-box">
		MENU	<span class="sb-toggle-right mobile-nav-arrow mobile-nav-box-open"></span>
	</div>
</div>


<div class="desktop-scrolled-header">
	<div class="top-nav-wrapper">
		<nav role="navigation" class="wrapper">
			<?php wp_nav_menu(array(
			'container' => false,                           // remove nav container
			'container_class' => 'menu cf',                 // class of container (should you choose to use it)
			'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
			'menu_class' => 'nav top-nav cf',               // adding custom nav class
			'theme_location' => 'main-nav',                 // where it's located in the theme
			'before' => '',                                 // before the menu
			'after' => '',                                  // after the menu
			'link_before' => '',                            // before each link
			'link_after' => '',                             // after each link
			'depth' => 0,                                   // limit the depth of the nav
			'fallback_cb' => ''                             // fallback function (if there is one)
			)); ?>

			<div class="phone-number">
				<a href="tel:+1-<?php echo do_shortcode('[hc-localized-number]'); ?>"><?php echo do_shortcode('[hc-localized-number]'); ?></a>
			</div>
		</nav>
	</div>
</div>


<div class="sticky-footer-container" id="sb-site">

	<header class="header">
		<div id="inner-header" class="wrapper">
			<div class="desktop-header">
				<div id="logo" class="desktop-header__left">
					<a href="<?php echo home_url(); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/cordisco-saile-logo.png" alt="Cordisco & Saile LLC, Logo">
					</a>
				</div>

				<div class="desktop-header__center">

					<?php /* Disabled
					<a href="<?php echo site_url(); ?>/john-cordisco-mike-missanelli"  class="desktop-header-radio">
						<span class="desktop-header-radio__title">
							As Heard On
						</span>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/97-5-the-fanatic.png" alt="As Featured on 97.5 The Fanatic" class="desktop-header-radio__image">
					</a> */ ?>

					<div class="personal-injury-law">
						<span>PERSONAL</span> INJURY LAW
					</div>



				</div>

				<div class="desktop-header__right">

					<div class="header-call-to-action">
						<span>Call Now!</span>
						<a href="tel:+1-<?php echo do_shortcode('[hc-localized-number]'); ?>"><?php echo do_shortcode('[hc-localized-number]'); ?></a>
					</div>
					<?php /* Disabled
					<div class="header-social-icons">
						<?php  // <span class="header-social-icons__intro-text">Find Us On Social Media | </span> ?>
						<div class="header-social-icons__social-icons" itemscope itemtype="http://schema.org/Organization">
							<link itemprop="url" href="http://www.your-company-site.com">
							<a itemprop="sameAs" href="https://www.facebook.com/pages/Cordisco-Law-LLC/335095316644989" class="header-social-icons__social-icon header-social-icons__social-icon--facebook"><i class="fa fa-facebook"></i></a>
							<a itemprop="sameAs" href="https://twitter.com/cordiscosaile" class="header-social-icons__social-icon header-social-icons__social-icon--twitter"><i class="fa fa-twitter"></i></a>
							<a itemprop="sameAs" href="https://www.linkedin.com/in/johncordiscoesq" class="header-social-icons__social-icon header-social-icons__social-icon--linkedin"><i class="fa fa-linkedin"></i></a>
						</div>
					</div> */ ?>
				</div>
			</div>
		</div>
		<div class="top-nav-wrapper">
			<nav role="navigation" class="wrapper">
				<?php wp_nav_menu(array(
				'container' => false,                           // remove nav container
				'container_class' => 'menu cf',                 // class of container (should you choose to use it)
				'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
				'menu_class' => 'nav top-nav cf',               // adding custom nav class
				'theme_location' => 'main-nav',                 // where it's located in the theme
				'before' => '',                                 // before the menu
    			'after' => '',                                  // after the menu
    			'link_before' => '',                            // before each link
    			'link_after' => '',                             // after each link
    			'depth' => 0,                                   // limit the depth of the nav
				'fallback_cb' => ''                             // fallback function (if there is one)
				)); ?>
			</nav>
		</div>
	</header>

	<?php
	/*
		Begin Yoast Breadcrumbs (Excluded on homepage)
	*/
	?>

	<?php if ( !is_front_page() ) { ?>
	<div class="breadcrumbs">
		<div class="breadcrumbs__inner wrapper">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<div class="breadcrumbs__content">','</div>');
				}
			?>
		</div>
	</div>
	<?php } ?>

	<div id="container">

<?php get_header(); ?>

	<div id="inner-content" class="wrapper">
			<div id="main" class="content-container">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> >

					<header class="article-header">
						<h1 class="h2 entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
					</header>

					<section class="entry-content cf">
						<?php the_content(); ?>
					</section>

					<footer class="article-footer cf">
						<p class="footer-comment-count">
							<?php comments_number( __( '<span>No</span> Comments', 'bonestheme' ), __( '<span>One</span> Comment', 'bonestheme' ), _n( '<span>%</span> Comments', '<span>%</span> Comments', get_comments_number(), 'bonestheme' ) );?>
						</p>

		                 	<?php printf( '<p class="footer-category">' . __('Filed under', 'bonestheme' ) . ': %1$s</p>' , get_the_category_list(', ') ); ?>

		               <?php the_tags( '<p class="footer-tags tags"><span class="tags-title">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '</p>' ); ?>
		            </footer>

				</article>

				<?php endwhile; ?>

						<?php numeric_posts_nav(); ?>

				<?php else : ?>

						<article id="post-not-found" class="hentry cf">
								<header class="article-header">
									<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
							</header>
								<section class="entry-content">
									<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
							</section>
							<footer class="article-footer">
									<!-- This is the index.php template -->
							</footer>
						</article>

				<?php endif; ?>

			</div>

		<?php get_sidebar(); ?>

	</div>

<?php get_footer(); ?>

<?php
/***************************************************
** BRINGING EVERYTHING IN
***************************************************/

//Bringing in CSS
function hc_location_styles(){
    //wp_enqueue_style( 'theme-style', get_stylesheet_directory_uri() .'/style.css' );
    wp_enqueue_style( 'hc-location-css', get_stylesheet_directory_uri() .'/assets/modules/widget-location-list/css/styles.css' );
}
add_action( 'wp_enqueue_scripts', 'hc_location_styles', 10000);

//Bringing in JS
function hc_location_scripts(){
    wp_enqueue_script('hc-location-js', get_stylesheet_directory_uri() . '/assets/modules/widget-location-list/js/scripts.js', array('jquery'), '1.0', true);
    //wp_enqueue_script( 'comment-reply' );
}
add_action( 'wp_enqueue_scripts', 'hc_location_scripts', 40);

//Bringing in Taxonomy
require_once('location-taxonomy.php');

//Bringing in Metaboxes
require_once('location-metaboxes.php');

//Bringing in Widget
require_once('location-widget.php');
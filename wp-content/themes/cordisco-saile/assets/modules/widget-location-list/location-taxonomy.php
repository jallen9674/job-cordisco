<?php

/***************************************************
** ADDING LOCATION TAXONOMY
***************************************************/

    register_taxonomy( 'hc_location',
        array('page'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
        array('hierarchical' => true,     /* if this is true, it acts like categories */
            'labels' => array(
                'name' => __( 'Practice Area Group/Location', 'bonestheme' ), /* name of the custom taxonomy */
                'singular_name' => __( 'Group', 'bonestheme' ), /* single taxonomy name */
                'search_items' =>  __( 'Search Groups', 'bonestheme' ), /* search title for taxomony */
                'all_items' => __( 'All Groups', 'bonestheme' ), /* all title for taxonomies */
                'parent_item' => __( 'Parent Group', 'bonestheme' ), /* parent title for taxonomy */
                'parent_item_colon' => __( 'Parent Group:', 'bonestheme' ), /* parent taxonomy title */
                'edit_item' => __( 'Edit Group', 'bonestheme' ), /* edit custom taxonomy title */
                'update_item' => __( 'Update Group', 'bonestheme' ), /* update title for taxonomy */
                'add_new_item' => __( 'Add New Group', 'bonestheme' ), /* add new title for taxonomy */
                'new_item_name' => __( 'New Group Name', 'bonestheme' ) /* name title for taxonomy */
            ),
            'show_admin_column' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'hc-location' ),
        )
    );
<?php

/*---------------------------------
Avvo Review Button for Mike
[hc-avvo-button-mike]
---------------------------------*/
function hcAvvoButtonMike($atts = null) {

    ob_start();
    //BEGIN OUTPUT
?>

<a href="https://www.avvo.com/attorneys/19053-pa-michael-saile-1587108/reviews.html" class="avvo-review-button" target="_blank">Read <b>143</b> of Michael Saile's Avvo Reviews</a>

<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}

add_shortcode('hc-avvo-button-mike', 'hcAvvoButtonMike');


/*---------------------------------
Avvo Review Button for John
[hc-avvo-button-john]
---------------------------------*/
function hcAvvoButtonJohn($atts = null) {

    ob_start();
    //BEGIN OUTPUT
?>

<a href="https://www.avvo.com/attorneys/19053-pa-john-cordisco-430378/reviews.html" class="avvo-review-button" target="_blank">Read <b>13</b> of John Cordisco's Avvo Reviews</a>

<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}

add_shortcode('hc-avvo-button-john', 'hcAvvoButtonJohn');


<?php

/*---------------------------------
BEGIN EBOOK WIDGET
[hc-blog-widget]
---------------------------------*/
function hcBlogWidget($atts = null) {

    ob_start();
    //BEGIN OUTPUT
?>

<div class="blog-widget">

    <div class="blog-widget__inner">
        <a href="<?php echo site_url(); ?>/blog/" class="blog-widget__link">
            <span class="blog-widget__icon">
                <i class="fa fa-book"></i>
            </span>
            <span class="blog-widget__text">
                View Blog
            </span>
        </a>
    </div>

</div>

<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}

add_shortcode('hc-blog-widget', 'hcBlogWidget');

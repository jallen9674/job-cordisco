<?php

/*---------------------------------
BEGIN EBOOK WIDGET
[hc-ebook-widget]
---------------------------------*/
function hcEbookWidget($atts = null) {

    ob_start();
    //BEGIN OUTPUT
?>

<div class="ebook-widget">
    <div class="ebook-widget__inner">

            <h3>Free Resources</h3>
            <div class="flexslider ebook-sidebar-slider">
                  <ul class="slides">
                    <li>
                        <div class="car-accident-ebook ebook-widget__single-ebook">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/ebooks/dont-crash-again-ebook-cover-no-shadow.png" alt="Don't Crash Ebook Cover">
                            <a href="<?php echo site_url(); ?>/free-offers/free-car-accident-information-maximizing-recovery-ebook/" class="ebook-download-button">Download Now</a>
                        </div>
                    </li>
                    <li>
                        <div class="bad-lawyer-ebook ebook-widget__single-ebook">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/ebooks/not-another-bad-lawyer-ebook-cover-no-shadow.png" alt="Not Another Bad Lawyer Ebook Cover">
                            <a href="<?php echo site_url(); ?>/free-offers/searching-best-personal-injury-lawyer-ebook/" class="ebook-download-button">Download Now</a>
                        </div>
                    </li>
                </ul>
            </div>
    </div>
</div>


<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}

add_shortcode('hc-ebook-widget', 'hcEbookWidget');

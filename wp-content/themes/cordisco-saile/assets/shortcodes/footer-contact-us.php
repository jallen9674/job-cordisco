<?php

/*---------------------------------
Footer Contact Us Widget w/ Slider
[hc-footer-contact-slider]
---------------------------------*/
function hcFooterContactSlider($atts = null) {

    ob_start();
    //BEGIN OUTPUT
?>

<div class="footer-contact-slider flexslider">
    <ul class="slides">
        <li>
                <span class="location-title">Bensalem Office</span>
                <div class="footer-icon footer-icon-address" style="margin-bottom: 10px;">
                    <strong>Address:</strong>
                    <address>900 Northbrook Dr #120
                    Trevose, PA 19053</address>
                </div>
                <p class="footer-icon footer-icon-email"><strong>Email:</strong><br>
                <a href="mailto:info@cordiscosaile.com">info@cordiscosaile.com</a></p>
                <p class="footer-icon footer-icon-phone"><strong>Phone:</strong><br>
                <a href="tel:+1-215-642-2335">215-642-2335</a></p>
        </li>
        <li>
                <span class="location-title">Newtown-Langhorne Office</span>
                <div class="footer-icon footer-icon-address" style="margin-bottom: 10px;">
                    <strong>Address:</strong>
                    <address>403 Executive Dr #100
                    Langhorne, PA 19047</address>
                </div>
                <p class="footer-icon footer-icon-email"><strong>Email:</strong><br>
                <a href="mailto:info@cordiscosaile.com">info@cordiscosaile.com</a></p>
                <p class="footer-icon footer-icon-phone"><strong>Phone:</strong><br>
                <a href="tel:+1-215-642-2335">215-642-2335</a></p>
        </li>
    </ul>
</div>

<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}

add_shortcode('hc-footer-contact-slider', 'hcFooterContactSlider');

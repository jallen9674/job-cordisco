<?php

/*---------------------------------
BEGIN EBOOK WIDGET
[hc-faq-widget]
---------------------------------*/
function hcFaqWidget($atts = null) {

    ob_start();
    //BEGIN OUTPUT
?>

<div class="faq-widget">
    <div class="faq-widget__inner">
        <a href="<?php echo site_url(); ?>/faqs/" class="faq-widget__link">
            <span class="faq-widget__icon">
                <i class="fa fa-question-circle"></i>
            </span>
            <span class="faq-widget__text">
                View FAQs
            </span>
        </a>
    </div>
</div>


<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}

add_shortcode('hc-faq-widget', 'hcFaqWidget');

<?php

/*---------------------------------
Infusionsoft Contact Forms
[hc-contact-form-homepage]
---------------------------------*/
function hcContactFormHomepage($atts = null) {

    ob_start();
    //BEGIN OUTPUT
?>

<form accept-charset="UTF-8" action="https://mr207.infusionsoft.com/app/form/process/89ab556981e654638f99d76933a55fa1" class="infusion-form homepage-top-contact-form" id="inf_form_89ab556981e654638f99d76933a55fa1" method="POST">
    <input name="inf_form_xid" type="hidden" value="89ab556981e654638f99d76933a55fa1" />
    <input name="inf_form_name" type="hidden" value="Home Page Hero Form" />
    <input name="infusionsoft_version" type="hidden" value="1.68.0.84" />
    <h2>Your Case Evaluation is FREE!</h2>
    <div class="infusion-field wpcf7-form-control-wrap your-name">
        <input class="infusion-field-input-container" id="inf_custom_Name" name="inf_custom_Name" type="text"  placeholder="Name *" aria-label="name required"/> </div>
    <div class="infusion-field wpcf7-form-control-wrap your-email">
        <input class="infusion-field-input-container" id="inf_field_Email" name="inf_field_Email" type="text" placeholder="Email *" aria-label="email required" /> </div>
    <div class="infusion-field wpcf7-form-control-wrap your-telephone">
        <input class="infusion-field-input-container" id="inf_field_Phone1" name="inf_field_Phone1" type="text" placeholder="Phone" aria-label="phone" /> </div>
    <div class="infusion-field wpcf7-form-control-wrap your-message">
        <textarea cols="24" id="inf_custom_Howcanwehelpyou" name="inf_custom_Howcanwehelpyou" rows="5" placeholder="How can we help you? (optional)" aria-label="How can we help you"></textarea>
    </div>

    <div class="infusion-submit">
        <input type="submit" value="Free Case Review" />
    </div>
</form>
<script type="text/javascript" src="https://mr207.infusionsoft.com/app/webTracking/getTrackingCode"></script>

<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}

add_shortcode('hc-contact-form-homepage', 'hcContactFormHomepage');
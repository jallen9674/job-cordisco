<?php

/*---------------------------------------------
Display Different Phone Number Based on Page ID
[hc-localized-number]
-- Note this is also repeated in function.php for the schema
----------------------------------------------*/
function hcLocalPhoneNumber($atts = null) {

    ob_start();
    //BEGIN OUTPUT
?>

<?php

    //Display 2336 on Langhorne
    if( is_page('3646') ) {
        echo '215-642-2336';
    } else {
        echo '215-642-2335';
    }
?>

<?php
    //END OUTPUT
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}

add_shortcode('hc-localized-number', 'hcLocalPhoneNumber');

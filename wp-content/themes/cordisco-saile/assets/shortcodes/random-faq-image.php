<?php

function cordiscoRandomFAQLoopImage($atts = null) {

    global $post;

    extract(shortcode_atts(array(
      'amount' => '',
   ), $atts));

    $queryAmount = $amount;

    ob_start();
    //BEGIN OUTPUT
?>

<div class="random-faq-loop  faq-filter-portfolio-wrapper">
        <?php $faqTerms = get_terms( 'cordisco_faq_cat' );
        // convert array of term objects to array of term IDs
        $faqTermIDs = wp_list_pluck( $faqTerms, 'term_id' );

        $args = array(
          'posts_per_page' => $queryAmount,
          'post_type' => 'cordisco_faq',
          'tax_query' => array(
                array(
                    'taxonomy' => 'cordisco_faq_cat',
                    'field' => 'term_id',
                    'terms' => $faqTermIDs
                ),
            ),
          'order' => 'DSC',
          'orderby' => 'rand',
        );

        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
      ?>
        <?php //Getting Category for Filtering
            $postTerms =  wp_get_object_terms($post->ID, 'cordisco_faq_cat');
            $categoryFilterSlug = '';
            $categoryPrettyName = '';
            if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
                 foreach ( $postTerms as $term ) {
                   $categoryFilterSlug .= ' ' . $term->slug;
                   $categoryPrettyName .= ' ' . $term->name . '<span class="divider">, </span>';
                 }
             }
         ?>

        <div class="faq-page-listing">

            <a href="<?php the_permalink(); ?>" class="faq-filter-image-link">
            <?php
                if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                    the_post_thumbnail('faq-thumb' , ['alt' => get_the_title()]);
                } else{
                    echo '<img alt="faq thumb" src="' . get_template_directory_uri() . '/assets/images/default-faq-thumb.jpg" />';
                }
            ?>
            </a>
           <div class="faq-meta">
             <span class="faq-category"><?php echo $categoryPrettyName; ?></span>
            </div>
            <a href="<?php the_permalink(); ?>" class="faq-filter-title-link"><?php the_title(); ?></a>

            <a href="<?php the_permalink(); ?>" class="continue-reading-button">Continue Reading &raquo;</a>

        </div>
          <?php endwhile; else : ?>
            <!-- IF NOTHING FOUND CONTENT HERE -->
          <?php endif; ?>
          <?php wp_reset_query(); ?>


</div> <!-- end .random-faq-loop -->

<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}

add_shortcode('cordisco-random-faq-loop-image', 'cordiscoRandomFAQLoopImage');

?>

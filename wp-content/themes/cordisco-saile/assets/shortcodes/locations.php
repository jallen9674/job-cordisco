<?php

function csLocations_rs($atts = null) {

    global $post;

    $queryAmount = $amount;

	
	 /* Get Current Post ID (For Grabbing Which Location to Display) */
    global $post;
    $locationCurrentPost = $post->ID;

    /*
    Get Current Location Taxonomy On Page
    */
    $postTerms =  wp_get_object_terms($post->ID, 'hc_location');
    $categoryFilterSlug = '';
    $categoryPrettyName = '';
    if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
         foreach ( $postTerms as $term ) {
           $categoryFilterSlug .= '' . $term->slug;
           $categoryPrettyName .= ' ' . $term->name;
         }
     }

    // before and after widget arguments are defined by themes

   //Widget Toggle
    if($postTerms && strpos(get_the_title(), 'Personal Injury') === false) :


//    echo $args['before_title'] . $title . $args['after_title'];

    //BEGIN FRONTEND OUTPUT
	
	
    ob_start();

?>

    

            <?php //BEGIN LOOP FOR MATCHING PAGES ?>
            <?php

	$tit = get_the_title();
	$pos = strpos(strtolower($tit), ' in ');
	$linkTitle = substr($tit, 0, $pos);

				global $post;
                 $args = array(
                  'posts_per_page' => 10,
                  'post_type' => 'page',
                  'meta_query' => array(
                        array(
                            'key'     => '_hc_location_widget_title',
                            'value'   => get_field('_hc_location_widget_title', $post->ID),
                            'compare' => '=',
                        ),
                   ),
                  'post__not_in' => array($locationCurrentPost),
                  'orderby' => 'rand',
                  'order' => 'asc'

                );

		
            $the_query = new WP_Query( $args );
            if ( $the_query->have_posts() ) : ?>
	<div class="location-widget-outer">
        <div class="location-widget-inner">

              <?php
              //Display Practice Area in Title
              $postTerms =  wp_get_object_terms($post->ID, 'hc_location');
              ?>
              <span class="location-widget-title">
                Locations
              </span>
            
            <ul class="location-widget-links">
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <?php //BEGIN OUTPUT ?>
                <?php $city = get_the_terms($post->ID, 'hc_location'); ?>
                <li class="single-location-link">
                    <a href="<?php the_permalink(); ?>"><?= $city[0]->name .' '. $linkTitle?></a>
                </li>
            <?php //END OUTPUT ?>
            <?php endwhile; ?>

				  </ul>
        </div>
    </div>
				
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            <?php //END LOOP FOR MATCHING PAGES ?>

          


<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
	
	endif;
}


add_shortcode('cs-locations-rs', 'csLocations_rs');
<?php

/*---------------------------------
BEGIN EBOOK WIDGET
[hc-pa-list]
---------------------------------*/
function hcPracticeAreaList($atts = null) {

    ob_start();
    //BEGIN OUTPUT
?>

<h2>Locations</h2>
<p><em>These are the locations where some practice areas have pages for each locale</em></p>
<ul>
    <li>Pennsylvania</li>
    <li>Bucks County</li>
    <li>Philadelphia</li>
    <li>Bristol</li>
    <li>Newtown</li>
</ul>


<h2>General Personal Injury</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/fatal-assault-and-battery-in-pennsylvania/">Fatal Assault and Battery in Pennsylvania [/fatal-assault-and-battery-in-pennsylvania/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/injured-by-falling-object-or-equipment-malfunction-bristol/">Injured by Falling Object or E [/injured-by-falling-object-or-equipment-malfunction-bristol/]quipment Malfunction Bristol</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/injured-in-fire-or-explosion-bristol/">Injured in a Fire or Explosion in Bristol [/injured-in-fire-or-explosion-bristol/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/malfunctioning-amusement-park-ride-accidents/">Injured on Amusement Park Ride in Pennsylvania [/malfunctioning-amusement-park-ride-accidents/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/near-drowning-and-drowning-accidents/">Near Drowning and Drowning Accidents [/near-drowning-and-drowning-accidents/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/nj-and-pa-personal-injury-attorneys-bar-fights/">NJ and PA Personal Injury Attorneys: Bar Fights [/nj-and-pa-personal-injury-attorneys-bar-fights/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/nj-and-pa-personal-injury-attorneys-liquor-liability/">Pennsylvania and New Jersey Dram Shop Laws [/nj-and-pa-personal-injury-attorneys-liquor-liability/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/personal-injury/">Personal Injury [/personal-injury/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/philadelphia-bucks-county-mass-transit-lawyers-in-langhorne/">Philadelphia, Bucks County Mass Transit Lawyers in Langhorne [/philadelphia-bucks-county-mass-transit-lawyers-in-langhorne/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/train-accident-lawyers-serving-bristol-and-philadelphia-pa/">Train Accident Lawyers Serving Bristol and Philadelphia PA [/train-accident-lawyers-serving-bristol-and-philadelphia-pa/]</a></li>
</ul>


<h2>Defective Medical Devices</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/defective-medical-device-lawyers/">Defective Medical Device Lawyers [/defective-medical-device-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/hip-replacements-complications-legal-options/">Hip Replacements Complications &amp; Legal Options [/hip-replacements-complications-legal-options/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/is-mirena-iud-safe/">Is Mirena IUD safe? [/is-mirena-iud-safe/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/knee-replacement-complications-may-warrant-civil-action/">Knee Replacement Complications May Warrant Civil Action [/knee-replacement-complications-may-warrant-civil-action/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/medtronic-recall-for-pain-pump/">Medtronic Recall for Pain Pump [/medtronic-recall-for-pain-pump/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/pa-morcellator-complications-and-morcellation-cancer-lawsuits/">PA Morcellator Complications and Morcellation Cancer Lawsuits [/pa-morcellator-complications-and-morcellation-cancer-lawsuits/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/transvaginal-mesh-lawyers/">Transvaginal Mesh Lawyers [/transvaginal-mesh-lawyers/]</a></li>
</ul>


<h2>Dangerous Drugs</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/defective-drug-attorneys/">Defective Drug Attorneys [/defective-drug-attorneys/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/defective-drug-lawyers/">Defective Drug Lawyers [/defective-drug-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/dangerous-products-and-drugs-lawyer-in-bristol/">Dangerous Products and Drugs Lawyer in Bristol [/dangerous-products-and-drugs-lawyer-in-bristol/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/actos-attorneys/">Actos Attorneys [/actos-attorneys/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/administering-taking-ssris-during-pregnancy/">Administering/Taking SSRIs During Pregnancy [/administering-taking-ssris-during-pregnancy/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/ambien-lawyers/">Ambien Lawyers [/ambien-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/celexa-lawyers/">Celexa Lawyers [/celexa-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/complications-from-tylenol-poisoning/">Complications from Tylenol Poisoning [/complications-from-tylenol-poisoning/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/dangerous-side-effects-linked-to-invokana-invokana-lawsuit/">Dangerous Side Effects Linked to Invokana | Invokana Lawsuit [/dangerous-side-effects-linked-to-invokana-invokana-lawsuit/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/dangerous-yasmin-yaz-side-effects/">Dangerous Yasmin-Yaz Side Effects [/dangerous-yasmin-yaz-side-effects/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/dangers-of-paxil-liability-for-injuries/">Dangers of Paxil &amp; Liability for Injuries [/dangers-of-paxil-liability-for-injuries/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/harm-due-to-avandia-side-effects/">Harm Due to Avandia Side Effects [/harm-due-to-avandia-side-effects/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/harm-due-to-byetta-side-effects/">Harm Due to Byetta Side Effects [/harm-due-to-byetta-side-effects/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/is-chantix-safe/">Is Chantix safe? [/is-chantix-safe/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/is-cymbalta-safe/">Is Cymbalta safe? [/is-cymbalta-safe/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/is-depakote-safe/">Is Depakote safe? [/is-depakote-safe/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/januvia-prescribing-information-liability-for-harm/">Januvia Prescribing Information &amp; Liability for Harm [/januvia-prescribing-information-liability-for-harm/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/legal-action-for-serious-xarelto-side-effects/">Legal Action for Serious Xarelto Side Effects [/legal-action-for-serious-xarelto-side-effects/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/pradaxa-prescribing-information/">Pradaxa Prescribing Information [/pradaxa-prescribing-information/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/risks-of-lexapro-for-anxiety-and-depression/">Risks of Lexapro for Anxiety and Depression [/risks-of-lexapro-for-anxiety-and-depression/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/risks-of-low-testosterone-treatment/">Risks of Low Testosterone Treatment [/risks-of-low-testosterone-treatment/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/risperdal-side-effects-liability-for-harm/">Risperdal Side Effects &amp; Liability for Harm [/risperdal-side-effects-liability-for-harm/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/rituxan-brain-infections/">Rituxan &amp; Brain Infections [/rituxan-brain-infections/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/serious-enbrel-side-effects/">Serious Enbrel Side Effects [/serious-enbrel-side-effects/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/talcum-powder-ovarian-cancer/">Talcum Powder &amp; Ovarian Cancer Lawsuit [/talcum-powder-ovarian-cancer/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/the-effects-of-prozac-liability-for-harm/">The Effects of Prozac &amp; Liability for Harm [/the-effects-of-prozac-liability-for-harm/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/topamax-side-effects-liability-for-damages/">Topamax Side Effects &amp; Liability for Damages [/topamax-side-effects-liability-for-damages/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/what-are-the-side-effects-of-zoloft/">What to Do: Harmed by Side Effects of Zoloft [/what-are-the-side-effects-of-zoloft/]</a></li>
</ul>


<h2>Medical Malpractice</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/air-bubbles-in-blood-med-mal-lawyers/">Air Bubbles in Blood | Medical Malpractice Lawyers [/air-bubbles-in-blood-med-mal-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/anesthesia-errors-lawyers/">Anesthesia Errors Lawyers [/anesthesia-errors-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/delayed-diagnosis-attorneys/">Delayed Diagnosis Attorneys [/delayed-diagnosis-attorneys/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/ectopic-pregnancy-and-prenatal-care-malpractice/">Ectopic Pregnancy and Prenatal Care Malpractice [/ectopic-pregnancy-and-prenatal-care-malpractice/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/healthcare-associated-infections-lawyers/">Medical &amp; Hospital Infection Lawyers [/healthcare-associated-infections-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/medical-malpractice-lawyers-in-newtown/">Medical Malpractice Lawyers in Newtown [/medical-malpractice-lawyers-in-newtown/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/medical-malpractice-lawyers-in-philadelphia/">Medical Malpractice Lawyers in Philadelphia [/medical-malpractice-lawyers-in-philadelphia/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/medical-misdiagnosis-lawyers/">Medical Misdiagnosis Lawyers [/medical-misdiagnosis-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/medical-malpractice-attorneys-in-bristol-pa-lawyers-at-cordisco-law/">Medical Malpractice Attorneys in Bristol, PA [/medical-malpractice-attorneys-in-bristol-pa-lawyers-at-cordisco-law/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/medical-malpractice-lawyers-in-bucks-county-pa-cordisco-law/">Medical Malpractice Lawyers in Bucks County [/medical-malpractice-lawyers-in-bucks-county-pa-cordisco-law/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/medication-errors-cordisco-law/">Medication Error Lawyers [/medication-errors-cordisco-law/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/pennsylvania-medical-malpractice-lawyers/">Pennsylvania Medical Malpractice Lawyers [/pennsylvania-medical-malpractice-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/philadelphia-medical-malpractice-lawyer/">Philadelphia Medical Malpractice Lawyer [/philadelphia-medical-malpractice-lawyer/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/retained-foreign-object-surgical-error/">Retained Foreign Object Surgical Error [/retained-foreign-object-surgical-error/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/saxagliptin-side-effects-and-liability-for-complications/">Saxagliptin Side Effects and Liability for Complications [/saxagliptin-side-effects-and-liability-for-complications/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/surgery-on-the-wrong-body-part-medical-malpractice/">Surgery on the Wrong Body Part &amp; Medical Malpractice [/surgery-on-the-wrong-body-part-medical-malpractice/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/surgical-error-lawyers/">Surgical Error Lawyers [/surgical-error-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/wrong-patient-surgery-resulting-in-medical-malpractice/">Wrong Patient Surgery Resulting in Medical Malpractice [/wrong-patient-surgery-resulting-in-medical-malpractice/]</a></li>
</ul>


<h2>Birth Injury</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/anemia-while-pregnant-can-cause-a-birth-injury/">Anemia While Pregnant Can Cause a Birth Injury [/anemia-while-pregnant-can-cause-a-birth-injury/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/birth-injury-lawyers-in-bristol/">Birth Injury Lawyers in Bristol [/birth-injury-lawyers-in-bristol/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/birth-injury-lawyers-in-bucks-county/">Birth Injury Lawyers in Bucks County [/birth-injury-lawyers-in-bucks-county/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/birth-injury-lawyers-in-newtown/">Birth Injury Lawyers in Newtown [/birth-injury-lawyers-in-newtown/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/birth-injury-lawyers-in-pennsylvania/">Birth Injury Lawyers in Pennsylvania [/birth-injury-lawyers-in-pennsylvania/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/cerebral-palsy-birth-injury/">Cerebral Palsy Birth Injury [/cerebral-palsy-birth-injury/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/complications-during-labor/">Complications During Labor [/complications-during-labor/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/complications-of-contagious-infection-during-pregnancy/">Complications of Contagious Infection During Pregnancy [/complications-of-contagious-infection-during-pregnancy/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/delayed-cesarean-section/">Delayed Cesarean Section [/delayed-cesarean-section/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/erb-s-palsy-birth-injury-in-pennsylvania/">Erb’s Palsy Birth Injury in Pennsylvania [/erb-s-palsy-birth-injury-in-pennsylvania/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/failure-to-diagnose-gestational-diabetes/">Failure to Diagnose Gestational Diabetes [/failure-to-diagnose-gestational-diabetes/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/failure-to-diagnose-medical-conditions-in-pregnancy/">Failure to Diagnose Medical Conditions in Pregnancy [/failure-to-diagnose-medical-conditions-in-pregnancy/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/fetal-birth-defects-after-negligent-prenatal-care-in-pennsylvania/">Fetal Birth Defects After Negligent Prenatal Care in Pennsylvania [/fetal-birth-defects-after-negligent-prenatal-care-in-pennsylvania/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/fetal-distress/">Fetal Distress [/fetal-distress/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/forceps-or-vacuum-extractor-injuries/">Forceps or Vacuum Extractor Injuries [/forceps-or-vacuum-extractor-injuries/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/genital-herpes-during-pregnancy/">Genital Herpes During Pregnancy [/genital-herpes-during-pregnancy/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/hypoglycemia-in-pregnancy/">Hypoglycemia in Pregnancy [/hypoglycemia-in-pregnancy/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/klumpke-s-palsy/">Klumpke’s Palsy [/klumpke-s-palsy/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/legal-options-for-victims-of-negligent-prenatal-care/">Legal Options for Victims of Negligent Prenatal Care [/legal-options-for-victims-of-negligent-prenatal-care/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/medical-negligence-during-birth-recovering-damages/">Medical Negligence during Birth &amp; Recovering Damages [/medical-negligence-during-birth-recovering-damages/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/medical-negligences-role-in-neonatal-lupus/">Medical Negligence’s Role in Neonatal Lupus [/medical-negligences-role-in-neonatal-lupus/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/philadelphia-birth-injury-lawyers/">Philadelphia Birth Injury Lawyers [/philadelphia-birth-injury-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/preeclampsia-treatments-and-liability/">Preeclampsia Treatments and Liability [/preeclampsia-treatments-and-liability/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/pregnant-with-hiv-or-other-contagious-disease-in-pennsylvania/">Pregnant with HIV or Other Contagious Disease in Pennsylvania [/pregnant-with-hiv-or-other-contagious-disease-in-pennsylvania/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/rh-incompatibility/">Rh Incompatibility [/rh-incompatibility/]</a></li>
</ul>


<h2>Dog Bite Accidents</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/dog-bite-attorney-in-bristol/">Dog Bite Attorney in Bristol [/dog-bite-attorney-in-bristol/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/dog-bite-lawyers-in-bucks-county-pa/">Dog Bite Lawyers in Bucks County, PA [/dog-bite-lawyers-in-bucks-county-pa/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/dog-bite-lawyers-in-newtown/">Dog Bite Lawyers in Newtown [/dog-bite-lawyers-in-newtown/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/pennsylvania-dog-bite-lawyers/">Pennsylvania Dog Bite Lawyers [/pennsylvania-dog-bite-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/philadelphia-dog-bite-lawyers/">Philadelphia Dog Bite Lawyers [/philadelphia-dog-bite-lawyers/]</a></li>
</ul>


<h2>Car Accidents</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/automobile-accident-attorneys-in-newtown/">Automobile Accident Attorneys in Newtown [/automobile-accident-attorneys-in-newtown/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/automobile-accident-lawyers-in-pennsylvania/">Automobile Accident Lawyers in Pennsylvania [/automobile-accident-lawyers-in-pennsylvania/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/automotive-defect-attorneys/">Automotive Defect Attorneys [/automotive-defect-attorneys/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/bristol-pennsylvania-motor-vehicle-accident-lawyer/">Bristol Motor Vehicle Accident Lawyer [/bristol-pennsylvania-motor-vehicle-accident-lawyer/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/bucks-county-car-accident-lawyers-langhorne/">Bucks County Car Accident Lawyers in Langhorne [/bucks-county-car-accident-lawyers-langhorne/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/car-accident-lawyers-in-bucks-county-pa/">Car Accident Lawyers in Bucks County, PA [/car-accident-lawyers-in-bucks-county-pa/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/fatal-auto-accident-in-bucks-county-philadelphia-attorney-for-wrongful-death/">Fatal Auto Accident in Bucks County | Philadelphia Attorney for wrongful death [/fatal-auto-accident-in-bucks-county-philadelphia-attorney-for-wrongful-death/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/fatal-car-accident-in-bristol/">Fatal Car Accident in Bristol [/fatal-car-accident-in-bristol/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/head-on-collisions/">Head-On Collisions [/head-on-collisions/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/hit-and-run-accidents/">Hit and Run Accidents [/hit-and-run-accidents/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/motor-vehicle-accident-attorneys-bucks-county/">Motor Vehicle Accident Attorneys Bucks County [/motor-vehicle-accident-attorneys-bucks-county/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/motor-vehicle-accident-attorneys-in-pennsylvania/">Motor Vehicle Accident Attorneys in Pennsylvania [/motor-vehicle-accident-attorneys-in-pennsylvania/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/motor-vehicle-accident-lawyer-in-philadelphia/">Motor Vehicle Accident Lawyer in Philadelphia [/motor-vehicle-accident-lawyer-in-philadelphia/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/motor-vehicle-accident-lawyers-in-newtown/">Motor Vehicle Accident Lawyers in Newtown [/motor-vehicle-accident-lawyers-in-newtown/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/motorcycle-accident-lawyers-in-bucks-county/">Motorcycle Accident Lawyers in Bucks County [/motorcycle-accident-lawyers-in-bucks-county/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/motorcycle-accident-lawyers-in-newtown/">Motorcycle Accident Lawyers in Newtown [/motorcycle-accident-lawyers-in-newtown/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/motorcycle-accident-lawyers-in-bristol/">Motorcycle Accident Lawyers in Newtown, Bucks County [/motorcycle-accident-lawyers-in-bristol/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/bristol-car-accident-lawyer/">Newtown PA Car Accident Lawyer in Bucks County [/bristol-car-accident-lawyer/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/nj-and-pa-personal-injury-attorneys-drunk-driving-accidents/">NJ and PA Personal Injury Attorneys: Drunk Driving Accidents [/nj-and-pa-personal-injury-attorneys-drunk-driving-accidents/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/nj-and-pa-personal-injury-attorneys-hit-and-run-accidents/">NJ and PA Personal Injury Attorneys: Hit-and-Run Accidents [/nj-and-pa-personal-injury-attorneys-hit-and-run-accidents/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/philadelphia-auto-accident-lawyers/">Philadelphia Car Accident Lawyers [/philadelphia-auto-accident-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/philadelphia-distracted-driving-accident-lawyers-bucks-mercer/">Philadelphia distracted driving accident lawyers | Bucks &amp; Mercer [/philadelphia-distracted-driving-accident-lawyers-bucks-mercer/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/rear-end-collision-in-bristol/">Rear-End Collision in Bristol [/rear-end-collision-in-bristol/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/rollover-accident-lawyers/">Rollover Accident Lawyers [/rollover-accident-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/single-vehicle-accidents/">Single-Vehicle Accidents [/single-vehicle-accidents/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/t-bone-accidents-side-impact-collisions/">T-Bone Accidents and Side Impact Collisions [/t-bone-accidents-side-impact-collisions/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/unintentional-acceleration-attorneys/">Unintentional Acceleration Attorneys [/unintentional-acceleration-attorneys/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/drunk-driver-accident-lawyers-in-bucks-county/">Drunk Driver Accident Lawyers in Bucks County [/drunk-driver-accident-lawyers-in-bucks-county/]</a></li>
</ul>


<h2>Truck Accidents</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/bucks-county-personal-injury-attorney-pa-nj-truck-accident-lawyer/">Bucks County Personal Injury Attorney | PA &amp; NJ Truck Accident Lawyer [/bucks-county-personal-injury-attorney-pa-nj-truck-accident-lawyer/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/bucks-county-truck-accident-attorney/">Bucks County Truck Accident Attorney [/bucks-county-truck-accident-attorney/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/tractor-trailer-accident-attorneys-in-bristol-cordisco-law/">Tractor Trailer Accident Attorneys in Bristol [/tractor-trailer-accident-attorneys-in-bristol-cordisco-law/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/truck-accident-lawyer-in-pennsylvania/">Truck Accident Lawyer in Pennsylvania [/truck-accident-lawyer-in-pennsylvania/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/truck-accident-lawyer-in-philadelphia/">Truck Accident Lawyer in Philadelphia [/truck-accident-lawyer-in-philadelphia/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/truck-accident-lawyers-in-bucks-county-pa/">Truck Accident Lawyers in Bucks County, PA [/truck-accident-lawyers-in-bucks-county-pa/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/truck-accident-lawyers-in-newtown/">Truck Accident Lawyers in Newtown [/truck-accident-lawyers-in-newtown/]</a></li>
</ul>


<h2>Bus Accidents</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/bus-accident-attorneys-in-newtown/">Bus Accident Attorneys in Newtown [/bus-accident-attorneys-in-newtown/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/bus-accident-attorneys-in-pennsylvania/">Bus Accident Attorneys in Pennsylvania [/bus-accident-attorneys-in-pennsylvania/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/bus-accident-attorneys-in-philadelphia/">Bus Accident Attorneys in Philadelphia [/bus-accident-attorneys-in-philadelphia/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/bus-accident-lawyer-in-bristol/">Bus Accident Lawyer in Bristol [/bus-accident-lawyer-in-bristol/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/bus-accident-lawyers-in-bucks-county-pa/">Bus Accident Lawyers in Bucks County, PA [/bus-accident-lawyers-in-bucks-county-pa/]</a></li>
</ul>


<h2>Motorcycle Accidents</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/bucks-county-attorney-motorcycle-accidents-philadelphia-pa-lawyer/">Bucks County Attorney | Motorcycle Accidents | Philadelphia, PA Lawyer [/bucks-county-attorney-motorcycle-accidents-philadelphia-pa-lawyer/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/pennsylvania-motorcycle-accident-attorneys/">Pennsylvania Motorcycle Accident Attorneys [/pennsylvania-motorcycle-accident-attorneys/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/philadelphia-motorcycle-accident-lawyers/">Philadelphia Motorcycle Accident Lawyers [/philadelphia-motorcycle-accident-lawyers/]</a></li>
</ul>


<h2>Bicycle Accidents</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/bicycle-accident-lawyer-in-bristol/">Bicycle Accident Lawyer in Newtown, Bucks County PA [/bicycle-accident-lawyer-in-bristol/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/bicycle-accident-lawyers-in-newtown/">Bicycle Accident Lawyers in Newtown [/bicycle-accident-lawyers-in-newtown/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/bicycle-accident-lawyers-in-pennsylvania/">Bicycle Accident Lawyers in Pennsylvania [/bicycle-accident-lawyers-in-pennsylvania/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/bucks-county-bicycle-accident-attorneys/">Bucks County Bicycle Accident Attorneys [/bucks-county-bicycle-accident-attorneys/]</a></li>
</ul>


<h2>Pedestrian Accidents</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/pedestrian-accident-attorneys-in-pennsylvania/">Pedestrian Accident Attorneys in Pennsylvania [/pedestrian-accident-attorneys-in-pennsylvania/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/pedestrian-accident-lawyer-in-bristol/">Pedestrian Accident Lawyer in Bristol [/pedestrian-accident-lawyer-in-bristol/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/pedestrian-accident-lawyer-in-philadelphia/">Pedestrian Accident Lawyer in Philadelphia [/pedestrian-accident-lawyer-in-philadelphia/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/pedestrian-accident-lawyers-in-bucks-county/">Pedestrian Accident Lawyers in Bucks County [/pedestrian-accident-lawyers-in-bucks-county/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/pedestrian-accident-lawyers-in-newtown/">Pedestrian Accident Lawyers in Newtown [/pedestrian-accident-lawyers-in-newtown/]</a></li>
</ul>


<h2>Bicycle & Pedestrian Accidents</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/bucks-county-pedestrian-bike-accident-philadelphia-accident-lawyer/">Bucks County Pedestrian – Bike Accident | Philadelphia Accident Lawyer [/bucks-county-pedestrian-bike-accident-philadelphia-accident-lawyer/]</a></li>
</ul>


<h2>Other Vehicle Accidents</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/atv-accident-attorney-in-bucks-county/">ATV Accident Attorney in Bucks County [/atv-accident-attorney-in-bucks-county/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/atv-accident-lawyer-in-bristol/">ATV Accident Lawyer in Bristol [/atv-accident-lawyer-in-bristol/]</a></li>
</ul>


<h2>Nursing Home Accidents</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/bed-sores-in-the-elderly-in-pennsylvania-nursing-homes/">Bed Sores in the Elderly in Pennsylvania Nursing Homes  [/bed-sores-in-the-elderly-in-pennsylvania-nursing-homes/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/elder-sexual-abuse-in-pennsylvania/">Elder Sexual Abuse in Pennsylvania [/elder-sexual-abuse-in-pennsylvania/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/nj-and-pa-personal-injury-attorneys-nursing-home-abuse/">NJ and PA Personal Injury Attorneys: Nursing Home Abuse [/nj-and-pa-personal-injury-attorneys-nursing-home-abuse/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/nursing-home-abuse-attorneys-in-newtown/">Nursing Home Abuse Attorneys in Newtown [/nursing-home-abuse-attorneys-in-newtown/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/nursing-home-abuse-attorneys-in-pennsylvania/">Nursing Home Abuse Attorneys in Pennsylvania [/nursing-home-abuse-attorneys-in-pennsylvania/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/nursing-home-abuse-lawyer-in-bristol/">Nursing Home Abuse Lawyer in Bristol [/nursing-home-abuse-lawyer-in-bristol/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/nursing-home-abuse-lawyers-in-bucks-county/">Nursing Home Abuse Lawyers in Bucks County [/nursing-home-abuse-lawyers-in-bucks-county/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/pennsylvania-nursing-home-injuries-broken-bones/">Pennsylvania Nursing Home Injuries: Broken Bones [/pennsylvania-nursing-home-injuries-broken-bones/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/philadelphia-nursing-home-abuse-and-neglect-lawyers/">Philadelphia Nursing Home Abuse and Neglect Lawyers [/philadelphia-nursing-home-abuse-and-neglect-lawyers/]a</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/physical-abuse-in-nursing-homes-in-pennsylvania/">Physical Abuse in Nursing Homes in Pennsylvania [/physical-abuse-in-nursing-homes-in-pennsylvania/]</a></li>
</ul>


<h2>Product Liability</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/accident-because-of-faulty-fuel-pump/">Accident Because of Faulty Fuel Pump [/accident-because-of-faulty-fuel-pump/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/faulty-ignition-switch-lawyer/">Faulty Ignition Switch Lawyer [/faulty-ignition-switch-lawyer/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/gas-pedal-problem-lawyer/">Gas Pedal Problem Lawyer [/gas-pedal-problem-lawyer/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/injured-because-of-defective-airbag/">Injured Because of Defective Airbag [/injured-because-of-defective-airbag/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/lawyer-for-dashboard-instrument-panel-defect/">Lawyer for Dashboard Instrument Panel Defect [/lawyer-for-dashboard-instrument-panel-defect/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/auto-accident-because-of-defective-brakes/">Auto Accident Because of Defective Brakes [/auto-accident-because-of-defective-brakes/]</a></li>
</ul>


<h2>Worker's Compensation</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/burns-from-a-fire-in-the-workplace/">Burns from a Fire in the Workplace [/burns-from-a-fire-in-the-workplace/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/chemical-burns-in-the-workplace-can-lead-to-workers-comp/">Chemical Burns in the Workplace Can Lead to Workers’ Comp [/chemical-burns-in-the-workplace-can-lead-to-workers-comp/] </a></li>
    <li><a href="http://cordiscosaile.wpengine.com/fall-from-height-work-injury-attorneys/">Fall from Height | Work Injury Attorneys [/fall-from-height-work-injury-attorneys/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/injured-by-hazardous-materials-at-work/">Injured by Hazardous Materials at Work [/injured-by-hazardous-materials-at-work/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/nj-and-pa-personal-injury-attorneys-construction-accidents/">NJ and PA Personal Injury Attorneys: Construction Accidents [/nj-and-pa-personal-injury-attorneys-construction-accidents/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/on-the-job-injury-due-to-defective-equipment/">On-the-Job Injury due to Defective Equipment [/on-the-job-injury-due-to-defective-equipment/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/pennsylvania-workers-compensation-lawyers/">Pennsylvania Workers’ Compensation Lawyers [/pennsylvania-workers-compensation-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/pennsylvania-workplace-injuries-electrical-burns/">Pennsylvania Workplace Injuries: Electrical Burns [/pennsylvania-workplace-injuries-electrical-burns/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/philadelphia-workplace-injury-lawyers/">Philadelphia Workplace Injury Lawyers [/philadelphia-workplace-injury-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/trapped-falling-structure-work-injury-lawyer/">Trapped by Falling Structure | Work Injury Lawyer [/trapped-falling-structure-work-injury-lawyer/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/unsafe-work-conditions-lawyers/">Unsafe Work Conditions Lawyers [/unsafe-work-conditions-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/work-vehicle-accident-attorneys/">Work Vehicle Accident Attorneys [/work-vehicle-accident-attorneys/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/work-related-wrongful-death-in-pennsylvania/">Work-Related Wrongful Death in Pennsylvania [/work-related-wrongful-death-in-pennsylvania/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/workers-compensation-lawyers-in-newtown/">Workers’ Compensation Lawyers in Newtown [/workers-compensation-lawyers-in-newtown/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/burn-injury-attorneys-workplace-accidents/">Workplace Accident – Burn Injury [/burn-injury-attorneys-workplace-accidents/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/workplace-injury-lawyer-in-bristol/">Workplace Injury Lawyer in Bristol [/workplace-injury-lawyer-in-bristol/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/workplace-injury-lawyers-in-bucks-county/">Workplace Injury Lawyers Bucks County [/workplace-injury-lawyers-in-bucks-county/]</a></li>
</ul>

<h2>Premises Liability</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/escalator-or-elevator-accidents/">Escalator or Elevator Accidents [/escalator-or-elevator-accidents/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/nj-and-pa-personal-injury-attorneys-slip-and-fall/">NJ and PA Personal Injury Attorneys: Slip and Fall [/nj-and-pa-personal-injury-attorneys-slip-and-fall/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/philadelphia-premises-liability-lawyers/">Philadelphia Premises Liability Lawyers [/philadelphia-premises-liability-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/premises-liability-attorney-bristol-pennsylvania/">Premises Liability Attorney Bristol PA [/premises-liability-attorney-bristol-pennsylvania/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/premises-liability-attorneys-in-newtown/">Premises Liability Attorneys in Newtown [/premises-liability-attorneys-in-newtown/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/premises-liability-attorneys-in-pennsylvania/">Premises Liability Attorneys in Pennsylvania [/premises-liability-attorneys-in-pennsylvania/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/premises-liability-lawyers-in-bucks-county-pa/">Premises Liability Lawyers in Bucks County, PA [/premises-liability-lawyers-in-bucks-county-pa/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/slip-and-fall-accident-in-pennsylvania/">Slip and Fall Accident Pennsylvania [/slip-and-fall-accident-in-pennsylvania/]</a></li>
</ul>

<h2>Wrongful Death</h2>
<ul>
    <li><a href="http://cordiscosaile.wpengine.com/pennsylvania-wrongful-death-attorneys/">Pennsylvania Wrongful Death Attorneys [/pennsylvania-wrongful-death-attorneys/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/philadelphia-wrongful-death-lawyers/">Philadelphia Wrongful Death Lawyers [/philadelphia-wrongful-death-lawyers/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/wrongful-death-attorney-bristol/">Wrongful Death Attorney in Newtown, Bucks County [/wrongful-death-attorney-bristol/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/wrongful-death-attorneys-in-newtown/">Wrongful Death Attorneys in Newtown [/wrongful-death-attorneys-in-newtown/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/wrongful-death-claim-excessive-use-of-deadly-force/">Wrongful Death Claim &amp; Excessive Use of Deadly Force [/wrongful-death-claim-excessive-use-of-deadly-force/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/wrongful-death-from-medical-malpractice/">Wrongful Death from Medical Malpractice [/wrongful-death-from-medical-malpractice/]</a></li>
    <li><a href="http://cordiscosaile.wpengine.com/wrongful-death-lawyers-in-bucks-county/">Wrongful Death Lawyers in Bucks County [/wrongful-death-lawyers-in-bucks-county/]</a></li>
</ul>

<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}

add_shortcode('hc-pa-list', 'hcPracticeAreaList');

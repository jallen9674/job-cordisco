<?php

/*---------------------------------
BEGIN CAR ACCIDENT EBOOK FORM WIDGET
[hc-ebook-form-car-accident]
---------------------------------*/
function hcEbookFormCarAccident($atts = null) {

    ob_start();
    //BEGIN OUTPUT
?>

<form accept-charset="UTF-8" action="https://mr207.infusionsoft.com/app/form/process/dc6e8cc525a5003e3529745a0599d899" class="infusion-form standard-form ebook-form" method="POST">
    <h2>Download Free Copy Now</h2>
    <input name="inf_form_xid" type="hidden" value="dc6e8cc525a5003e3529745a0599d899" />
    <input name="inf_form_name" type="hidden" value="Web Form submitted" />
    <input name="infusionsoft_version" type="hidden" value="1.65.0.51" />
    <div class="infusion-field wpcf7-form-control-wrap your-name">
        <input class="infusion-field-input-container your-name" id="inf_field_FirstName" name="inf_field_FirstName" type="text" placeholder="First Name (required)" />
    </div>
    <div class="infusion-field wpcf7-form-control-wrap your-name">
        <input class="infusion-field-input-container your-name" id="inf_field_LastName" name="inf_field_LastName" type="text" placeholder="Last Name (required)" />
    </div>
    <div class="infusion-field wpcf7-form-control-wrap your-email">
        <input class="infusion-field-input-container your-email" id="inf_field_Email" name="inf_field_Email" type="text" placeholder="Your Email (required)" />
    </div>
    <div class="infusion-submit">
        <input type="submit" value="Submit" />
    </div>
</form>
<script type="text/javascript" src="https://mr207.infusionsoft.com/app/webTracking/getTrackingCode"></script>



<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}

add_shortcode('hc-ebook-form-car-accident', 'hcEbookFormCarAccident');



/*---------------------------------
BEGIN BEST LAWYER EBOOK FORM WIDGET
[hc-ebook-form-best-lawyer]
---------------------------------*/
function hcEbookFormBestLawyer($atts = null) {

    ob_start();
    //BEGIN OUTPUT
?>

<form accept-charset="UTF-8" action="https://mr207.infusionsoft.com/app/form/process/3365c076b1dcedd4ccd02b507b8f9d26" class="infusion-form standard-form ebook-form" method="POST">
    <h2>Download Free Copy Now</h2>
    <input name="inf_form_xid" type="hidden" value="3365c076b1dcedd4ccd02b507b8f9d26" />
    <input name="inf_form_name" type="hidden" value="Not Another Bad Web Form" />
    <input name="infusionsoft_version" type="hidden" value="1.65.0.51" />
    <div class="infusion-field wpcf7-form-control-wrap your-name">
        <input class="infusion-field-input-container your-name" id="inf_field_FirstName" name="inf_field_FirstName" type="text" placeholder="Your Name (required)" />
    </div>
    <div class="infusion-field wpcf7-form-control-wrap your-name">
        <input class="infusion-field-input-container your-name" id="inf_field_LastName" name="inf_field_LastName" type="text" placeholder="Your Name (required)" />
    </div>
    <div class="infusion-field wpcf7-form-control-wrap your-email">
        <input class="infusion-field-input-container your-email" id="inf_field_Email" name="inf_field_Email" type="text" placeholder="Your Email (required)" />
    </div>
    <div class="infusion-submit">
        <input type="submit" value="Submit" />
    </div>
</form>
<script type="text/javascript" src="https://mr207.infusionsoft.com/app/webTracking/getTrackingCode"></script>


<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}

add_shortcode('hc-ebook-form-best-lawyer', 'hcEbookFormBestLawyer');


<!doctype html>
<html amp <?php echo AMP_HTML_Utils::build_attributes_string( $this->get( 'html_tag_attributes' ) ); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
    <script async custom-element="amp-analytics"  src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
	<?php if(get_the_ID() !== 3334) { ?>
		<script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
		<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
	<?php } ?>
    <?php do_action( 'amp_post_template_head', $this ); ?>


    <?php  //Setting Template Variables
        $siteName = 'Cordisco and Saile';
        $phoneNumber = '215-642-2335';
        $accentColor = '#008ac6'; //Red
        $textColor = '#333';
        $titleColor = '#295985';
        $analyticsID =  "UA-94083004-1";
        $titleBackground = '#295985'; // Blue
        $metaColor = '#777';
        $linkColor = '#008ac6'; //Red
        $linkHover = '#295985';
    ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <style amp-custom>
        .swp-hidden-panel-wrap {
            display:  none;
        }

        body {
            font-family: 'Roboto', Serif;
            font-size: 16px;
            line-height: 1.8;
            background: #fff;
            color: <?php echo $textColor; ?>;
            padding-bottom: 100px;
            margin:  0;
        }

        .wrapper {
            max-width: 600px;
            margin: 0 auto;
            padding: 0 10px;
        }

        .amp-content {
            padding:  0 16px 16px 16px;
            overflow-wrap: break-word;
            word-wrap: break-word;
            font-weight: 400;
            color: <?php echo $textColor; ?>;
        }

        amp-video{
            margin:  20px auto;
            max-width: 568px;
        }

        .amp-wp-article{
            max-width:  600px;
            margin:  20px auto;
            text-align: left;
            overflow: hidden;
        }

        .amp-wp-article amp-img {
            display: block;
            margin: 0 auto 20px auto;
            float: none;
            border-radius: 3px;
            box-shadow: 0 2px 4px rgba(0,0,0,0.4);
        }


        p,
        ol,
        ul,
        figure {
            margin: 0 0 24px 0;
        }

        a,
        a:visited {
            color: <?php echo $linkColor; ?>;
        }

        a:hover,
        a:active,
        a:focus {
            color: <?php echo $linkHover; ?>;
        }

        /* Header Styles */

         .title-bar {
            padding: 10px 0;
            background-image: -webkit-linear-gradient(top,#fff,#f8f8f8);
            background-image: -moz-linear-gradient(top,#fff,#f8f8f8);
            background-image: -o-linear-gradient(top,#fff,#f8f8f8);
            background-image: linear-gradient(top,#fff,#f8f8f8);
            width:  100%;
        }

        .title-bar amp-img {
            display: inline-block;
            vertical-align: top;
            padding: 0;
            margin: 0 auto;
        }

        .logo-link {
            padding-top: 15px;
            padding-bottom: 16px;
            display: inline-block;
        }

        .main-logo {
            height: auto;
            width: 260px;
        }

        @media screen and (max-width: 480px){
            .main-logo {
                height: auto;
                width: 215px;
                margin-top:  20px;
            }
        }

        .page-title {
            margin-top: 15px;
            color:  <?php echo $titleColor; ?>;
        }

        h1, h2, h3 {
            color:  <?php echo $titleColor; ?>;
            font-weight: normal;
            line-height: 1.2;
        }

        h1 b, h2 b, h3 b, h4 b, h1 strong, h2 strong, h3 strong, h4 strong {
            font-weight: normal;
        }

        .phone-icon-amp{
            color: white;
            height: 40px;
            width: 40px;
            fill: #fff;
            display: block;
            margin: 7px auto;
        }

        .phone-link{
			background: #cc3434 url('<?php echo get_stylesheet_directory_uri(); ?>/assets/amp/phone-icon.jpg') 14px center  no-repeat;
			color: #fff;
			padding: 15px 15px 15px 40px;
			font-weight: 600;
			font-size: 16px;
			text-decoration: none;
			line-height: 1;
			text-transform: uppercase;
			margin: 25px 0 0 0;
			float: right;
        }

		.phone-link:visited { clolor: #fff; }

        .phone-link:hover{
            color: #fff;
            text-decoration: none;
        }

        /* Navigation Styles */
        .amp-navigation{
            text-align: center;
            width: 100%;
            margin: 0;
            padding: 0;
            box-shadow: 0 3px 3px rgba(0,0,0,0.3);
            background:  #2a5884;
        }

        .amp-navigation ul{
            margin:  0;
            padding:  0;
        }

        .amp-navigation li{
            display: inline-block;
            margin: 0;
            text-align: center;
        }

        .amp-navigation a{
            text-align: center;
            padding-right: 10px;
            display:  inline-block;
            padding:  5px 10px;
            color:  white;
            transition: all 0.2s;
            text-decoration: none;
        }

        .amp-navigation a:hover{
            background:  #c59c17;
            text-decoration: none;
        }


        /* Footer */
        footer{
            text-align: center;
            color: #777;
            font-size: 13px;
        }

        /* Footer Phone */
        .footer-contact {
            padding-bottom: 10px;
            padding-top: 10px;
            border-bottom: 1px solid #eaeaea;
            border-top: 1px solid #eaeaea;
            display: block;
            max-width: 600px;
            margin: 0 auto 10px auto;
        }
        .footer-phone-content {
            font-size: 20px;
            text-align: center;
            display: block;
            line-height: 1;
            margin: 10px auto;
            padding: 0 10px;
        }

        .footer-phone-number {
            font-size: 28px;
            text-align: center;
            display: block;
            margin: 10px auto;
            line-height: 1;
            padding: 0 10px;
        }

        .footer-phone-number a {
            text-decoration: none;
            color: <?php echo $linkColor ?>;
            text-align: center;
        }

        /* Disclaimer */

        .disclaimer {
            background: #e0e0e0;
            border:  1px solid #ccc;
            padding:  10px;
            margin:  20px auto;
        }

        .disclaimer--title{
            font-size:  20px;
            display:  block;
            margin-bottom:  5px;
        }

        .disclaimer--content{
            font-size:  14px;
        }

        /* General Article */
        .amp-wp-article {
            padding-left: 10px;
            padding-right: 10px;
        }

        .addtoany_content_bottom {
            display: none;
        }

        .ufaq-faq-title.ufaq-faq-toggle {
            display: none;
        }

        .ufaq-faq-body {
            margin-bottom: 20px;
        }

		/* AMP Form Styles */

        .footer-form-heading {
            display: block;
            text-align: center;
            margin-top: 10px;
        }
        .wpcf7-form {
            display: block;
            margin: 20px auto;
            text-align: left;
            padding: 10px;
            box-sizing: border-box;
            width: 100%;
            max-width: 500px;
        }
        .wpcf7-form p {
            margin-bottom: 10px;
        }

        .wpcf7-form input[type="text"], .wpcf7-form input[type="email"], .wpcf7-form input[type="tel"], .wpcf7-form textarea {
            padding: 15px;
            border: 1px solid #ccc;
            border-radius: 3px;
            font-family: inherit;
            box-sizing: border-box;
            margin-bottom: 5px;
            font-size: 1rem;
            box-shadow: 0 1px 2px rgba(0,0,0,0.4) inset;
            width: 100%;
        }
        .wpcf7-form textarea {
            height: 200px;
        }
        .wpcf7-form input[type="submit"] {
            padding: 15px;
            border: 1px solid #ccc;
            border-radius: 3px;
            box-shadow: 0 1px 2px rgba(0,0,0,0.4) inset;
            width: 100%;
            cursor: pointer;
            text-transform: uppercase;
            color: #fff;
            font-weight: bold;
            font-size: 1em;
            background: #2a5884;
            transition: 0.2s;
            max-width: 400px;
            display: block;
            margin: 20px auto;
        }
        .wpcf7-form input[type="submit"]:hover {
            color: #fff;
            background: #285eab;
        }

        .wpcf7-form .your-info {
            display: none;
        }

        .wpcf7-form .submit-disclaimer-text, .wpcf7-form .general-disclaimer-text {
            font-size: 14px;
            font-style: italic;
        }

        /*Form Validation Styles*/
        .ampcf7-error {
            font-size: 12px;
            font-style: italic;
            color: #fff;
            background: #f00;
            border: #dd0000;
            padding: 10px;
        }

        .ampcf7-success {
            font-size: 12px;
            font-style: italic;
            color: #fff;
            background: #168416;
            border: #00dd00;
            padding: 10px;
        }

        .ampcf7-error p, .ampcf7-success p {
            margin-bottom: 0;
        }

        @media screen and (max-width: 430px){

            .phone-link{
                float: none;
				margin: 10px auto;
				width: 120px;
				display: block;
            }

			.logo-link {
				padding-top: 15px;
				padding-bottom: 16px;
				display: block;
				margin: 0 auto;
				float: none;
				width: 220px;
			}

			h1{
                font-size:  24px;
            }

            h2{
                font-size:  20px;
            }

        }

        @media screen and (min-width: 550px){
            .two-column{
                -moz-column-count: 2;
                -moz-column-gap: 20px;
                -webkit-column-count: 2;
                -webkit-column-gap: 20px;
                column-count: 2;
                column-gap: 20px;
            }
        }
    </style>
</head>

<body class="<?php echo esc_attr( $this->get( 'body_class' ) ); ?> etst">


<div class="title-bar">
    <div class="wrapper">
        <a href="<?php echo site_url(); ?>" class="logo-link">
            <amp-img
                src="<?php echo get_stylesheet_directory_uri(); ?>/assets/amp/amp-logo.png"
                alt="Cordisco Saile Logo"
                width="260" height="64"
                layout="responsive"
                class="main-logo">
            </amp-img>
        </a>

        <a href="tel:+1-<?php echo  $phoneNumber; ?>" class="phone-link">Click to call</a>
    </div>
</div>

<nav class="amp-navigation">
    <div class="wrapper">
        <?php wp_nav_menu(array(
            'container' => false,                           // remove nav container
            'container_class' => 'menu cf',                 // class of container (should you choose to use it)
            'menu' => __( 'The Amp Menu', 'bonestheme' ),  // nav name
            'menu_class' => 'nav amp-nav cf',               // adding custom nav class
            'theme_location' => 'amp-nav',                 // where it's located in the theme
            'before' => '',                                 // before the menu
            'after' => '',                                  // after the menu
            'link_before' => '',                            // before each link
            'link_after' => '',                             // after each link
            'depth' => 0,                                   // limit the depth of the nav
            'fallback_cb' => ''                             // fallback function (if there is one)
        )); ?>
    </div>
</nav>


<article class="amp-wp-article">

    <header class="amp-wp-article-header">
        <h1 class="amp-wp-title"><?php echo wp_kses_data( $this->get( 'post_title' ) ); ?></h1>
    </header>

    <?php $this->load_parts( array( 'featured-image' ) ); ?>

    <div class="amp-wp-article-content">
        <?php
        function phoneNumberReplace($content, $phone){
               $pattern = '((\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4})mi';
               $content = preg_replace($pattern, '$1 '. $phone . '$6', $content);
               return $content;
            }
            //Update Phone Number for Unified Phone Number
            $pageContent = $this->get( 'post_amp_content' );
            echo phoneNumberReplace( $pageContent, $phoneNumber );
        ?>
        <?php //echo $this->get( 'post_amp_content' ); // amphtml content; no kses ?>
    </div>

</article>

<div class="footer-contact">
    <span class="footer-phone-content">Call Now to Speak with One of Our Representatives</span>
    <span class="footer-phone-number"><a href="tel:+1-<?php echo  $phoneNumber; ?>"><?php echo  $phoneNumber; ?></a></span>


	<?php
		//do not display this form on the contact us page as it currently has a working one
		if(get_the_ID() !== 3334) {
			echo '<span class="footer-phone-content footer-form-heading">Or Fill Out the Form Below.</span>';
			//Cleaning up form for AMP validation via shortcode
			$formString = do_shortcode('[contact-form-7 id="3817" title="Contact Us Page Form"]');
			$formStringCleaned = str_replace('<form action="https:', '<form action-xhr="', $formString);
			echo $formStringCleaned;
		}
    ?>
</div>

<footer>
    Copyright <?php echo date('Y') ?> Cordisco Saile <br> All Rights Reserved.
</footer>

<?php //$this->load_parts( array( 'footer' ) ); ?>

<?php do_action( 'amp_post_template_footer', $this ); ?>

<amp-analytics type="googleanalytics" id="analytics1">
<script type="application/json">
{
  "vars": {
    "account": "<?php echo $analyticsID; ?>"
  },
  "triggers": {
  "trackPageviewWithAmpdocUrl": {
      "on": "visible",
      "request": "pageview",
      "vars": {
          "ampdocUrl": "<?php echo get_the_permalink() . 'amp/'; ?>"
      }
     }
  }
}
</script>
</amp-analytics>

</body>
</html>

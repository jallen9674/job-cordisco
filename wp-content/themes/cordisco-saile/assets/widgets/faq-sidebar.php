<?php

/***** YEARLY ARCHIVES FOR FAQS *******/
function past_issue_rewrite_rules(){

    // add_rewrite_rule(
    //     'faqs/category/([^/]*)/([^/]*)/?',
    //     'index.php?cordisco_faq_cat=$matches[1]&archiveyear=$matches[2]',
    //     'top'
    // );

    add_rewrite_rule(
        'faqs/([0-9]{4})/?',
        'index.php?page_id=2954&archiveyear=$matches[1]',
        'top'
    );

    // add_rewrite_tag('%archiveyear%', '([^&]+)');


    // echo '<!--';
    // var_dump(get_query_var('archiveyear'));
    // echo '-->';

}
add_action( 'init', 'past_issue_rewrite_rules' );

function rs_get_faq_year_from_url(){
  $year = '';
  if(preg_match('/\/faqs\/([0-9]{4})?\//', $_SERVER['REQUEST_URI'], $matches)){
    $year = $matches[1];
  }

  return $year;
}

function include_archive_year( $query ) {
    $year = rs_get_faq_year_from_url();

    if($year && $query->is_main_query()) {
      $query->set('year', $year);
      
      function custom_title($title){
        $year = rs_get_faq_year_from_url();
        return $year . ' - ' . $title;
      }
      
      add_filter('wpseo_title', 'custom_title');
      
    }
}
add_action( 'pre_get_posts', 'include_archive_year' );


// Creating the widget
class hc_faq_sidebar extends WP_Widget {

    function __construct() {
    parent::__construct(

    // Base ID
    'hc_faq_sidebar',

    // Widget
    'HC faq Sidebar Widget',

    // Widget description
    array( 'description' => 'Sidebar widget used on faq pages')
    );
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );

    $faqTerms = get_the_terms( $post->ID, 'cordisco_faq_cat' );

    // check if page relateg to faq
    if((is_tax('cordisco_faq_cat')) || is_singular('cordisco_faq')):

      // before and after widget arguments are defined by themes
      echo $args['before_widget'];
      
      $queried_object = get_queried_object();
      if ( $queried_object ) {
        $post_id = $queried_object->ID;
        echo get_field('sidebar_content', $queried_object);
      }

      $args = array(
        'type'            => 'yearly',
        'format'          => 'custom',
        'post_type'     => 'cordisco_faq',
        'before' => '<li>',
        'after' => '</li>'
      );
      ?>

      <style>
        .archive-article-list ul li {
          display: inline-block;
          min-width: 30%;
        }
        ul.blog-sidebar-list {
            background: #fff;
            padding: 10px 20px;
            margin-bottom: 0;
        }
        ul.blog-sidebar-list li {
            margin: 10px 0;
        }
        #page-footer {
          display: table;
          width: 100%;
        }
        .btn.btn-contact-us {
            display: block;
            vertical-align: top;
            width: 100%;
            color: #fff;
            float: none;
            border-radius: 5px;
            text-decoration: none;
            outline: none;
            background: #c60144;
            text-align: center;
            font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 1em;
            border: none;
            padding: 12px 10px;
            -webkit-transition: all 0.2s;
            -o-transition: all 0.2s;
            transition: all 0.2s;
            margin: 20px auto 0 auto
        }
        .btn.btn-contact-us:hover {
            background: #3b80bf
        }
      </style>

      <div class="widget widget_search">
        <h4 class="widgettitle">Categories</h4>
        <ul class="blog-sidebar-list">
          <?php
            $categories = get_terms([
                'taxonomy' => 'cordisco_faq_cat',
                'hide_empty' => false,
            ]);
            foreach($categories as $category) {
              echo '<li><a href="' . get_term_link($category->term_id, 'cordisco_faq_cat') . '">' . $category->name . '</a></li>';
            }
          ?>
        </ul>
      </div>

      <div class="widget widget_search">
        <h4 class="widgettitle">Recent Posts</h4>
        <ul class="blog-sidebar-list">
            <?php
            $faqTerms = get_terms( 'cordisco_faq_cat' );
            $faqTermIDs = wp_list_pluck( $faqTerms, 'term_id' );

            $recent_posts = get_posts([
              'numberposts'=>5,
              'post_type' => 'cordisco_faq',
              'tax_query' => array(
                    array(
                        'taxonomy' => 'cordisco_faq_cat',
                        'field' => 'term_id',
                        'terms' => $faqTermIDs
                    ),
                ),
            ]);
            foreach( $recent_posts as $recent ){
              echo '<li><a href="' . get_permalink($recent->ID) . '">' .   $recent->post_title.'</a> </li> ';
            }
            wp_reset_query();
            ?>
          </ul>
      </div>

      <div class="widget widget_search archive-article-list">
        <h4 class="widgettitle">Archive Articles</h4>
        <ul class="blog-sidebar-list">
          <?php
              $args['numberposts'] = -1;
              $posts_to_year = get_posts( $args );

              $years = array_unique(array_map(function($post){
                
                return date('Y', strtotime($post->post_date));
              }, $posts_to_year));

              
                foreach( $years as $year ){
                  // if($queried_object && $queried_object->taxonomy == 'cordisco_faq_cat') {
                  //   $link = site_url("/faqs/category/{$queried_object->slug}/$year");
                  // } else {
                    $link = site_url("/faqs/$year");
                  // }
                  echo '<li><a href="' . $link . '/">' . $year . '</a> </li> ';
                }
              
            ?>
        </ul>
      </div>

      <a href="<?= get_permalink(3334); ?>" class="btn btn-contact-us">Free <?=$queried_object->name?> Personal Injury Lawyer Consultation</a>

    </div>

      <?php echo $args['after_widget'];

    endif;
}

// Widget Backend
public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) ) {
        $title = $instance[ 'title' ];
    }
    else {
        $title = __( 'New title', 'wpb_widget_domain' );
    }

    // Widget admin form
    ?>
    <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
    </p>
    <?php
}

// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    return $instance;
    }
} // Class wpb_widget ends here

// Register and load the widget
function hc_load_faq_sidebar_widget() {
    register_widget( 'hc_faq_sidebar' );
}

add_action( 'widgets_init', 'hc_load_faq_sidebar_widget' );
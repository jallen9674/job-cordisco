<?php

function is_blog () {
  global  $post;
  $posttype = get_post_type($post );
  return ( ((is_archive()) || (is_author()) || (is_category()) || (is_home()) || (is_single()) || (is_tag())) && ( $posttype == 'post')  ) ? true : false ;
}

// Creating the widget
class hc_blog_sidebar extends WP_Widget {

    function __construct() {
    parent::__construct(

    // Base ID
    'hc_blog_sidebar',

    // Widget
    'HC Blog Sidebar Widget',

    // Widget description
    array( 'description' => 'Sidebar widget used on blog pages')
    );
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );

    // check if page related to blog
    if(is_blog()):

      // before and after widget arguments are defined by themes
      echo $args['before_widget'];
      
      $queried_object = get_queried_object();
      if ( $queried_object ) {
        $post_id = $queried_object->ID;
        echo get_field('sidebar_content', $queried_object);
      }

      $args = array(
        'type'            => 'yearly',
        'format'          => 'custom',
        'post_type'     => 'post',
        'before' => '<li>',
        'after' => '</li>'
      );
      ?>

      <style>
        .archive-article-list ul li {
          display: inline-block;
          min-width: 30%;
        }
        ul.blog-sidebar-list {
            background: #fff;
            padding: 10px 20px;
            margin-bottom: 0;
        }
        ul.blog-sidebar-list li {
            margin: 10px 0;
        }
        #page-footer {
          display: table;
          width: 100%;
        }
        .btn.btn-contact-us {
            display: block;
            vertical-align: top;
            width: 100%;
            color: #fff;
            float: none;
            border-radius: 5px;
            text-decoration: none;
            outline: none;
            background: #c60144;
            text-align: center;
            font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 1em;
            border: none;
            padding: 12px 10px;
            -webkit-transition: all 0.2s;
            -o-transition: all 0.2s;
            transition: all 0.2s;
            margin: 20px auto 0 auto
        }
        .btn.btn-contact-us:hover {
            background: #3b80bf
        }
      </style>

      <div class="widget widget_search">
        <h4 class="widgettitle">Categories</h4>
        <ul class="blog-sidebar-list">
          <?php 
            // $categories = get_categories();
            // foreach($categories as $category) {
            //   echo '<li><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></li>';
            // }

          wp_list_categories(['title_li' => '']);
          ?>
        </ul>
      </div>

      <div class="widget widget_search">
        <h4 class="widgettitle">Recent Posts</h4>
        <ul class="blog-sidebar-list">
            <?php
            $category = get_category( get_query_var( 'cat' ) );
            $cat_id = $category->cat_ID;
              $recent_posts = wp_get_recent_posts([
                'numberposts'=>5,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'category' => $cat_id
              ]);
              foreach( $recent_posts as $recent ){
                echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';
              }
              wp_reset_query();
            ?>
          </ul>
      </div>

      <div class="widget widget_search archive-article-list">
        <h4 class="widgettitle">Archive Articles</h4>
        <ul class="blog-sidebar-list">
          <?php wp_get_archives( $args ); ?>
        </ul>
      </div>

      <a href="<?= get_permalink(3334); ?>" class="btn btn-contact-us">Free <?=$queried_object->name?> Personal Injury Lawyer Consultation</a>

      <?php echo $args['after_widget'];

    endif;
}

// Widget Backend
public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) ) {
        $title = $instance[ 'title' ];
    }
    else {
        $title = __( 'New title', 'wpb_widget_domain' );
    }

    // Widget admin form
    ?>
    <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
    </p>
    <?php
}

// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    return $instance;
    }
} // Class wpb_widget ends here

// Register and load the widget
function hc_load_blog_sidebar_widget() {
    register_widget( 'hc_blog_sidebar' );
}

add_action( 'widgets_init', 'hc_load_blog_sidebar_widget' );
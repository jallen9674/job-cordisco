<?php


add_filter( 'posts_where', 'wpse18703_posts_where', 10, 2 );
function wpse18703_posts_where( $where, &$wp_query )
{
    global $wpdb;
    if ( $wpse18703_title = $wp_query->get( 'hc_title' ) ) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'' . esc_sql( $wpdb->esc_like( $wpse18703_title ) ) . '%\'';
    }
    return $where;
}



// Creating the widget
class cordisco_city_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
        // Base ID of your widget
        'cordisco_city_widget',

        // Widget name will appear in UI
        __('"Near Me" Link Widget', 'cordisco_city_widget_domain'),

        // Widget description
        array( 'description' => __( 'Widget that displays a rotation of citys for Cordisco & Saile', 'cordisco_city_widget_domain' ), )
    );
    }


    // Creating widget front-end
    // This is where the action happens
    public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );
    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    echo '<div class="near-me-widget-box">';
/*----------------------- BEGIN OUTPUT --------------------------*/

    $titleArray = array(
        'Find an Experienced <br> Lawyer Now!',
        'Find a Competent <br> Lawyer Now!',
        'Find a Determined <br> Lawyer Now!',
        'Find a Diligent <br> Lawyer Now!',
        'Find a Friendly <br> Lawyer Now!',
        'Find a Hard-Working <br> Lawyer Now!',
        'Find a Passionate <br> Lawyer Now!',
        'Find a Reliable <br> Lawyer Now!',
        'Find a Persistent <br> Lawyer Now!',
    );
    echo $args['before_title'] . $titleArray[array_rand($titleArray)] . $args['after_title'];
?>



    <ul class="near-me-widget-list">

        <?php //Output a list of 10 random cities ?>

          <?php
            // get all terms in the taxonomy
            $terms = get_terms( 'hc_location' );
            // convert array of term objects to array of term IDs
            $term_ids = wp_list_pluck( $terms, 'term_id' );
            global $post;



            remove_all_filters('posts_orderby');
            $args = array(
                    'post_type' => 'page',
                    'posts_per_page' => 10,
                    'orderby' => 'rand',
                    'order' => 'ASC',
                    'hc_title' => 'Personal Injury',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'hc_location',
                            'field'    => 'term_id',
                            'terms'    => $term_ids,
                        ),
                    )
                );

            $query = new WP_Query($args);

            $max = $query->post_count;
            $rand_main_page = rand(0, $max-1);
            $l = 0;
            if($query->have_posts()):  while($query->have_posts()):$query->the_post();


                //Get the Link Title
                $linkTitle = get_post_meta( $post->ID, '_hc_location_widget_title', true );

                //Get Location Title
                $postTerms =  wp_get_object_terms($post->ID, 'hc_location');

                $categoryFilterSlug = '';
                $categoryPrettyName = '';

                if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
                     foreach ( $postTerms as $term ) {
                       $categoryFilterSlug .= '' . $term->slug;
                       $categoryPrettyName .= ' ' . $term->name;
                     }
                 }
            ?>

            <?php if (1 || $linkTitle !== 'General Information' ) :?>
                <li>
                    <a href="<?php the_permalink(); ?>">
                        <?php echo $categoryPrettyName . '  ' . $linkTitle; ?> Lawyer
                    </a>
                </li>
            <?php endif;
            if($l == $rand_main_page) { ?>
                <li>
                    <a href="/">
                        Bucks County Personal Injury Lawyer
                    </a>
                </li>
            <?php }
            $l++;
            ?>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
        <?php endif; ?>

    </ul>

</div> <?php //End .city-widget-box; ?>
</div>

<?php
/*----------------------- END OUTPUT --------------------------*/

}

// Widget Backend
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'New title', 'cordisco_city_widget_domain' );
}
// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php
}

// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
} // Class cordisco_city_widget ends here

// Register and load the widget
function wpb_load_widget() {
    register_widget( 'cordisco_city_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );
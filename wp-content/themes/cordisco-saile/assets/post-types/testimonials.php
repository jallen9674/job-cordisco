<?php
/*---------------------------------------------------------
Adding Testimonials Post Type & Taxonomy
----------------------------------------------------------*/

function cordisco_testionial_posttype() {
	// creating (registering) the FAQ
	register_post_type( 'cordisco_testimonial', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Testimonials', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Testimonial', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Testimonials', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Testimonial', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Testimonials', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Testimonial', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Testimonial', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Testimonial', 'bonestheme' ), /* Search Testimonial Title */
			'not_found' =>  __( 'Nothing found in the Database.', 'bonestheme' ), /* This displays if there are no entries yet */
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the Testimonial post type', 'bonestheme' ), /* Testimonial Description */
			'public' => false,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => '',  /* get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png'*/
			'rewrite'	=> array( 'slug' => 'testimonial', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'false', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'author', 'thumbnail')
		) /* end of options */
	); /* end of register FAQ */
}

// adding the function to the Wordpress init
add_action( 'init', 'cordisco_testionial_posttype');


add_action( 'cmb2_init', 'cordisco_testimonial_meta' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_init' hook.
 */
function cordisco_testimonial_meta() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_cs_';

    /**
     * Sample metabox to demonstrate each field type included
     */
    $cs_testimonial_metabox = new_cmb2_box( array(
        'id'            => $prefix . 'testimonials',
        'title'         => __( 'Testimonial Information', 'cmb2' ),
        'object_types'  => array( 'cordisco_testimonial'), // Post type
        // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
    ) );

    $cs_testimonial_metabox->add_field( array(
        'name' => __( 'Testimonial Author', 'cmb2' ),
        'desc' => __( 'Enter the author of the testimonial.', 'cmb2' ),
        'id'   => $prefix . 'testimonial_author',
        'type' => 'text_medium',
        // 'repeatable' => true,
    ) );

    $cs_testimonial_metabox->add_field( array(
		'name' => __( 'Date', 'cmb2' ),
		'desc' => __( 'The date to be displayed for the testimonial', 'cmb2' ),
		'id'   => $prefix . 'testimonial_date',
		'type' => 'text_date_timestamp',
		// 'timezone_meta_key' => $prefix . 'timezone', // Optionally make this field honor the timezone selected in the select_timezone specified above
	) );

    $cs_testimonial_metabox->add_field( array(
        'name' => __( 'Number of Stars to Display', 'cmb2' ),
        'desc' => __( 'The number of stars to be displayed. This should be an integer between 1 and 5. If nothing is entered 5 stars will be displayed.', 'cmb2' ),
        'id'   => $prefix . 'testimonial_stars',
        'type' => 'text_medium',
        // 'timezone_meta_key' => $prefix . 'timezone', // Optionally make this field honor the timezone selected in the select_timezone specified above
    ) );

    $cs_testimonial_metabox->add_field( array(
        'name' => __( 'Link To Original Review', 'cmb2' ),
        'desc' => __( 'The URL of the review here.', 'cmb2' ),
        'id'   => $prefix . 'testimonial_url',
        'type' => 'text_url',
        // 'timezone_meta_key' => $prefix . 'timezone', // Optionally make this field honor the timezone selected in the select_timezone specified above
    ) );


    $cs_testimonial_metabox->add_field( array(
        'name' => __( 'Link Display Text', 'cmb2' ),
        'desc' => __( 'The text that should be displayed with the review URL (Ex. Google Review, etc.).', 'cmb2' ),
        'id'   => $prefix . 'testimonial_url_title',
        'type' => 'text_medium',
        // 'timezone_meta_key' => $prefix . 'timezone', // Optionally make this field honor the timezone selected in the select_timezone specified above
    ) );

    $cs_testimonial_metabox->add_field( array(
        'name' => __( 'Testimonial Content (Long)', 'cmb2' ),
        'desc' => __( 'Enter the long snippet of text that will be displayed as the description', 'cmb2' ),
        'id'   => $prefix . 'testimonial_description_long',
        'type' => 'wysiwyg',
        // 'repeatable' => true,
    ) );


    $cs_testimonial_metabox->add_field( array(
        'name' => __( 'Testimonial Content (Short)', 'cmb2' ),
        'desc' => __( 'Enter the short snippet of text that will be displayed as the description', 'cmb2' ),
        'id'   => $prefix . 'testimonial_description_short',
        'type' => 'wysiwyg',
        // 'repeatable' => true,
    ) );


}



?>
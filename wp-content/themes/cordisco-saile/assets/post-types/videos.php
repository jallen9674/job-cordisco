<?php


add_action( 'cmb2_init', 'cordisco_videos' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_init' hook.
 */
function cordisco_videos() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_cordisco_';

    /**
     * Sample metabox to demonstrate each field type included
     */
    $cordisco_video_meta = new_cmb2_box( array(
        'id'            => 'cordisco-video-faq-metabox',
        'title'         => __( 'Video Embed Information', 'cmb2' ),
        'object_types'  => array( 'page', ), // Post type
        // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
    ) );

    $cordisco_video_meta->add_field( array(
        'name'       => __( 'Youtube Video ID', 'cmb2' ),
        'desc'       => __( 'The ID of the Youtube Video to be embedded (ex. zdjzzlkYib0).', 'cmb2' ),
        'id'         => $prefix . 'video_id',
        'type'       => 'text_medium',
        'show_on_cb' => 'yourprefix_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );


    $cordisco_video_meta->add_field( array(
        'name'       => __( 'Youtube Video Embed with Schema', 'cmb2' ),
        'desc'       => __( 'The Youtube Embed with schema markup. This will be used instead of the standard embed. This can be generated at https://www.sistrix.com/video-seo/.', 'cmb2' ),
        'id'         => $prefix . 'video_id_schema',
        'type'       => 'textarea',
        'show_on_cb' => 'yourprefix_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );
}

<?php

// let's create the function for the custom type
function cordisco_attorneys() {
    // creating (registering) the custom type
    register_post_type( 'cordisco_attorneys', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
        // let's now add all the options for this post type
        array( 'labels' => array(
            'name' => __( 'Attorneys', 'bonestheme' ), /* This is the Title of the Group */
            'singular_name' => __( 'Attorney', 'bonestheme' ), /* This is the individual type */
            'all_items' => __( 'All Attorneys', 'bonestheme' ), /* the all items menu item */
            'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
            'add_new_item' => __( 'Add New Attorney', 'bonestheme' ), /* Add New Display Title */
            'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
            'edit_item' => __( 'Edit Attorneys', 'bonestheme' ), /* Edit Display Title */
            'new_item' => __( 'New Attorney', 'bonestheme' ), /* New Display Title */
            'view_item' => __( 'View Attorney', 'bonestheme' ), /* View Display Title */
            'search_items' => __( 'Search Attorneys', 'bonestheme' ), /* Search Custom Type Title */
            'not_found' =>  __( 'Nothing found in the Database.', 'bonestheme' ), /* This displays if there are no entries yet */
            'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
            'parent_item_colon' => ''
            ), /* end of arrays */
            'description' => __( 'These are the attorneys', 'bonestheme' ), /* Custom Type Description */
            'public' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'show_ui' => true,
            'query_var' => true,
            'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
            'menu_icon' => '',  /* get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png'*/
            'rewrite'   => array( 'slug' => 'attorneys', 'with_front' => false ), /* you can specify its url slug */
            'has_archive' => false, /* you can rename the slug here */
            'capability_type' => 'post',
            'hierarchical' => false,
            /* the next one is important, it tells what's enabled in the post editor */
            'supports' => array( 'title', 'revisions', 'thumbnail')
        ) /* end of options */
    ); /* end of register post type */

}

    // adding the function to the Wordpress init
    add_action( 'init', 'cordisco_attorneys');


add_action( 'cmb2_init', 'cordisco_attorney_metaboxes' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_init' hook.
 */
function cordisco_attorney_metaboxes() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_cordisco_';

    /**
     * Sample metabox to demonstrate each field type included
     */
    $cordisco_attorney_metabox = new_cmb2_box( array(
        'id'            => $prefix . 'cordisco_attorneys',
        'title'         => __( 'Attorney Information', 'cmb2' ),
        'object_types'  => array( 'cordisco_attorneys'), // Post type
        // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
    ) );

    $cordisco_attorney_metabox->add_field( array(
        'name' => __( 'Name', 'cmb2' ),
        'desc' => __( 'Enter the name of the attorney here.', 'cmb2' ),
        'id'   => $prefix . 'attorney_name',
        'type' => 'text_medium',
        // 'repeatable' => true,
    ) );

    $cordisco_attorney_metabox->add_field( array(
        'name' => __( 'Special Title Information', 'cmb2' ),
        'desc' => __( 'Enter the special title (ex. Founding Partner) of the attorney here.', 'cmb2' ),
        'id'   => $prefix . 'attorney_title',
        'type' => 'text_medium',
        // 'repeatable' => true,
    ) );

    $cordisco_attorney_metabox->add_field( array(
        'name' => __( 'General Page Text', 'cmb2' ),
        'desc' => __( 'Enter the main page content here.', 'cmb2' ),
        'id'   => $prefix . 'attorney_bio',
        'type' => 'wysiwyg',
        // 'repeatable' => true,
    ) );

}




?>
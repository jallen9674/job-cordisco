<?php
/*---------------------------------------------------------
Adding FAQs Post Type & Taxonomy
----------------------------------------------------------*/

function cordiscoFaqPosttype() {
	// creating (registering) the FAQ
	register_post_type( 'cordisco_faq', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'FAQs', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'FAQ', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All FAQs', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New FAQ', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit FAQs', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New FAQ', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View FAQ', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search FAQ', 'bonestheme' ), /* Search FAQ Title */
			'not_found' =>  __( 'Nothing found in the Database.', 'bonestheme' ), /* This displays if there are no entries yet */
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the FAQ post type', 'bonestheme' ), /* FAQ Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => '',  /* get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png'*/
			'rewrite'	=> array( 'slug' => 'faqs', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'faq', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
		) /* end of options */
	); /* end of register FAQ */
}

// adding the function to the Wordpress init
add_action( 'init', 'cordiscoFaqPosttype');


register_taxonomy( 'cordisco_faq_cat',
	array('cordisco_faq'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'FAQ Categories', 'bonestheme' ), /* name of the FAQ taxonomy */
			'singular_name' => __( 'FAQ Category', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search FAQ Categories', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All FAQ Categories', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent FAQ Category', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent FAQ Category:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit FAQ Category', 'bonestheme' ), /* edit FAQ taxonomy title */
			'update_item' => __( 'Update FAQ Category', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New FAQ Category', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New FAQ Category Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_admin_column' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'faqs/category', 'with_front' => false ),
	)
);

?>

<?php

/*---------------------------------------------------------
Adding Taxonomy for General Page Categorization
----------------------------------------------------------*/

register_taxonomy( 'cordisco_page_cat',
    array('page'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    array('hierarchical' => true,     /* if this is true, it acts like categories */
        'labels' => array(
            'name' => __( 'Page Categories', 'bonestheme' ), /* name of the Page taxonomy */
            'singular_name' => __( 'Page Category', 'bonestheme' ), /* single taxonomy name */
            'search_items' =>  __( 'Search Page Categories', 'bonestheme' ), /* search title for taxomony */
            'all_items' => __( 'All Page Categories', 'bonestheme' ), /* all title for taxonomies */
            'parent_item' => __( 'Parent Page Category', 'bonestheme' ), /* parent title for taxonomy */
            'parent_item_colon' => __( 'Parent Page Category:', 'bonestheme' ), /* parent taxonomy title */
            'edit_item' => __( 'Edit Page Category', 'bonestheme' ), /* edit Page taxonomy title */
            'update_item' => __( 'Update Page Category', 'bonestheme' ), /* update title for taxonomy */
            'add_new_item' => __( 'Add New Page Category', 'bonestheme' ), /* add new title for taxonomy */
            'new_item_name' => __( 'New Page Category Name', 'bonestheme' ) /* name title for taxonomy */
        ),
        'show_admin_column' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'page-cat' ),
    )
);

?>
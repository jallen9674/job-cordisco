jQuery(document).ready(function($) {

	// Example adding and removing custom meta boxes on page templates

		$('#page_template').change(function() {
		    if ($(this).val() === 'page-custom.php') {
		       $('#custom_page_metabox').toggleClass('show');
		    }
		    else {
		    	$('#custom_page_metabox').removeClass('show');
		    }
		});
		if ($('#page_template').val() === 'page-custom.php') {
		     $('#custom_page_metabox').toggleClass('show');
		}

});
/*--------------------------------------------
Square4 General JavaScript
---------------------------------------------*/


/*--------------------------------------------
Bringing Everything for Koala Concat + Minify
-- May change back to gulp we will see.
---------------------------------------------*/
// @prepros-prepend "../vendor/jquery.mCustomScrollbar.js"
// @prepros-prepend "../vendor/jquery.touchMenuHover.js"
// @prepros-prepend "../vendor/mousewheelStopPropagation.js"
// @prepros-prepend "../vendor/parsley.js"
// @prepros-prepend "../vendor/slidebars.js"
// @prepros-prepend "../vendor/isotope.js"
// @prepros-prepend "../vendor/jquery.flexslider.js"
// @prepros-prepend "../vendor/lazyload.js"



/*--------------------------------------------
 ___ _            _     ___ _     _   _
 / __| |_ __ _ _ _| |_  |_ _| |_  | | | |_ __
 \__ \  _/ _` | '_|  _|  | ||  _| | |_| | '_ \
 |___/\__\__,_|_|  \__| |___|\__|  \___/| .__/
                                        |_|
---------------------------------------------*/


jQuery(document).ready(function($) {

/*****************************************
Lazy Load -- https://www.sitepoint.com/five-techniques-lazy-load-images-website-performance/
*****************************************/
var observer = lozad();
observer.observe();

/*****************************************
Get Initial Referrer and Set in Cookie
*****************************************/

//Cookie Helper Functions

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}


function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function getQueryVariable(variable){
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}

//Getting Referrer Info and Setting Cookie

if ( getCookie('hennessey-referrer') !== '' ) {
  console.log('Existing Cookie: ' + getCookie('hennessey-referrer'));
  console.log('Current Referrer: ' + document.referrer);

  //Update TextArea in Form
  $('textarea[name="your-info"]').html(getCookie('hennessey-referrer'));
} else {

  //Putting Together Cookie Info
  var utmSource = getQueryVariable('utm_source');
  var utmCampaign = getQueryVariable('utm_campaign');
  var utmMedium = getQueryVariable('utm_medium');
  var initialReferrer = document.referrer;

  //Pretty Values for No Info
  if ( utmSource == false ){
    utmSource = 'Not Set';
  }

  if ( utmCampaign == false ){
    utmCampaign = 'Not Set';
  }

  if ( utmMedium == false ){
    utmMedium = 'Not Set';
  }

  if ( initialReferrer == false ){
    initialReferrer = 'Direct/Not Set';
  }

  //Setting up Cookie Value
  var cookieSourceValue =   'Tracking Information - Source: ' + utmSource +
                            ', Campaign: ' + utmCampaign +
                            ', Medium: ' + utmMedium +
                            ', Entry Referrer: ' + initialReferrer;

  //Set Cookie
  setCookie('hennessey-referrer',cookieSourceValue,1);

  //Debugging
  console.log('New Cookie: ' + getCookie('hennessey-referrer'));
  console.log('Current Referrer: ' + document.referrer);

  //Update TextArea in Form
  $('textarea[name="your-info"]').html(getCookie('hennessey-referrer'));
}

/*--------------------------------------------
Initiating Lightbox for Homepage Video
- http://lokeshdhakar.com/projects/lightbox2/
---------------------------------------------*/

  $('.popup-with-zoom-anim').magnificPopup({
    type: 'inline',

    fixedContentPos: false,
    fixedBgPos: true,

    overflowY: 'auto',

    closeBtnInside: true,
    preloader: false,

    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-zoom-in'
  });

if ( $('body').hasClass('home') ) {
  videojs("homepage-video").ready(function(){
    var myPlayer = this;

    $('.video-play-button').on("click", function(){
        myPlayer.play();
          e.preventDefault();
        hash = link.attr("href");
        window.location = hash;
    });

    $('.mfp-container').on("click", function(){
        myPlayer.pause();
    });

  });
}

/*--------------------------------------------
Hide/Show Homepage Infusionsoft Form Captcha
---------------------------------------------*/

$('.sidebar-contact-form').on("click", function(){
    $(this).find('.captcha-wrapper').addClass('visible');
    event.stopPropagation();
});

$(window).click(function() {
    $('.captcha-wrapper').removeClass('visible');
});



/*--------------------------------------------
Second Level Menu Items Applying Styles to Parent
---------------------------------------------*/

$(".nav ul li.current-menu-item").parent().parent().children("a").addClass("currentpage-styles");


/*--------------------------------------------
Two Column Auto Split List
---------------------------------------------*/

$(function() {
  $("ul.two-column-list").each(function() {
    var list = $(this);
    var size_a = $(this).children($("li")).size();
    var current_size_a = 0;
        if (size_a % 2 === 0 ) {
            size_a = (size_a/2);
                    }
        else {
            size_a = (size_a/2)+1;
        }
      list.children().each(function() {
      //console.log(current_size_a + ": " + $(this).text());

      if (++current_size_a > size_a) {
        var new_list = $("<ul class='two-column-list fancy-list last-col'></ul>").insertAfter(list);
        list = new_list;
        current_size_a = 1;
      }
      list.append(this);
    });
  });
});


/*--------------------------------------------
Homepage Testimonial Activate!
---------------------------------------------*/
$('.homepage-section.testimonials-section .flexslider').flexslider({
  animation: "fade",
  directionNav: false,
  controlNav: true
});


/*--------------------------------------------
Ebook Sidebar Slider
---------------------------------------------*/
$('.ebook-sidebar-slider').flexslider({
  animation: "fade",
  directionNav: false,
  controlNav: true
});

/*--------------------------------------------
Footer Location Slider
---------------------------------------------*/
$('.footer-contact-slider').flexslider({
  animation: "fade",
  directionNav: false,
  controlNav: true
});



/*--------------------------------------------
Fade in Header on Scroll
---------------------------------------------*/

 var scrollHeader = $(window).scrollTop();

  if (scrollHeader >= 250) {
      $(".desktop-scrolled-header").addClass("scrolled");
  } else {
      $(".desktop-scrolled-header").removeClass("scrolled");
  }

$(window).scroll(function() {
  var scrollHeader = $(window).scrollTop();

    if (scrollHeader >= 250) {
        $(".desktop-scrolled-header").addClass("scrolled");
    } else {
        $(".desktop-scrolled-header").removeClass("scrolled");
    }
});

/*--------------------------------------------
Display Mobile Click to Call on Scroll
---------------------------------------------*/

$(window).on( 'scroll', function(){
    if ($(window).scrollTop() >= 300) {
        $('.footer-phone-circle').addClass('visible');
    } else {
        $('.footer-phone-circle').removeClass('visible');
    }
});


//Close Button
$('.mobile-contact-close').on("click",function(){
   $('.mobile-contact-box').addClass('closed');
   $('.footer-phone-circle').addClass('visible');
});

/*--------------------------------------------
Adding Drop Down Arrows to Menu Items
---------------------------------------------*/

$('.nav.top-nav .menu-parent-item a').append('<span class="desktop-downarrow"></span>');


/*--------------------------------------------
Adding Nice Scroll Bar to Slideout Nav
---------------------------------------------*/


$(".sb-slidebar").mCustomScrollbar({
    scrollButtons:{
      enable:false
    },
    scrollbarPosition: 'inside',
    scrollInertia: 0,
    updateOnContentResize: true
});


/*--------------------------------------------
Mobile CSS Dropdown Fix
---------------------------------------------*/

$('.menu-parent-item').touchMenuHover();


/*--------------------------------------------
Slider Navigation
 -  http://plugins.adchsm.me/slidebars/usage.php
---------------------------------------------*/

$.slidebars({
  siteClose:true,
  disableOver: 767
});


/*--------------------------------------------
Stop Mouse Scroll Propagation on Slider Nav
 - https://github.com/basselin/jquery-mousewheel-stop-propagation
---------------------------------------------*/

$('.sb-left').mousewheelStopPropagation();


/*--------------------------------------------
Scroll to Top Button
---------------------------------------------*/

$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top').addClass('top-visible');    // Fade in the arrow
    } else {
        $('#return-to-top').removeClass('top-visible');   // Else fade out the arrow
    }
});
$('#return-to-top').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
});

// Select all links with hashes
$('a[href*="#"]')
// Remove links that don't actually link to anything
.not('[href="#"]')
.not('[href="#0"]')
.click(function(event) {
  // On-page links
  if (
    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
    &&
    location.hostname == this.hostname
  ) {
    // Figure out element to scroll to
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    // Does a scroll target exist?
    if (target.length) {
      // Only prevent default if animation is actually gonna happen
      event.preventDefault();
      $('html, body').animate({
        scrollTop: target.offset().top - 90
      }, 1000, function() {
        // Callback after animation
        // Must change focus!
        var $target = $(target);
        $target.focus();
        if ($target.is(":focus")) { // Checking if the target was focused
          return false;
        } else {
          $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
          $target.focus(); // Set focus again
        };
      });
    }
  }
});
}); /* end of as page load scripts */



/*-----------------------------------------------
Initializing things that rely on full CSS load
to render correctly (Isotope etc.)
-----------------------------------------------*/

jQuery(window).bind('load', function($){

/*-----------------------------------------------
Isotope Video FAQ Filtering
-----------------------------------------------*/

  var $container = jQuery('.faq-filter-list').isotope({
    itemSelector: '.grid-item',
    layoutMode: 'fitRows',
    getSortData: {
      category: '[data-category]'
    },
    hiddenClass: 'isotope-hidden',
  });

// filter functions
  var filterFns = {

  };

  var itemReveal = Isotope.Item.prototype.reveal;
Isotope.Item.prototype.reveal = function() {
  itemReveal.apply( this, arguments );
  jQuery( this.element ).removeClass('isotope-hidden');
};

var itemHide = Isotope.Item.prototype.hide;
Isotope.Item.prototype.hide = function() {
  itemHide.apply( this, arguments );
  jQuery( this.element ).addClass('isotope-hidden');
};

  // bind filter button click
  jQuery('#filters').on( 'click', 'button', function() {

    var filterValue = jQuery( this ).attr('data-filter');
    // use filterFn if matches value
    filterValue = filterFns[ filterValue ] || filterValue;
    $container.isotope({ filter: filterValue });



  });

  // change is-checked class on buttons
  jQuery('.filters-button-group').each( function( i, buttonGroup ) {
    var $buttonGroup = jQuery( buttonGroup );
    $buttonGroup.on( 'click', 'button', function() {
      $buttonGroup.find('.is-checked').removeClass('is-checked');
      jQuery( this ).addClass('is-checked');
    });
  });


  $container.on( 'arrangeComplete',  function( event, filteredItems ) {
    //console.log( 'Isotope arrange completed on ' + filteredItems.length + ' items' );
    //equalheight('.faq-filter-listing');
  });

  $container.on( 'layoutComplete',  function( event, filteredItems ) {
    console.log( 'Layout Complete Isotope arrange completed on ' + filteredItems.length + ' items' );
    //jQuery('.faq-filter-listing').addClass('sorted');

     if(jQuery('.isotope-link:nth-of-type(1)').hasClass('is-checked')){
      jQuery('.faq-filter-listing').removeClass('sorted');
     }

     equalheight('.faq-filter-listing:not(.isotope-hidden)');

  });


  //Equal Height Columns
  equalheight('.faq-filter-listing:not(.isotope-hidden)');

}); //End Window Load Scripts





equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;

 jQuery(container).each(function() {

   $el = jQuery(this);
   jQuery($el).height('auto');
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

/*-----------------------------------------------
Deferred Loading External Javascript for PageSpeed
 - Currently Disabled
-----------------------------------------------*/

/*
 function downloadJSAtOnload() {
   var element1 = document.createElement("script");
   element1.src = "https://thelivechatsoftware.com/Dashboard/cwgen/scripts/library.js";
   document.body.appendChild(element1);
 }

 // Check for browser support of event handling capability
 if (window.addEventListener)
 window.addEventListener("load", downloadJSAtOnload, false);
 else if (window.attachEvent)
 window.attachEvent("onload", downloadJSAtOnload);
 else window.onload = downloadJSAtOnload;
*/

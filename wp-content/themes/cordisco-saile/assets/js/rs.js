jQuery(document).ready(function($){
	$('.widget_hc_faq_sidebar.faq-widget').removeClass('faq-widget')
	$(window).scroll(function(){
		if($(window).scrollTop() > 50) {
			$('.rs-mobile-header').addClass('rs-mobile-header-fixed');
			$('#rs-mobile-nav').addClass('rs-mobile-nav-scrolling');
		} else {
			$('.rs-mobile-header').removeClass('rs-mobile-header-fixed');
			$('#rs-mobile-nav').removeClass('rs-mobile-nav-scrolling');
		}
	});


	$('body').on('click', '.menu-item-has-children > a', function(e){
		e.stopPropagation();
		e.preventDefault();
		$(this).next().slideToggle();
	})

	$('.rs-icon-btn.rs-icon-menu').click(function(e){
		e.preventDefault();
		$('#rs-mobile-nav').slideToggle();
	})


	if($(window).width() < 768) {
		var html = $('.nav.top-nav')[0].outerHTML;
		$('body').append('<div id="rs-mobile-nav">'+html+'</div>')
	}


	function enableScrolling() {

        event.preventDefault()
        $('#rs-popup-form').fadeOut(300)  
        $('body, html').css({
            'height': 'auto',
            'overflow': 'auto'
        })
        document.ontouchmove = function(e){ return true; }
    }

    $('#fixed-review-info .fixed-form-wrapper').click(function(e){
            e.stopPropagation();
        })

    $('#fixed-review-info .close').click(function(e){
        e.preventDefault();
        $('.blur-wrp').css('filter', 'none')
        $('#fixed-review-info').removeClass('fixed-review-info-expanded')

        enableScrolling()
    })

    // $('body').click(function(){
    //     $('.blur-wrp').css('filter', 'none')
    //     $('#fixed-review-info').removeClass('fixed-review-info-expanded')

    //     enableScrolling()
    // })


    $('#fixed-review-info').click(function(){
        $('#fixed-review-info').removeClass('fixed-review-info-expanded')
        enableScrolling()
    })

	$('.rs-icon-form').click(function(e){
		e.stopPropagation();

        e.preventDefault()
        $('#fixed-review-info').addClass('fixed-review-info-expanded')
        $('.blur-wrp').css('filter', 'blur(4px)')



        e.preventDefault();
        $('#rs-popup-form').css('display', 'flex').hide().fadeIn(300)
        $('body, html').css({
            'height': '100%',
            'overflow': 'hidden'
        })
        document.ontouchmove = function(e){ e.preventDefault(); }
	})

	$('.fixed-review-content .form-group input').blur(function(){
			var additional = true;
			if($(this).attr('type') === 'email') {
				function validateEmail(email) {
				    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				    return re.test(String(email).toLowerCase());
				}
				additional = validateEmail($(this).val())
			}
			if($(this).attr('type') === 'tel') {
				var re = /^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;
				additional = re.test($(this).val().toLowerCase());
			}
			if($(this).val().length > 2 && additional) {
				$(this).parents('.form-group').addClass('form-group-success')
			} else {
				$(this).parents('.form-group').removeClass('form-group-success')
			}
		})



	// PRACTICE AREAS SUB MENU


	function removeDuplicates(e,r){for(var n={},o=0,t=e.length;t>o;o++)n[e[o][r]]||(n[e[o][r]]=e[o]);var u=[];for(var c in n)u.push(n[c]);return u}
	
	
	if($('.location-widget-links.location-listing a').length) {
			
			var links = [];
			var core = [
"Bicycle Accidents",
"Birth Injuries",
"Bus Accidents",
"Car Accidents",
"Construction Accidents",
"Dog Bites",
"Drunk Driving Accidents",
"Medical Malpractice",
"Motorcycle Accidents",
"Nursing Home Abuse",
"Pedestrian Accidents",
"Premises Liability",
"Slip and Fall Accidents",
"Social Security Disability",
"Swimming Pool Accidents",
"Taxicab Accidents",
"Traumatic Brain Injuries",
"Truck Accidents",
"Uninsured Motorist Accidents",
"Unsafe Premises and Property Negligence",
"Workers’ Compensation",
"Wrongful Death"
];

			$('.location-widget-links.location-listing a').each(function(){
				var el = this;
				// core.forEach(function(item){
				// 	if($(el).text().trim() === item) {
						links.push({url: $(el).attr('href'), title: $(el).text().trim()});
				//	}
				//});
			});

			var html = '';
			removeDuplicates(links, 'title').forEach(function(item){
			html += '<li><a href="'+item.url+'">'+item.title+'</a></li>';
			});
			
			$('.menu-item-3347').addClass('menu-item-has-children menu-parent-item').append('<ul class="sub-menu"></ul>');
			$('.menu-item-3347 > a + .sub-menu').html(html);
			$('.menu-item-3347 > a + .sub-menu').width('auto');
			$('.menu-item-3347 > a').attr('href', '#');
		}

});
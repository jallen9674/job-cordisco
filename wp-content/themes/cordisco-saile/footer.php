
		</div> <?php // end #container ?>

		<div class="footer-contact-form">
			<div class="wrapper">
				<div class="footer-contact-form__inner">
					<?php echo do_shortcode('[contact-form-7 id="3359" title="Footer Contact Form" html_class="footer-contact-form__form"]'); ?>
				</div>
			</div>
		</div>

		<footer class="footer" id="page-footer" >

			<div id="inner-footer" class="wrapper">

				<div class="footer-widgets">
					<div class="footer-widgets__inner">

						<?php if ( is_active_sidebar( 'footer' ) ) : ?>
							<?php dynamic_sidebar( 'footer' ); ?>
						<?php endif; ?>

					</div>
				</div>

			</div>

			<div class="inner-footer-bottom">
				<div class="wrapper">
					<div class="row">
						<div class="wrapper">
							<div class="search-box-wrapper">
								<span class="footer-search-title">Need Help Finding What You Need?</span>
								<form class="desktop-search search-box" action="<?php echo home_url('/'); ?>">
									<input type="text" placeholder="Search the Site" name="s" value="" aria-label="Search Input">
								    <input type="submit" class="search-submit" value="" aria-label="Search Submit">
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="wrapper">
					<div class="footer-social-icons__social-icons" itemscope itemtype="http://schema.org/Organization">
						<link itemprop="url" href="<?php echo site_url(); ?>">
						<a itemprop="sameAs" rel="nofollow" target="_blank" href="https://www.facebook.com/pages/Cordisco-Law-LLC/335095316644989" class="footer-social-icons__social-icon footer-social-icons__social-icon--facebook"><i class="fa fa-facebook"></i></a>
						<a itemprop="sameAs" rel="nofollow" target="_blank" href="https://twitter.com/cordiscosaile" class="footer-social-icons__social-icon footer-social-icons__social-icon--twitter"><i class="fa fa-twitter"></i></a>
						<a itemprop="sameAs" rel="nofollow" target="_blank" href="https://www.linkedin.com/in/johncordiscoesq" class="footer-social-icons__social-icon footer-social-icons__social-icon--linkedin"><i class="fa fa-linkedin"></i></a>
					</div>
					<p class="footer-disclaimer">
						Cordisco and Saile law firm practices personal injury law.  Cordisco and Saile's offices are located in Bucks County, Pennsylvania. Clients may be referred to associated law firms. Determination of the need for legal services and the choice of a lawyer are extremely important decisions and should not be based solely on advertisements or self-proclaimed knowledge. No representation is made that the quality of the legal services to be performed is greater than the quality of the legal services performed by other law firms. Prior results do not guarantee a similar outcome. The information herein is attorney advertising. It is for informational use only. Do not construe this as legal advice. You should always seek guidance from an attorney who is licensed to practice in your jurisdiction.
					</p>
					<p class="source-org copyright">
						&copy; <?php echo date('Y'); ?> Cordisco & Saile LLC, All Rights Reserved.<br>
						<a href="<?php echo site_url(); ?>/sitemap/
						">Site Map</a> | <a href="<?php echo site_url(); ?>/privacy/">Privacy Policy</a>
					</p>
				</div>
			</div>

		</footer>


	</div><?php //  END .sticky-footer-container ?>


	<?php //BEGIN Mobile Phone Popup ?>
	<?php /* Disabled Currently
	<div class="mobile-contact-box sb-slide">
	    Contact Us Now!<br>
	    <a onclick="ga('send', 'event', 'Mobile Phone Link', 'Mobile Phone Click', 'Mobile Phone Click');" class="mobile-contact-button mobile-phone-analytics-track" href="tel:+1-215-642-2335">215-642-2335</a>
	    <span class="mobile-contact-close"><i class="fa fa-times"></i></span>
	</div>
	*/ ?>
	<a onclick="ga('send', 'event', 'Mobile Phone Link', 'Mobile Phone Click', 'Mobile Phone Click');" href="tel:+1-215-642-2335" class="footer-phone-circle mobile-phone-analytics-track"><i class="fa fa-phone"></i></a>

	<?php //END Mobile Phone Popup ?>


 	<div class="sb-slidebar sb-right">
		<span class="slidebar-header h2">
		Menu
			<span class="mobile-nav-box-close sb-close">
			</span>
		</span>

		<form class="search-box mobile-search" role="search" action="<?php echo home_url('/'); ?>">
			<input type="text" placeholder="Search the Site" name="s" value="" aria-label="Search Input">
			<input type="submit" class="search-submit" value="" aria-label="Search Submit">
		</form>

		<nav role="navigation">
			<?php wp_nav_menu(array(
			'container' => false,                           // remove nav container
			'container_class' => 'menu cf',                 // class of container (should you choose to use it)
			'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
			'menu_class' => 'nav slideout-nav cf',               // adding custom nav class
			'theme_location' => 'main-nav',                 // where it's located in the theme
			'before' => '',                                 // before the menu
			'after' => '',                                  // after the menu
			'link_before' => '',                            // before each link
			'link_after' => '',                             // after each link
			'depth' => 0,                                   // limit the depth of the nav
			'fallback_cb' => ''                             // fallback function (if there is one)
			)); ?>
		</nav>
	</div>

	<span id="return-to-top"><i class="fa fa-angle-up"></i></span>

	<?php //Adding APEX Chat ?>
	<?php
	  //Hiding Chat on Certain Pages
	  if ( (!is_single( array(2576, 269, 2551)) &&  !is_user_logged_in() ) ) :
			echo '<script src="//www.apex.live/scripts/invitation.ashx?company=cordiscosaile" async></script>';
	  endif;
	?>


<div id="fixed-review-info" style="opacity: 0;">
	<div class="fixed-form-wrapper">
		<a href="" class="close">x</a>
		<div class="standard-form">
			<?=do_shortcode('[contact-form-7 id="3361" title="Sidebar Contact Form"]')?>
		</div>

	</div>
</div>


	<?php // all js scripts are loaded in library/bones.php ?>
	<?php wp_footer(); ?>
	</body>

</html>

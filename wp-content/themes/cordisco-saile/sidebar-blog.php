<div id="sidebar" class="sidebar content-container blog-sidebar">

	<?php if ( is_active_sidebar( 'blog' ) ) : ?>

		<?php dynamic_sidebar( 'blog' ); ?>

	<?php else : ?>

		<?php
			/*
			 * This content shows up if there are no widgets defined in the backend.
			*/
		?>

		<div class="no-widgets">
			<p>These are not the widgets you are looking for...</p>
		</div>

	<?php endif; ?>

	<?php
			//echo do_shortcode('[cordisco-random-faq-loop-image amount="4"]');
	?>

</div>
